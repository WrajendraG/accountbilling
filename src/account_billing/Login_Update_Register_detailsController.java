package account_billing;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;
import requriedDatabaseConnection.DatabaseConnection;

public class Login_Update_Register_detailsController implements Initializable {

    @FXML
    private Label uname, passwd;
    @FXML
    private Button exit;
    @FXML
    private TextField uname2, passwd2;
    JOptionPane jp = new JOptionPane();
    @FXML
    private AnchorPane pane;
    private String userId;
    @FXML
    private String passWord;
    Connection connection;
    @FXML
    private Button login;

   
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        connection = DatabaseConnection.getConnection();
    }

    @FXML
    private void login(ActionEvent event) throws IOException {
        
        
        try {

            Statement stm = connection.createStatement();
                        
                ResultSet rs = stm.executeQuery("select * from PASS");
                int flag =0;
                while (rs.next()) {
                    
                    userId = rs.getString(1);
                    passWord = rs.getString(2);
                    
                              if ((userId.equals(uname2.getText())) && (passWord.equals(passwd2.getText()))) {
                    
                                    jp.showMessageDialog(null, "Login Completed Successfully", "SUCCESS", jp.INFORMATION_MESSAGE);
                                  
                                        Parent parent = FXMLLoader.load(getClass().getResource("Register_Details_Update.fxml"));
                                        pane.getChildren().setAll(parent);

                                            flag=1;

                                    } 

                    
                }
                if (flag == 0) {
                    
                    jp.showMessageDialog(null, " Wrong Login  userId or password ", "Try Again", jp.INFORMATION_MESSAGE);
            }
                                              
        
    
        } catch (SQLException sql) {
            jp.showMessageDialog(null, sql, "EXCEPTION", jp.ERROR_MESSAGE);
        }
    }

    @FXML
    private void exit(ActionEvent event) {
        System.exit(0);
    }

}
