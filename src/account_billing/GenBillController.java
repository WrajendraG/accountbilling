package account_billing;

import requriedDatabaseConnection.DatabaseConnection;
import com.osiersystems.pojos.ADD_items_details;
import com.osiersystems.pojos.NumToWord;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import org.controlsfx.control.textfield.TextFields;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.StringConverter;

public class GenBillController implements Initializable {

    //------------------------------------------------------------------------------
    @FXML
    private TextField invoice_Number;
    @FXML
    private DatePicker date;
    @FXML
    private TextField GST_Number;
    @FXML
    private TextField mobile_Number;
    @FXML
    private TextArea address;
    @FXML
    private TextField customer_Name;

    @FXML
    private TextField items;
    @FXML
    private Label label_cgst;
    @FXML
    private Label label_sgst;

    @FXML
    private TextField hsn_Code;

    @FXML
    private TextField quantity;

    @FXML
    private TextField amount;
    @FXML
    private ComboBox<String> units;
    @FXML
    private TextField rate;
    @FXML
    private TextField cgst;
    @FXML
    private TextField sgst;
    @FXML
    private TextField total;

    @FXML
    private TextField remaining_stock;
    int remaining_stockdisplayPerpose = 0;

    String status = "N/A";
    int receipt_Number = 0;

    @FXML
    private TableView<ADD_items_details> invoice_table;

    //-----------------------------column in table ------------------------------------
    @FXML
    private TableColumn<ADD_items_details, String> table_Sr_No;
    @FXML
    private TableColumn<ADD_items_details, String> table_Items;
    @FXML
    private TableColumn<ADD_items_details, String> table_Hsn_Code;
    @FXML
    private TableColumn<ADD_items_details, String> table_Units;
    @FXML
    private TableColumn<ADD_items_details, String> table_Rate;
    @FXML
    private TableColumn<ADD_items_details, String> table_Qty;
    @FXML
    private TableColumn<ADD_items_details, String> table_Amount;
    @FXML
    private TableColumn<ADD_items_details, String> table_Discount;
    @FXML
    private TableColumn<ADD_items_details, String> table_Taxable_Value;
    @FXML
    private TableColumn<ADD_items_details, String> table_Cgst;
    @FXML
    private TableColumn<ADD_items_details, String> table_Cgst_Rate;
    @FXML
    private TableColumn<ADD_items_details, String> table_Cgst_Amount;
    @FXML
    private TableColumn<ADD_items_details, String> table_Sgst;
    @FXML
    private TableColumn<ADD_items_details, String> table_Sgst_Rate;
    @FXML
    private TableColumn<ADD_items_details, String> table_Sgst_Amount;
    @FXML
    private TableColumn<ADD_items_details, String> table_Total;

    //   private ObservableList<ADD_items_details> list = FXCollections.observableArrayList();
    //---------------------------------- tablet total calculation --------------------------------------------  
    @FXML
    private TextField net_Total_Rate;
    @FXML
    private TextField net_Total_Amount;
    @FXML
    private TextField net_Total_Cgst_Amount;
    @FXML
    private TextField net_Total_Discount;
    @FXML
    private TextField net_Total_Taxable_value;
    @FXML
    private TextField net_Total_Sgst_Amount;
    @FXML
    private TextField net_Total_Qty;
    @FXML
    private TextField net_Total_Total;

    @FXML
    private AnchorPane anchor;

//---------------------------------------------------------------------------------------
    // Generate invoice requried data variable
    String invoiceYear = null;
    String invoicMonth = null;
    String invoicDay = null;
    String YYYYMMDD = null;
    int LastInvoiceNumber = 0; // extract last invoice number through database an increment by 1. for next invoice;

    float Globle_paidAmount = 0, GlobleOutStandingAmount;
    float Global_Total;

    String backYYMMDD = null;
    int t = 1; // this is used for when you want back invoice that time there are click event that means when you enter back invoice show alert  message only one time.

    Date date1;

//--------------------------Extra Variable---------------
    Alert alert = new Alert(Alert.AlertType.WARNING);

    Connection connection;

// when updating prodect that time requried this variable from itemDeatils table
    String itemsCalculationPurpose, hsnCodeCalculationPurpose, unitCalculationPurpose, gsttaxableCategory;
    float rateCalculationPurpose, amountCalculationPrupose, discountPerCalculationPrupose, discountRupessCalculationPrupose, taxableValueCalculationPrupose, cgstRupessCalculationPurpose, cgstPerCalculationPurpose, sgstRupessCalculationPurpose, sgstPerCalculationPurpose, totalCalculationPrupose, mrpCalculationPurpose;     // cost == amount within table  // use for updating purpose
    int qtyCalculationPurpose, stockCalculationPurpose;

    float gstRupessCalculationPrupose, salePriceCalculationPurpose;
    int gstPerCalculationPrupose;

    String minSalePricemrpCalculationPurpose, purchasePriceCalculationPerpose;

    //  storing a multiple items in Invoice table within single row.
    String invoiceItemName, invoiceHsnCode, invoiceUnit;
    //all float from database
    String invoiceRate, invoiceAmount, invoiceDiscount, invoiceTaxableValue, invoiceCgstRate, invoiceCgstAmount, invoiceSgstRate, invoiceSgstAmount, invoiceTotal, invoiceMRP, invoiceSalePrice;
    // all int from databse
    String invoiceQty, invoiceStock, invoiceTaxableCategory;
    //--------------------------------------
    @FXML
    private TextField bar_code;
    @FXML
    private TextField PaidAmount;
    @FXML
    private TextField OutstandingAmount;
    @FXML
    private Button ButtoninvoicePrint;
    @FXML
    private Button ButtonReciptInvoice;
    @FXML
    private TextField Business_Name;

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ INIRIALZE METHOD 
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        myinitializeMethod();

    }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   SEARCH DAILY COUSTOMER 
    @FXML
    private void searchCustomerName(KeyEvent event) {

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from CUSTOMER_ACCOUNT where BUSINESS_NAME ='" + Business_Name.getText() + "' ");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                mobile_Number.setText(resultSet.getString("PERSON_MOBILE_NO"));
                GST_Number.setText(resultSet.getString("BUSINESS_PAN_NO"));  // here Gst Number means PAN number ON GUI
                address.setText(resultSet.getString("BUSINESS_ADDRESS"));
                customer_Name.setText(resultSet.getString("CONTACT_PERSON"));
                resultSet.next();

            }

            preparedStatement.close();

        } catch (SQLException sql) {
            sql.printStackTrace();
            System.out.println("DB connectivity issue");
        }

    }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@@ GENERATE BACK INVOICE
//    private void backInvoice(KeyEvent event) {
//
//        if (t == 1) {
//
//            alert.setTitle("Back Invoice ");
//            alert.setHeaderText(null);
//            alert.setContentText("Date Format Must be DD-MM-YY Exmaple 31-12-2017 . ( - ) dash is important for Separator Enter For Invoice Number");
//            alert.showAndWait();
//            t++;
//        }
//
//        String d = String.valueOf(date.getValue());
//
//  
//        String[] d2 = d.split("-");
//        for (int i = d2.length - 1; i >= 0; i--) {
//            invoiceYear = d2[2];
//            invoicMonth = d2[1];
//            invoicDay = d2[0];
//
//            backYYMMDD = invoicDay + invoicMonth + invoiceYear;
//
//            try {
//                PreparedStatement preparedStatement = connection.prepareStatement("select count(*) as c from INVOICE_DETAILS where INVOICE_NUMBER LIKE '" + backYYMMDD + "%' ");
//                ResultSet resultSet = preparedStatement.executeQuery();
//                System.out.println("Generate back");
//                while (resultSet.next()) {
//
//                    LastInvoiceNumber = Integer.parseInt(resultSet.getString("c"));
//
//                }
//                LastInvoiceNumber = LastInvoiceNumber + 1;
//
//                invoice_Number.setText(backYYMMDD + LastInvoiceNumber);
//
//                preparedStatement.close();
//
//            } catch (SQLException sql) {
//                sql.printStackTrace();
//                System.out.println("DB connectivity issue");
//            }
//
//        }
//
//    }
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  SEARCH ITEMS IN STOCK
    @FXML
    private void searchitems(KeyEvent event) {

        try {

            String query = "select * from add_items_details where items_name='" + items.getText() + "'";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                itemsCalculationPurpose = resultSet.getString("ITEMS_NAME");
                hsnCodeCalculationPurpose = resultSet.getString("HSN_CODE");
                unitCalculationPurpose = resultSet.getString("UNITS");
                rateCalculationPurpose = Float.parseFloat(resultSet.getString("RATE"));

                stockCalculationPurpose = Integer.parseInt(resultSet.getString("STOCK"));

                salePriceCalculationPurpose = Float.parseFloat(resultSet.getString("SALE_PRICE"));

                purchasePriceCalculationPerpose = resultSet.getString("PURCHASE_PRICE");

                mrpCalculationPurpose = Float.parseFloat(resultSet.getString("MRP"));
                minSalePricemrpCalculationPurpose = resultSet.getString("MIN_SALE");

                gsttaxableCategory = resultSet.getString("TAXT_CATEGORY");

                cgstPerCalculationPurpose = Float.parseFloat(resultSet.getString("CGST_PER"));
                cgstRupessCalculationPurpose = Float.parseFloat(resultSet.getString("CGST_RUPESS"));
                sgstPerCalculationPurpose = Float.parseFloat(resultSet.getString("SGST_PER"));
                sgstRupessCalculationPurpose = Float.parseFloat(resultSet.getString("SGST_RUPESS"));

                gstPerCalculationPrupose = Integer.parseInt(resultSet.getString("GST_PER"));

                gstRupessCalculationPrupose = Float.parseFloat(resultSet.getString("GST_RUPESS"));

                discountRupessCalculationPrupose = Float.parseFloat(resultSet.getString("DISCOUNT_RUPESS"));
                discountPerCalculationPrupose = Float.parseFloat(resultSet.getString("DISCOUNT_PER"));

                hsn_Code.setText(hsnCodeCalculationPurpose);

                rate.setText(String.valueOf(rateCalculationPurpose));   //  salePriceCalculationPurpose =>    including gst

                qtyCalculationPurpose = 1;   // when you add new item than deafult quantity =1; requried  when qty change that time remainstock method invocked.

                quantity.setText(String.valueOf(qtyCalculationPurpose));

                units.setValue(unitCalculationPurpose);

            }

            // update stock from table after they added in table .
            // when he print invoice commit stock from table
            try {

                remaining_stockdisplayPerpose = (stockCalculationPurpose - qtyCalculationPurpose);

                remaining_stock.setText(String.valueOf(remaining_stockdisplayPerpose));

                //  addition on total when qty increased amount is increased (qty*rate) , when amount is incresed total is increased including gst  
                amountCalculationPrupose = (rateCalculationPurpose * qtyCalculationPurpose);

                taxableValueCalculationPrupose = amountCalculationPrupose;

                String temp1 = String.format("%.2f", amountCalculationPrupose);
                amount.setText(String.valueOf(temp1));

                cgst.setText(String.valueOf(cgstRupessCalculationPurpose));
                sgst.setText(String.valueOf(sgstRupessCalculationPurpose));

                totalCalculationPrupose = (amountCalculationPrupose + gstRupessCalculationPrupose);
                String total2 = String.format("%.2f", totalCalculationPrupose);
                total.setText(String.valueOf(total2));

            } catch (NumberFormatException e) {
                System.out.println(" Problem in  searchRemainingStock method ");
                e.printStackTrace();

            }

        } catch (SQLException ex) {
            Logger.getLogger(GenBillController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    ADD IN DATABASE (OR TABLE )
    @FXML
    private void addWithinTable(ActionEvent event) throws SQLException {

        String checkItems = null;

        if (items.getText().isEmpty() && hsn_Code.getText().isEmpty() && cgst.getText().isEmpty() && sgst.getText().isEmpty() && total.getText().isEmpty()) {

            alert.setTitle("Validation");
            alert.setHeaderText(null);
            alert.setContentText("Enter All Field data ");
            alert.showAndWait();

        } else {
            System.out.println(" I am Here ");
            String query = "select * from BILL_CALCULATION where items='" + items.getText() + "'";
            PreparedStatement preparedStatement4 = connection.prepareStatement(query);
            ResultSet resultSet4 = preparedStatement4.executeQuery();//'" + items.getText() + "'

            while (resultSet4.next()) {

                checkItems = resultSet4.getString("items");

            }

            preparedStatement4.close();

            if (items.getText().equals(checkItems)) {

                alert.setTitle("Validation");
                alert.setHeaderText(null);
                alert.setContentText("You are product already purchased you can update that product!");
                alert.showAndWait();
                loadDataFromDatabaseToTable();

            } else {

                PreparedStatement ps = connection.prepareStatement("insert into BILL_CALCULATION values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
// Store global variable here 
                ps.setString(1, itemsCalculationPurpose);
                ps.setString(2, hsnCodeCalculationPurpose);
                ps.setString(3, unitCalculationPurpose);
                ps.setFloat(4, rateCalculationPurpose);
                ps.setInt(5, qtyCalculationPurpose);
                ps.setFloat(6, amountCalculationPrupose);
                ps.setFloat(7, discountRupessCalculationPrupose);
                ps.setFloat(8, taxableValueCalculationPrupose);
                ps.setFloat(9, cgstPerCalculationPurpose);
                ps.setFloat(10, cgstRupessCalculationPurpose);
                ps.setFloat(11, sgstPerCalculationPurpose);
                ps.setFloat(12, sgstRupessCalculationPurpose);
                ps.setFloat(13, totalCalculationPrupose);
                ps.setInt(14, stockCalculationPurpose);  //              
                ps.setInt(15, gstPerCalculationPrupose);
                ps.setFloat(16, mrpCalculationPurpose);
                ps.setFloat(17, salePriceCalculationPurpose);

                int result = ps.executeUpdate();

                if (result > 0) {
                    System.out.println(" Items Insert successfuly  in table ");

                    loadDataFromDatabaseToTable();
                } else {
                    System.out.println(" not Insert successfuly in table ");
                }

                ps.close();
            }
        }
    }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@    LOAD TABLE
    public void loadDataFromDatabaseToTable() throws SQLException {

        ObservableList<ADD_items_details> list = FXCollections.observableArrayList();
        Statement stm = connection.createStatement();

        ResultSet resultSet = stm.executeQuery("select * from BILL_CALCULATION");
        int i = 1;
        while (resultSet.next()) {

            //   here global Items store for invoice
            if (i == 1) {
                invoiceItemName = resultSet.getString("ITEMS");
                invoiceHsnCode = resultSet.getString("HSN_CODE");
                invoiceUnit = resultSet.getString("UNITS");
                invoiceRate = String.valueOf(resultSet.getFloat("RATE"));
                invoiceQty = String.valueOf(resultSet.getInt("QTY"));
                invoiceAmount = String.valueOf(resultSet.getFloat("AMOUNT"));
                invoiceDiscount = String.valueOf(resultSet.getFloat("DISCOUNT"));
                invoiceTaxableValue = String.valueOf(resultSet.getFloat("TAXABLE_VALUE"));
                invoiceCgstRate = String.valueOf(resultSet.getFloat("CGST_RATE"));
                invoiceCgstAmount = String.valueOf(resultSet.getFloat("CGST_AMOUNT"));
                invoiceSgstRate = String.valueOf(resultSet.getFloat("SGST_RATE"));
                invoiceSgstAmount = String.valueOf(resultSet.getFloat("SGST_AMOUNT"));
                invoiceTotal = String.valueOf(resultSet.getFloat("TOTAL"));
                invoiceStock = String.valueOf(resultSet.getInt("STOCK"));
                invoiceTaxableCategory = String.valueOf(resultSet.getInt("TAXABLECATEGORY"));
                invoiceMRP = String.valueOf(resultSet.getFloat("MRP"));
                invoiceSalePrice = String.valueOf(resultSet.getFloat("SALE_PRICE"));
            } else {
                invoiceItemName = invoiceItemName + "," + resultSet.getString("ITEMS");
                invoiceHsnCode = invoiceHsnCode + "," + resultSet.getString("HSN_CODE");
                invoiceUnit = invoiceUnit + "," + resultSet.getString("UNITS");
                invoiceRate = invoiceRate + "," + String.valueOf(resultSet.getFloat("RATE"));
                invoiceQty = invoiceQty + "," + String.valueOf(resultSet.getInt("QTY"));
                invoiceAmount = invoiceAmount + "," + String.valueOf(resultSet.getFloat("AMOUNT"));
                invoiceDiscount = invoiceDiscount + "," + String.valueOf(resultSet.getFloat("DISCOUNT"));
                invoiceTaxableValue = invoiceTaxableValue + "," + String.valueOf(resultSet.getFloat("TAXABLE_VALUE"));
                invoiceCgstRate = invoiceCgstRate + "," + String.valueOf(resultSet.getFloat("CGST_RATE"));
                invoiceCgstAmount = invoiceCgstAmount + "," + String.valueOf(resultSet.getFloat("CGST_AMOUNT"));
                invoiceSgstRate = invoiceSgstRate + "," + String.valueOf(resultSet.getFloat("SGST_RATE"));
                invoiceSgstAmount = invoiceSgstAmount + "," + String.valueOf(resultSet.getFloat("SGST_AMOUNT"));
                invoiceTotal = invoiceTotal + "," + String.valueOf(resultSet.getFloat("TOTAL"));
                invoiceStock = invoiceStock + "," + String.valueOf(resultSet.getInt("STOCK"));
                invoiceTaxableCategory = invoiceTaxableCategory + "," + String.valueOf(resultSet.getInt("TAXABLECATEGORY"));
                invoiceMRP = invoiceMRP + "," + String.valueOf(resultSet.getFloat("MRP"));
                invoiceSalePrice = invoiceSalePrice + "," + String.valueOf(resultSet.getFloat("SALE_PRICE"));

            }
// this value pass to pojo constructur and that value get using observable list to table.

            list.add(new ADD_items_details(i, resultSet.getString("ITEMS"), resultSet.getString("HSN_CODE"), resultSet.getString("UNITS"), resultSet.getFloat("RATE"),
                    resultSet.getInt("QTY"), resultSet.getFloat("AMOUNT"), resultSet.getFloat("DISCOUNT"), resultSet.getFloat("TAXABLE_VALUE"),
                    resultSet.getFloat("CGST_RATE"), resultSet.getFloat("CGST_AMOUNT"), resultSet.getFloat("SGST_RATE"), resultSet.getFloat("SGST_AMOUNT"),
                    resultSet.getFloat("TOTAL"), resultSet.getInt("STOCK"), resultSet.getInt("TAXABLECATEGORY"), resultSet.getFloat("MRP"),
                    resultSet.getFloat("SALE_PRICE")));
            i++;
        }

        invoice_table.setItems(list);

        resultSet.close();

        //   Load net total 
        Statement statement = connection.createStatement();
        ResultSet resultSet1 = statement.executeQuery("SELECT SUM(RATE) as r ,SUM(QTY) as q , SUM(AMOUNT) as a,SUM(DISCOUNT) as d,SUM(TAXABLE_VALUE) as txtvalue ,SUM(CGST_AMOUNT) as c,SUM(SGST_AMOUNT) as s,SUM(TOTAL) as t FROM BILL_CALCULATION");
        while (resultSet1.next()) {

            net_Total_Rate.setText(String.valueOf(resultSet1.getFloat("r")));
            net_Total_Qty.setText(String.valueOf(resultSet1.getInt("q")));
            net_Total_Amount.setText(String.valueOf(resultSet1.getFloat("a")));
            net_Total_Discount.setText(String.valueOf(resultSet1.getFloat("d")));
            net_Total_Taxable_value.setText(String.valueOf(resultSet1.getFloat("txtvalue")));
            net_Total_Cgst_Amount.setText(String.valueOf(resultSet1.getFloat("c")));
            net_Total_Sgst_Amount.setText(String.valueOf(resultSet1.getFloat("s")));
            net_Total_Total.setText(String.valueOf(resultSet1.getFloat("t")));

            Global_Total = resultSet1.getFloat("t");

            PaidAmount.setText(String.valueOf(resultSet1.getFloat("t")));
            Globle_paidAmount = Float.parseFloat(PaidAmount.getText());
            GlobleOutStandingAmount = (Global_Total - Globle_paidAmount);
            OutstandingAmount.setText(String.valueOf(GlobleOutStandingAmount));

            items.clear();
            hsn_Code.clear();
            quantity.clear();
            amount.clear();
            //    units.
            cgst.clear();
            rate.clear();
            sgst.clear();
            total.clear();
            bar_code.clear();
            remaining_stock.clear();

            //---------------------------------------------------------------------- global variable set 0 default
            rateCalculationPurpose = 0;
            amountCalculationPrupose = 0;
            discountPerCalculationPrupose = 0;
            discountRupessCalculationPrupose = 0;
            taxableValueCalculationPrupose = 0;
            cgstRupessCalculationPurpose = 0;
            cgstPerCalculationPurpose = 0;
            sgstRupessCalculationPurpose = 0;
            sgstPerCalculationPurpose = 0;
            totalCalculationPrupose = 0;
            mrpCalculationPurpose = 0;
            qtyCalculationPurpose = 0;
            stockCalculationPurpose = 0;

            gstRupessCalculationPrupose = 0;
            gstPerCalculationPrupose = 0;
            salePriceCalculationPurpose = 0;
            //---------------------------------------------------------------------------------
            minSalePricemrpCalculationPurpose = null;
            purchasePriceCalculationPerpose = null;
            itemsCalculationPurpose = null;
            hsnCodeCalculationPurpose = null;
            unitCalculationPurpose = null;

            gsttaxableCategory = null;

        }

    }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ CLICK ON TABLE 
    @FXML
    private void setCellValueFromToTextField() throws SQLException {

        invoice_table.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                ADD_items_details list = invoice_table.getItems().get(invoice_table.getSelectionModel().getSelectedIndex());

                items.setText("" + list.getItem_name());
                itemsCalculationPurpose = list.getItem_name();

                hsn_Code.setText("" + list.getHsn_code());
                hsnCodeCalculationPurpose = list.getHsn_code();

                quantity.setText("" + list.getQty());
                qtyCalculationPurpose = list.getQty();

                units.getSelectionModel().select(list.getUnits());
                unitCalculationPurpose = list.getUnits();

                rate.setText("" + list.getRate());
                rateCalculationPurpose = list.getRate();

                amount.setText("" + list.getAmount());
                amountCalculationPrupose = list.getAmount();   //  when updating qty perticular amount is requried for calculation

//                costCalculationPurpose = list.getRate(); // used in searchRemainingStock method that will help to updating product. to maintain that product rate/cost.
                cgst.setText("" + list.getCgst_rupess());
                cgstRupessCalculationPurpose = list.getCgst_rupess();//// used in searchRemainingStockmethod that will help to updating product.
                cgstPerCalculationPurpose = list.getCgst_per();

                sgst.setText("" + list.getSgst_rupess());
                sgstRupessCalculationPurpose = list.getSgst_rupess();//// used in searchRemainingStock method that will help to updating product. 
                sgstPerCalculationPurpose = list.getSgst_per();

                total.setText("" + list.getTotal());
                totalCalculationPrupose = list.getTotal();//// used in searchRemainingStock method that will help to updating product. 

                remaining_stock.setText("" + list.getStock());
                stockCalculationPurpose = list.getStock();

                discountRupessCalculationPrupose = list.getDiscount_rupess();

                taxableValueCalculationPrupose = list.getTaxableValue();

                gstPerCalculationPrupose = list.getTaxableCategory();

                mrpCalculationPurpose = list.getMrp();
                salePriceCalculationPurpose = list.getSaleprice();

            }
        });

    }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ UPDATE TABLE
    @FXML
    private void upDateDetailsWithinTable(ActionEvent event) throws SQLException {

        PreparedStatement ps = connection.prepareStatement("UPDATE BILL_CALCULATION SET ITEMS= ?, HSN_CODE=? , UNITS=?, RATE=? , QTY=? , AMOUNT=? , DISCOUNT=? , TAXABLE_VALUE=? , CGST_RATE=? , CGST_AMOUNT=? , SGST_RATE=? , SGST_AMOUNT=? , TOTAL=? , STOCK=? , TAXABLECATEGORY=? , MRP=? , SALE_PRICE=? WHERE ITEMS = '" + itemsCalculationPurpose + "'");

        ps.setString(1, itemsCalculationPurpose);
        ps.setString(2, hsnCodeCalculationPurpose);
        ps.setString(3, unitCalculationPurpose);
        ps.setFloat(4, rateCalculationPurpose);
        ps.setInt(5, qtyCalculationPurpose);
        ps.setFloat(6, amountCalculationPrupose);
        ps.setFloat(7, discountRupessCalculationPrupose);
        ps.setFloat(8, taxableValueCalculationPrupose);
        ps.setFloat(9, cgstPerCalculationPurpose);
        ps.setFloat(10, cgstRupessCalculationPurpose);
        ps.setFloat(11, sgstPerCalculationPurpose);
        ps.setFloat(12, sgstRupessCalculationPurpose);
        ps.setFloat(13, totalCalculationPrupose);
        ps.setInt(14, stockCalculationPurpose);
        ps.setInt(15, gstPerCalculationPrupose);
        ps.setFloat(16, mrpCalculationPurpose);
        ps.setFloat(17, salePriceCalculationPurpose);

        int result = ps.executeUpdate();

        if (result > 0) {

            alert.setTitle("Validation");
            alert.setHeaderText(null);
            alert.setContentText("You are product update Successfully! ");
            alert.showAndWait();

            loadDataFromDatabaseToTable();

        } else {

            alert.setTitle("Validation");
            alert.setHeaderText(null);
            alert.setContentText("product can not update you can delete product and add modified product again ! ");
            alert.showAndWait();

        }

        System.out.println("flag 9 ");

    }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  DELETE TABLE
    @FXML
    private void deleteDetailsWithinTable(ActionEvent event) throws SQLException {

        PreparedStatement ps = connection.prepareStatement("delete from Bill_Calculation WHERE ITEMS =  ? ");
        ps.setString(1, itemsCalculationPurpose);
        int result = ps.executeUpdate();
        if (result > 0) {

            alert.setTitle("Validation");
            alert.setHeaderText(null);
            alert.setContentText("You are product Delete Successfully! ");
            alert.showAndWait();

            loadDataFromDatabaseToTable();
        }
        ps.close();

    }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   GENERATE INVOICE
    private void generateInvoice() {

        Date d = new Date(System.currentTimeMillis());
        String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(d);
        String[] d2 = modifiedDate.split("-");
        for (int i = 0; i < d2.length; i++) {
            invoiceYear = d2[0];
            invoicMonth = d2[1];
            invoicDay = d2[2];
        }
        YYYYMMDD = invoiceYear + invoicMonth + invoicDay;

    }

    private void chackLastInvoiceNumber() {
        try {

            PreparedStatement preparedStatement = connection.prepareStatement("select count(*) as c from INVOICE_DETAILS where INVOICE_NUMBER LIKE '" + YYYYMMDD + "%' ");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                LastInvoiceNumber = Integer.parseInt(resultSet.getString("c"));

            }

            LastInvoiceNumber = LastInvoiceNumber + 1;

            preparedStatement.close();

        } catch (SQLException sql) {
            sql.printStackTrace();
            System.out.println("DB connectivity issue");
        }

    }

    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ CHECK REMAIN STOCK as will as update cost
    @FXML
    public void searchRemainingStock(KeyEvent event) throws SQLException {

        //   int result = chack_Stock();
        try {

         //   qtyCalculationPurpose = (Integer.parseInt(quantity.getText()));
            qtyCalculationPurpose = (Integer.valueOf(quantity.getText()));
            rateCalculationPurpose = (Float.valueOf(rate.getText()));

            if (stockCalculationPurpose >= qtyCalculationPurpose) {
                remaining_stockdisplayPerpose = (stockCalculationPurpose - qtyCalculationPurpose);//1
                remaining_stock.setText(String.valueOf(remaining_stockdisplayPerpose));

                //  addition on total when qty increased amount is increased (qty*rate) , when amount is incresed total is increased including gst  
                amountCalculationPrupose = (rateCalculationPurpose * qtyCalculationPurpose);

                taxableValueCalculationPrupose = amountCalculationPrupose;
                String temp1 = String.format("%.2f", amountCalculationPrupose);
                amount.setText(String.valueOf(temp1));

//------------------------------------------------------------------------------------------ gst calculation on Amount
                float gst = ((amountCalculationPrupose / 100) * gstPerCalculationPrupose);

                float DivGST = gst / 2;
                cgstRupessCalculationPurpose = DivGST;
                sgstRupessCalculationPurpose = DivGST;
                cgst.setText(String.valueOf(cgstRupessCalculationPurpose));
                sgst.setText(String.valueOf(sgstRupessCalculationPurpose));

                totalCalculationPrupose = (amountCalculationPrupose + gst); // here total include  gst         
                String temp5 = String.format("%.2f", totalCalculationPrupose);
                total.setText(String.valueOf(temp5));

//-------------------------------------------------------------------------   calculate discount per quantity on sealing price and mrp
                float tempmrp = (mrpCalculationPurpose * qtyCalculationPurpose);
                float tempscalingprice = (salePriceCalculationPurpose * qtyCalculationPurpose);
                discountRupessCalculationPrupose = (tempmrp - tempscalingprice); // when you increase quentity discount also increased... ( dicont for multiple product)
            } else {

                alert.setTitle("Validate owner name");
                alert.setHeaderText(null);
                alert.setContentText("Pleass Enter Valide Stock");
                alert.showAndWait();
                quantity.setText("1");
            }
//---------------------------------------------------------------------------------------------------------------
        } catch (NumberFormatException e) {
            System.out.println(" Problem in  searchRemainingStock method ");
            e.printStackTrace();

        }

    }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  PRINT
    @FXML
    private void bargainingInTotalAmount(ActionEvent event) {

        // Reverse Calculation on when coustomer barging in total cost 
//-----------------------------------------------------------------------------------------------
        totalCalculationPrupose = Float.parseFloat(total.getText());
        qtyCalculationPurpose = Integer.parseInt(quantity.getText());

        amountCalculationPrupose = (totalCalculationPrupose / qtyCalculationPurpose);  // finding single value for  gst calculation

        float gst = ((amountCalculationPrupose / (100 + gstPerCalculationPrupose)) * gstPerCalculationPrupose); // calcuate gst on that amount

        rateCalculationPurpose = (amountCalculationPrupose - gst);      // subtract gst on that cost and get rate

        rate.setText(String.valueOf(rateCalculationPurpose));    // show new rate after total changes

        amountCalculationPrupose = rateCalculationPurpose * qtyCalculationPurpose;   // but that rate multiply by qty and get new amount  

        amount.setText(String.valueOf(amountCalculationPrupose));  //  show new amount after qty multiply .

        float gstagain = ((amountCalculationPrupose / 100) * gstPerCalculationPrupose); // calculate new gst on that amount 

        float DivGST = gstagain / 2;                               // divide gst for two part because central gst and state gst.
        cgstRupessCalculationPurpose = DivGST;
        sgstRupessCalculationPurpose = DivGST;
        String temp1 = String.format("%.2f", DivGST);
        cgst.setText(String.valueOf(temp1));
        sgst.setText(String.valueOf(temp1));  //   show new sgst and cgst

        totalCalculationPrupose = (amountCalculationPrupose + gstagain); //   addition  on new amount and new gst  and get total
        String temp4 = String.format("%.2f", totalCalculationPrupose);
        total.setText(String.valueOf(temp4));    //  show new total to user that total must be same as first time entered total amount.

        //-------------------------------------------------------------------------   calculate discount per quantity
        float tempmrp = (mrpCalculationPurpose * qtyCalculationPurpose);
        float tempscalingprice = (salePriceCalculationPurpose * qtyCalculationPurpose);
        discountRupessCalculationPrupose = (tempmrp - tempscalingprice); // when you increase quentity discount also increased... ( dicont for multiple product
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
    }

    @FXML
    private void clearInvoice(ActionEvent event) throws SQLException {

        ClearTable();

    }

    @FXML
    private void searchBarcodeItems(KeyEvent event) {

        try {

            String query = "select * from add_items_details where BAR_CODE = '" + bar_code.getText() + "'";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                itemsCalculationPurpose = resultSet.getString("ITEMS_NAME");
                hsnCodeCalculationPurpose = resultSet.getString("HSN_CODE");
                unitCalculationPurpose = resultSet.getString("UNITS");
                rateCalculationPurpose = Float.parseFloat(resultSet.getString("RATE"));

                stockCalculationPurpose = Integer.parseInt(resultSet.getString("STOCK"));

                salePriceCalculationPurpose = Float.parseFloat(resultSet.getString("SALE_PRICE"));

                purchasePriceCalculationPerpose = resultSet.getString("PURCHASE_PRICE");

                mrpCalculationPurpose = Float.parseFloat(resultSet.getString("MRP"));
                minSalePricemrpCalculationPurpose = resultSet.getString("MIN_SALE");

                gsttaxableCategory = resultSet.getString("TAXT_CATEGORY");

                cgstPerCalculationPurpose = Float.parseFloat(resultSet.getString("CGST_PER"));
                cgstRupessCalculationPurpose = Float.parseFloat(resultSet.getString("CGST_RUPESS"));
                sgstPerCalculationPurpose = Float.parseFloat(resultSet.getString("SGST_PER"));
                sgstRupessCalculationPurpose = Float.parseFloat(resultSet.getString("SGST_RUPESS"));

                gstPerCalculationPrupose = Integer.parseInt(resultSet.getString("GST_PER"));

                gstRupessCalculationPrupose = Float.parseFloat(resultSet.getString("GST_RUPESS"));

                discountRupessCalculationPrupose = Float.parseFloat(resultSet.getString("DISCOUNT_RUPESS"));
                discountPerCalculationPrupose = Float.parseFloat(resultSet.getString("DISCOUNT_PER"));

                items.setText(itemsCalculationPurpose);
                hsn_Code.setText(hsnCodeCalculationPurpose);

                rate.setText(String.valueOf(rateCalculationPurpose));   //  salePriceCalculationPurpose =>    including gst

                qtyCalculationPurpose = 1;   // when you add new item than deafult quantity =1; requried  when qty change that time remainstock method invocked.

                quantity.setText(String.valueOf(qtyCalculationPurpose));

                units.setValue(unitCalculationPurpose);

            }

            // update stock from table after they added in table .
            // when he print invoice commit stock from table
            try {

                remaining_stockdisplayPerpose = (stockCalculationPurpose - qtyCalculationPurpose);

                remaining_stock.setText(String.valueOf(remaining_stockdisplayPerpose));

                //  addition on total when qty increased amount is increased (qty*rate) , when amount is incresed total is increased including gst  
                amountCalculationPrupose = (rateCalculationPurpose * qtyCalculationPurpose);

                taxableValueCalculationPrupose = amountCalculationPrupose;

                String temp1 = String.format("%.2f", amountCalculationPrupose);
                amount.setText(String.valueOf(temp1));

                cgst.setText(String.valueOf(cgstRupessCalculationPurpose));
                sgst.setText(String.valueOf(sgstRupessCalculationPurpose));

                totalCalculationPrupose = (amountCalculationPrupose + gstRupessCalculationPrupose);
                String total2 = String.format("%.2f", totalCalculationPrupose);
                total.setText(String.valueOf(total2));

            } catch (NumberFormatException e) {
                System.out.println(" Problem in  searchRemainingStock method ");
                e.printStackTrace();

            }

        } catch (SQLException ex) {
            Logger.getLogger(GenBillController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void ClearTable() throws SQLException {

        //PreparedStatement preparedStatement = connection.prepareStatement("TRUNCATE TABLE BILL_CALCULATION");
        Statement statement = connection.createStatement();

        int result = statement.executeUpdate("DELETE From BILL_CALCULATION");
        if (result == 0) {
            Business_Name.clear();
            customer_Name.clear();
            GST_Number.clear();
            mobile_Number.clear();
            address.clear();
            items.clear();
            hsn_Code.clear();
            quantity.clear();
            amount.clear();
            //    units.
            cgst.clear();
            rate.clear();
            sgst.clear();
            total.clear();

            remaining_stock.clear();
            bar_code.clear();
            PaidAmount.clear();
            OutstandingAmount.clear();

            invoiceItemName = "";
            invoiceHsnCode = "";
            invoiceUnit = "";
            //all float from database
            invoiceRate = "";
            invoiceAmount = "";
            invoiceDiscount = "";
            invoiceTaxableValue = "";
            invoiceCgstRate = "";
            invoiceCgstAmount = "";
            invoiceSgstRate = "";
            invoiceSgstAmount = "";
            invoiceTotal = "";
            invoiceMRP = "";
            invoiceSalePrice = "";
            // all int from databse
            invoiceQty = "";
            invoiceStock = "";
            invoiceTaxableCategory = "";
            t = 1;

            loadDataFromDatabaseToTable();

        }
    }

    @FXML
    private void paidAmountAction() {

        Globle_paidAmount = Float.parseFloat(PaidAmount.getText());
        GlobleOutStandingAmount = (Global_Total - Globle_paidAmount);
        OutstandingAmount.setText(String.valueOf(GlobleOutStandingAmount));

//        if (GlobleOutStandingAmount <= 0) {
//            //Enable Invoice     
//            ButtoninvoicePrint.setDisable(false);
//            // Dsible recipt
//            ButtonReciptInvoice.setDisable(true);
//
//        }
//        if (GlobleOutStandingAmount > 0) {
//
//            // disible invoice
//            ButtoninvoicePrint.setDisable(true);
//            //Enable recipt
//            ButtonReciptInvoice.setDisable(false);
//        }
    }

    @FXML
    private void printTempraryReceipt(ActionEvent event) throws ClassNotFoundException, SQLException, ParseException {
        printInvoice();
    }

    @FXML
    private void printInvoice() throws ClassNotFoundException, SQLException, ParseException {

        GennInvoice gennInvoice = new GennInvoice();

        Connection con = DatabaseConnection.getConnection();

        con.setAutoCommit(false);

        if (PaidAmount.getText().isEmpty()) {

            alert.setTitle("Validate");
            alert.setHeaderText(null);
            alert.setContentText("Enter Paid Amount");
            alert.showAndWait();
        } else {
            int result2 = 0;
            PreparedStatement ps = con.prepareStatement("insert into INVOICE_DETAILS values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
// Store global variable here 
            PreparedStatement ps2 = null;

            LocalDate localDate = date.getValue();

//            DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd/MM/uuuu");
//            String text = localDate.format(formatters);
//            LocalDate parsedDate = LocalDate.parse(text, formatters);
            ps.setString(1, invoice_Number.getText());

            ps.setDate(2, (java.sql.Date.valueOf(localDate)));
            ps.setString(3, GST_Number.getText()); //  Pan Number
            ps.setString(4, mobile_Number.getText());
            ps.setString(5, address.getText());
            ps.setString(6, Business_Name.getText()); // this is buyer  we can add contact persoin in invoice for retriving puropse Gst also add
            ps.setString(7, invoiceItemName);
            ps.setString(8, invoiceHsnCode);
            ps.setString(9, invoiceRate);
            ps.setString(10, invoiceQty);
            ps.setString(11, invoiceAmount);
            ps.setString(12, invoiceDiscount);
            ps.setString(13, invoiceTaxableValue);
            ps.setString(14, invoiceUnit);  // unit
            ps.setString(15, invoiceCgstRate);
            ps.setString(16, invoiceCgstAmount);
            ps.setString(17, customer_Name.getText()); //Contact Person
            ps.setString(18, invoiceSgstRate);
            ps.setString(19, invoiceSgstAmount);
            ps.setString(20, invoiceTotal);
            ps.setString(21, net_Total_Qty.getText());
            ps.setString(22, net_Total_Amount.getText());
            ps.setString(23, net_Total_Discount.getText());
            ps.setString(24, net_Total_Taxable_value.getText());
            ps.setString(25, net_Total_Cgst_Amount.getText());
            ps.setString(26, net_Total_Sgst_Amount.getText());
            ps.setString(27, net_Total_Total.getText());
            ps.setString(28, PaidAmount.getText());
            ps.setString(29, OutstandingAmount.getText());

            if (Float.parseFloat(OutstandingAmount.getText()) > 0.0) {

                status = "Pending";

                ps2 = con.prepareStatement("insert into RECEIPT_DETAILS values (?,?,?,?,?,?,?,?,?,?,?)");

                receiptNumber();// update receipt number  
                ps2.setInt(1, receipt_Number);
                ps2.setDate(2, (java.sql.Date.valueOf(localDate)));
                ps2.setString(3, invoice_Number.getText());
                ps2.setString(4, Business_Name.getText());
                ps2.setString(5, mobile_Number.getText());
                ps2.setString(6, net_Total_Total.getText());
                ps2.setString(7, PaidAmount.getText());
                ps2.setString(8, OutstandingAmount.getText());
                ps2.setString(9, status);
                ps2.setString(10, address.getText());

                float temp = Float.parseFloat(net_Total_Total.getText() + "f");  // convert string value to float 
                int temp2 = Math.round(temp);  // methos input paramater is int thats why convert in int using round function because of that get nearest fix value.
                NumToWord obj = new NumToWord();
                String Worldofnumber = obj.convert((int) temp2);  // THIS method convert int value to word and return result in string format 

                ps2.setString(11, Worldofnumber + " Ruppes Only /-");   /// stored result in database.

                result2 = ps2.executeUpdate();

            }
            if (Float.parseFloat(OutstandingAmount.getText()) == 0.0) {

                status = "Paid";

            }

            ps.setString(30, status);

            int result = ps.executeUpdate();

            if (result > 0) {

                if (result2 > 0) {
                    alert.setTitle("Validate");
                    alert.setHeaderText(null);
                    alert.setContentText(" Reciept Store successfuly because of amount outstanding ");
                    alert.showAndWait();
                }

                alert.setTitle("Validate");
                alert.setHeaderText(null);
                alert.setContentText(" Invoice Store successfuly ");
                alert.showAndWait();
                
                con.commit();
                
                
                ClearTable();
                //  clear screen first.
                 
                myinitializeMethod();
                
                // called print here 
               

            } else {
                alert.setTitle("Validate");
                alert.setHeaderText(null);
                alert.setContentText(" Invoice not Store ");
                alert.showAndWait();
                con.rollback();

            }

            ps.close();
            ps2.close();

            //gennInvoice.printInvoiceHTMLFormat();
        }
    }

    public void myinitializeMethod() {

        //  ---------Create connection object for all method.
        connection = DatabaseConnection.getConnection();

        try {
            ClearTable();

        } catch (SQLException ex) {
            Logger.getLogger(GenBillController.class.getName()).log(Level.SEVERE, null, ex);
        }

// -------- Pojo linking with table variable name to be appand that pojo value to them.
        //table_Items.setCellValueFactory(new PropertyValueFactory<>("items"));   // check here itemname
        table_Sr_No.setCellValueFactory(new PropertyValueFactory<>("SrNo"));
        table_Sr_No.setStyle("-fx-alignment: CENTER");

        table_Items.setCellValueFactory(new PropertyValueFactory<>("item_name"));

        table_Hsn_Code.setCellValueFactory(new PropertyValueFactory<>("hsn_code"));
        table_Hsn_Code.setStyle("-fx-alignment: CENTER");
        table_Units.setCellValueFactory(new PropertyValueFactory<>("units"));
        table_Units.setStyle("-fx-alignment: CENTER");
        table_Rate.setCellValueFactory(new PropertyValueFactory<>("rate"));
        table_Rate.setStyle("-fx-alignment: CENTER");
        table_Qty.setCellValueFactory(new PropertyValueFactory<>("qty"));
        table_Qty.setStyle("-fx-alignment: CENTER");
        table_Amount.setCellValueFactory(new PropertyValueFactory<>("amount"));
        table_Amount.setStyle("-fx-alignment: CENTER");
        table_Discount.setCellValueFactory(new PropertyValueFactory<>("discount_rupess"));
        table_Discount.setStyle("-fx-alignment: CENTER");
        table_Taxable_Value.setCellValueFactory(new PropertyValueFactory<>("taxableValue"));
        table_Taxable_Value.setStyle("-fx-alignment: CENTER");
        table_Cgst_Rate.setCellValueFactory(new PropertyValueFactory<>("cgst_per"));
        table_Cgst_Rate.setStyle("-fx-alignment: CENTER");
        table_Cgst_Amount.setCellValueFactory(new PropertyValueFactory<>("cgst_rupess"));
        table_Cgst_Amount.setStyle("-fx-alignment: CENTER");
        table_Sgst_Rate.setCellValueFactory(new PropertyValueFactory<>("sgst_per"));
        table_Sgst_Rate.setStyle("-fx-alignment: CENTER");
        table_Sgst_Amount.setCellValueFactory(new PropertyValueFactory<>("sgst_rupess"));
        table_Sgst_Amount.setStyle("-fx-alignment: CENTER");
        table_Total.setCellValueFactory(new PropertyValueFactory<>("total"));
        table_Total.setStyle("-fx-alignment: CENTER");

//----------------------   field  editable .
        invoice_table.setEditable(true);
        Business_Name.setEditable(true);
        customer_Name.setEditable(true);
        items.setEditable(true);
        date.setEditable(true);
        quantity.setEditable(true);
        total.setEditable(true);

//-do not requried this field  editable .
        invoice_Number.setEditable(false);
        remaining_stock.setEditable(false);
        hsn_Code.setEditable(false);
        rate.setEditable(false);
        cgst.setEditable(false);
        sgst.setEditable(false);
        units.setEditable(false);

        //---------------------- call mouse click event here 
        try {

            setCellValueFromToTextField();
        } catch (SQLException ex) {
            Logger.getLogger(GenBillController.class.getName()).log(Level.SEVERE, null, ex);
        }
//---------------------------------------------------  track curent system date and that help to create invoice number logic.
        date.setConverter(new StringConverter<LocalDate>() {

            String pattern = "dd-MM-yyyy";

            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            {

                date.setPromptText(pattern.toLowerCase());

            }

            @Override

            public String toString(LocalDate date) {

                if (date != null) {

                    return dateFormatter.format(date);

                } else {

                    return "";

                }

            }

            @Override

            public LocalDate fromString(String string) {

                if (string != null && !string.isEmpty()) {

                    return LocalDate.parse(string, dateFormatter);

                } else {

                    return null;

                }

            }

        });

        date1 = new Date(System.currentTimeMillis());
//        String modifiedDate = new SimpleDateFormat("dd-MM-yy").format(date1);

        date.setValue(LocalDate.now());

        generateInvoice();
        chackLastInvoiceNumber();

        invoice_Number.setText(YYYYMMDD + LastInvoiceNumber);

//----------------------------------------// Auto complete Items  list code ----------------------------------------------------
        ObservableList list2 = FXCollections.observableArrayList();
        try {

            connection = DatabaseConnection.getConnection();

            String query = "select items_name from add_items_details";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                list2.add(resultSet.getString("items_name"));
            }

            TextFields.bindAutoCompletion(items, list2);

            preparedStatement.close();

        } catch (SQLException ex) {
            Logger.getLogger(GenBillController.class.getName()).log(Level.SEVERE, null, ex);
        }

//---------------------------------------Auto complete Customer  list code---------------------------------------------------
        ObservableList list3 = FXCollections.observableArrayList();
        try {

            connection = DatabaseConnection.getConnection();

            String query3 = "select CONTACT_PERSON from CUSTOMER_ACCOUNT";
            PreparedStatement preparedStatement3 = connection.prepareStatement(query3);
            ResultSet resultSet3 = preparedStatement3.executeQuery();

            while (resultSet3.next()) {

                list3.add(resultSet3.getString("CONTACT_PERSON"));
            }

            TextFields.bindAutoCompletion(customer_Name, list3);

            preparedStatement3.close();

        } catch (SQLException ex) {
            Logger.getLogger(GenBillController.class.getName()).log(Level.SEVERE, null, ex);
        }

        //-----------------------------------------------------------------------------------------     
        ObservableList list4 = FXCollections.observableArrayList();
        try {

            connection = DatabaseConnection.getConnection();

            String query4 = "select BUSINESS_NAME from CUSTOMER_ACCOUNT";
            PreparedStatement preparedStatement4 = connection.prepareStatement(query4);
            ResultSet resultSet4 = preparedStatement4.executeQuery();

            while (resultSet4.next()) {

                list4.add(resultSet4.getString("BUSINESS_NAME"));
            }

            TextFields.bindAutoCompletion(Business_Name, list4);

            preparedStatement4.close();

        } catch (SQLException ex) {
            Logger.getLogger(GenBillController.class.getName()).log(Level.SEVERE, null, ex);
        }

        //-----------------------------------------------------------------------------------------     
        bar_code.requestFocus();

    }

    @FXML
    private void backInvoice(ActionEvent event) {
        if (t == 1) {

            alert.setTitle("Back Invoice ");
            alert.setHeaderText(null);
            alert.setContentText("Date Format Must be DD-MM-YY Exmaple 31-12-2017 . ( - ) dash is important for Separator Enter For Invoice Number");
            alert.showAndWait();
            t++;
        }

        String d = String.valueOf(date.getValue());

        String[] d2 = d.split("-");
        for (int i = d2.length - 1; i >= 0; i--) {
            invoiceYear = d2[2];
            invoicMonth = d2[1];
            invoicDay = d2[0];

            backYYMMDD = invoicDay + invoicMonth + invoiceYear;

            try {
                PreparedStatement preparedStatement = connection.prepareStatement("select count(*) as c from INVOICE_DETAILS where INVOICE_NUMBER LIKE '" + backYYMMDD + "%' ");
                ResultSet resultSet = preparedStatement.executeQuery();
                System.out.println("Generate back");
                while (resultSet.next()) {

                    LastInvoiceNumber = Integer.parseInt(resultSet.getString("c"));

                }

                alert.setTitle("Validate owner name");
                alert.setHeaderText(null);
                alert.setContentText("" + LastInvoiceNumber + "");
                alert.showAndWait();
                LastInvoiceNumber = LastInvoiceNumber + 1;

                invoice_Number.setText(backYYMMDD + LastInvoiceNumber);

                preparedStatement.close();

            } catch (SQLException sql) {
                sql.printStackTrace();
                System.out.println("DB connectivity issue");
            }

        }

    }

    private void receiptNumber() {
        try {

            PreparedStatement preparedStatement = connection.prepareStatement("select COUNT(*) as c from RECEIPT_DETAILS ");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                receipt_Number = Integer.parseInt(resultSet.getString("c"));

            }

            receipt_Number = receipt_Number + 1;

            preparedStatement.close();

        } catch (SQLException sql) {
            sql.printStackTrace();
            System.out.println("DB connectivity issue");
        }
    }
}
