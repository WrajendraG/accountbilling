/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account_billing;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author ACER
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private BorderPane pane;
    @FXML
    private Pane framepane;
    @FXML
    private Menu bill;
    @FXML
    private MenuItem canBill;
    @FXML
    private HBox hbox;
    @FXML
    private Button home_btn;
    @FXML
    private Button Shortcut_AddItems;
    @FXML
    private Button Shortcut_View_Edit_Delete_Item;
    @FXML
    private Button Shortcut_CreateAccount;
    @FXML
    private Button Shortcut_View_Edit_Active_Deactive_Account;
    @FXML
    private Button Shortcut_BalanceStock;
    @FXML
    private Button Shortcut_NilStock;
    @FXML
    private Button Shortcut_GenerateInvoice;
    @FXML
    private Button Shortcut_view_edit_canceled;
    @FXML
    private Button Shortcut_DaytodayWise;
    @FXML
    private Button Shortcut_Daytodaysale;
    @FXML
    private Button Shortcut_invoiceDetails;
    @FXML
    private Button Shortcut_Backup;
    @FXML
    private Button Shortcut_CleanUp;
    @FXML
    private Button Shortcut_Install;
    @FXML
    private Button Shortcut_Exit;

    @FXML
    private MenuItem MenuAddItems;
    @FXML
    private MenuItem MenuHome;
    @FXML
    private Button Shortcut_receiptView;

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@Manu bar @@@@@@@@@@@@@
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        
        try {
            menuHomeAction();
            // TODO
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @FXML
    private void dayWiseMenu(ActionEvent event) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("DayWiseSale.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void daytodayMenu(ActionEvent event) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("DayToDaySale.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void addItemKeyEvent(KeyEvent event) throws Exception {
        KeyCode keyCode = event.getCode();
        Parent parent = FXMLLoader.load(getClass().getResource("AddItems.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void nilMenu(ActionEvent event) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("Nill_Stock.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void cleanupMenu(ActionEvent event) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("CleanUp.fxml"));
        framepane.getChildren().setAll(parent);

    }

    @FXML
    private void backupMenu(ActionEvent event) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("BackUp.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void genbilMenu(ActionEvent event) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("GenBill.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void balstockMenu(ActionEvent event) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("Balance_Stock.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void ChangePasswordAction(ActionEvent event) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("ChangePassword.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void personalSettingMenu(ActionEvent event) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("PersonalSettings.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void edititemMenu(ActionEvent event) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("Edit_Delete_Items.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void homeWindows(ActionEvent event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("HomeWindows.fxml"));
        framepane.getChildren().setAll(parent);
    }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @FXML
    private void register_Details_Menu(ActionEvent event) throws Exception {
        // Parent root = FXMLLoader.load(getClass().getResource("/updatework/License_Agreement.fxml"));

        Parent parent = FXMLLoader.load(getClass().getResource("Login_Update_Register_details.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void business_Details_Menu(ActionEvent event) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("Login_Update_Business_Details.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void other_Details_Menu(ActionEvent event) throws Exception {

        Parent parent = FXMLLoader.load(getClass().getResource("Login_Update_Other_Details.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void addItemMenu(Event event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("AddItems.fxml"));
        framepane.getChildren().setAll(parent);

    }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @FXML
    private void createAccounts(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("CreateAccount.fxml"));
        framepane.getChildren().setAll(parent);

    }

    @FXML
    private void viewAccounts(ActionEvent event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("ViewCreatedAccount.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void genarateBarcode(ActionEvent event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("GenerateBarcode2.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void viewEditCancelInvoiceAction(ActionEvent event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("View_Edit_Cancel_Invoice.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void Shortcut_AddItemsAction(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("AddItems.fxml"));
        framepane.getChildren().setAll(parent);

    }

    @FXML
    private void Shortcut_View_Edit_Delete_ItemAction(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("Edit_Delete_Items.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void Shortcut_CreateAccountAction(ActionEvent event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("CreateAccount.fxml"));
        framepane.getChildren().setAll(parent);

    }

    @FXML
    private void Shortcut_View_Edit_Active_Deactive_AccountAction(ActionEvent event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("ViewCreatedAccount.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void Shortcut_BalanceStockAction(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("Balance_Stock.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void Shortcut_NilStockAction(ActionEvent event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("Nill_Stock.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void Shortcut_GenerateInvoiceAction(ActionEvent event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("GenBill.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void Shortcut_view_edit_canceledAction(ActionEvent event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("View_Edit_Cancel_Invoice.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void Shortcut_DaytodayWiseAction(ActionEvent event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("DayToDaySale.fxml"));
        framepane.getChildren().setAll(parent);

    }

    @FXML
    private void Shortcut_DaytodaysaleAction(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("DayWiseSale.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void Shortcut_invoiceDetailsAction(ActionEvent event) {

    }

    @FXML
    private void Shortcut_BackupAction(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("BackUp.fxml"));
        framepane.getChildren().setAll(parent);

    }

    @FXML
    private void Shortcut_CleanUpAction(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("CleanUp.fxml"));
        framepane.getChildren().setAll(parent);

    }

    @FXML
    private void Shortcut_InstallAction(ActionEvent event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("Install.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void Shortcut_ExitAction(ActionEvent event) {
        System.exit(0);

    }

    @FXML
    private void menuAddItemsAction(ActionEvent event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("AddItems.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void menuHomeAction() throws IOException {
       
        Parent parent = FXMLLoader.load(getClass().getResource("HomeWindows.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void OnmenuHomeValidationAction(Event event) throws IOException {
        
        Parent parent = FXMLLoader.load(getClass().getResource("HomeWindows.fxml"));
        framepane.getChildren().setAll(parent);
    }
    
    
        @FXML
    private void Create_Supplier_Sub_Menu(ActionEvent event) throws IOException {
        System.out.println("yes we are at right place");
        Parent parent = FXMLLoader.load(getClass().getResource("CreateAccountForSupplier.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void VIew_Supplier_Sub_Menu(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("ViewAccountForSupplier.fxml"));
        framepane.getChildren().setAll(parent);
    }

    public void loadhomepage() throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("HomeWindows.fxml"));
        framepane.getChildren().setAll(parent);

    }

    @FXML
    private void Create_Purchase_Sub_Menu(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("CreatePurchase.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void View_Purchase_Sub_Menu(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("ViewPurchase.fxml"));
        framepane.getChildren().setAll(parent);
    }

    public void check() throws IOException {
        System.out.println("am in checkrj");
        Parent parent = FXMLLoader.load(getClass().getResource("HomeWindows.fxml"));
        framepane.getChildren().setAll(parent);
    }


    @FXML
    private void receiptViewAction(ActionEvent event) throws IOException {
           Parent parent = FXMLLoader.load(getClass().getResource("ReceiptView.fxml"));
        framepane.getChildren().setAll(parent);
    }

    @FXML
    private void Shortcut_receiptViewAction(ActionEvent event) throws IOException {
           Parent parent = FXMLLoader.load(getClass().getResource("ReceiptView.fxml"));
        framepane.getChildren().setAll(parent);
    }

}
