package account_billing;

import com.osiersystems.pojos.Edit_Delete_Items;
import com.osiersystems.pojos.InvoiceModification;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import requriedDatabaseConnection.DatabaseConnection;

/**
 * FXML Controller class
 *
 * @author Niteen
 */
public class View_Edit_Cancel_InvoiceController implements Initializable {

    @FXML
    private TextField Text_Field_InvoiceNumber;
    @FXML
    private Button Button_SearchInvoice;
    @FXML
    private RadioButton radio_Box_Allinvoices;
    @FXML
    private RadioButton radio_Box_ByInvoiceNumber;
    @FXML
    private TableView<InvoiceModification> Table_Invoice;
    @FXML
    private TableColumn<InvoiceModification, Integer> Table_InvoiceSrNo;
    @FXML
    private TableColumn<InvoiceModification, String> Table_InvoiceNumber;
    @FXML
    private TableColumn<InvoiceModification, String> Table_PersonName;
    @FXML
    private TableColumn<InvoiceModification, String> Table_DateOfInvoice;
    @FXML
    private TableColumn<InvoiceModification, String> Table_Total_Amount;
    @FXML
    private TableColumn<InvoiceModification, String> Table_Paid_Amount;
    @FXML
    private TableColumn<InvoiceModification, String> Table_Outstanding_Amount;
    @FXML
    private TableColumn<InvoiceModification, String> Table_InvoiceStatus;
    @FXML
    private ToggleGroup radio_Box_InvoiceFilter;

    /**
     * Initializes the controller class.
     */
    Connection connection;

    String Global_SqlQuery = null;
    String Global_Table_InvoiceNumber;
    String Global_Table_PersonName;
    String Global_Table_DateOfInvoice;
    String Global_Table_Total_Amount;
    String Global_Table_InvoiceStatus;
    String Globle_Table_Paid_Amount;
    String Globle_Table_Outstanding_Amount;
    Alert alert = new Alert(Alert.AlertType.WARNING);

    String Categeory;
    boolean flag = false;

    Stage stage;
    Parent p;
    @FXML
    private RadioButton radio_Box_CanceledInvoice;
    @FXML
    private RadioButton radio_Box_OutstandingAmountInvoice;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        connection = DatabaseConnection.getConnection();

        Table_InvoiceSrNo.setCellValueFactory(new PropertyValueFactory<>("Pojo_InvoiceSrNo"));
        Table_InvoiceSrNo.setStyle("-fx-alignment: CENTER");

        Table_InvoiceNumber.setCellValueFactory(new PropertyValueFactory<>("Pojo_InvoiceNumber"));
        Table_InvoiceNumber.setStyle("-fx-alignment: CENTER");

        Table_PersonName.setCellValueFactory(new PropertyValueFactory<>("Pojo_PersonName"));
        Table_PersonName.setStyle("-fx-alignment: LEFT");

        Table_DateOfInvoice.setCellValueFactory(new PropertyValueFactory<>("Pojo_DateOfInvoice"));
        Table_DateOfInvoice.setStyle("-fx-alignment: CENTER");

        Table_Total_Amount.setCellValueFactory(new PropertyValueFactory<>("Pojo_Total_Amount"));
        Table_Total_Amount.setStyle("-fx-alignment: CENTER");

        Table_Paid_Amount.setCellValueFactory(new PropertyValueFactory<>("Pojo_Paid_Amount"));
        Table_Paid_Amount.setStyle("-fx-alignment: CENTER");

        Table_Outstanding_Amount.setCellValueFactory(new PropertyValueFactory<>("Pojo_Outstanding_Amount"));
        Table_Outstanding_Amount.setStyle("-fx-alignment: CENTER");

        Table_InvoiceStatus.setCellValueFactory(new PropertyValueFactory<>("Pojo_InvoiceStatus"));
        Table_InvoiceStatus.setStyle("-fx-alignment: CENTER");

            clickOnMouseOnTableInvoice();
            
        try {
        
        
            radio_Box_AllinvoicesAction();
            Text_Field_InvoiceNumber.setDisable(false);
            Button_SearchInvoice.setDisable(false);
        } catch (SQLException ex) {
            Logger.getLogger(View_Edit_Cancel_InvoiceController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void radio_Box_AllinvoicesAction() throws SQLException {

        if (radio_Box_Allinvoices.isSelected() || radio_Box_ByInvoiceNumber.isSelected()) {
            flag = false;  //  reinitialize again because flag updated (true) at the time of all bill load .
            Text_Field_InvoiceNumber.setDisable(true);
            Button_SearchInvoice.setDisable(true);

            Global_SqlQuery = "SELECT * FROM  INVOICE_DETAILS";

            loadDataFromDatabaseToTable();

        }

    }

    @FXML
    private void radio_Box_CanceledInvoiceAction(ActionEvent event) throws SQLException {

        if (radio_Box_CanceledInvoice.isSelected()) {
            flag = false;  //  reinitialize again because flag updated (true) at the time of all bill load .
            Text_Field_InvoiceNumber.setDisable(true);
            Button_SearchInvoice.setDisable(true);

            Global_SqlQuery = "select * From INVOICE_DETAILS where STATUS = 'Canceled' ";
            loadDataFromDatabaseToTable();

            if (flag == false) {

                alert.setTitle("Chacking Items");
                alert.setHeaderText(null);
                alert.setContentText("Canceled type of Invoice Not Found");
                alert.showAndWait();
            }

        }
    }

    @FXML
    private void radio_Box_OutstandingAmountInvoiceAction(ActionEvent event) throws SQLException {

        if (radio_Box_OutstandingAmountInvoice.isSelected()) {

            flag = false;  //  reinitialize again because flag updated (true) at the time of all bill load .
            Text_Field_InvoiceNumber.setDisable(true);
            Button_SearchInvoice.setDisable(true);

            Global_SqlQuery = "select * From INVOICE_DETAILS where STATUS = 'Pending'";
            loadDataFromDatabaseToTable();

            if (flag == false) {

                alert.setTitle("Chacking Items");
                alert.setHeaderText(null);
                alert.setContentText("Oustanding type of Invoice Not Found");
                alert.showAndWait();

            }

        }
    }

    @FXML
    private void radio_Box_ByInvoiceNumberAction() throws SQLException {

        Text_Field_InvoiceNumber.setDisable(false);
        Button_SearchInvoice.setDisable(false);

    }

    @FXML
    private void SearchInvoiceAction(ActionEvent event) throws SQLException {

        if (Text_Field_InvoiceNumber.getText().isEmpty()) {

            alert.setTitle("Note");
            alert.setHeaderText(null);
            alert.setContentText("Please Select By Invoice RadioBox And Enter Invoice within Text Field !");
            alert.showAndWait();

        } else {

            if (radio_Box_ByInvoiceNumber.isSelected()) {
                flag = false;  //  reinitialize again because flag updated (true) at the time of all bill load .
                Global_SqlQuery = "select * From INVOICE_DETAILS where INVOICE_NUMBER = '" + Text_Field_InvoiceNumber.getText() + "' ";
                loadDataFromDatabaseToTable();

                if (flag == false) {

                    alert.setTitle("Chacking Items");
                    alert.setHeaderText(null);
                    alert.setContentText("This type of Invoice Not Found");
                    alert.showAndWait();

                }

            }

        }

    }

    public void loadDataFromDatabaseToTable() throws SQLException {

        ObservableList<InvoiceModification> list = FXCollections.observableArrayList();
        Statement stm = connection.createStatement();
        ResultSet resultSet = stm.executeQuery(Global_SqlQuery);
        int i = 1;
        while (resultSet.next()) {

            Global_Table_InvoiceNumber = resultSet.getString("INVOICE_NUMBER");
            Global_Table_PersonName = resultSet.getString("CUSTOMER_NAME");   
            Global_Table_DateOfInvoice = resultSet.getString("INVOICE_DATE");
            Global_Table_Total_Amount = resultSet.getString("NET_TOTAL_TOTAL");
            Globle_Table_Paid_Amount = resultSet.getString("PAID");
            Globle_Table_Outstanding_Amount = resultSet.getString("OUTSTANDING");
            Global_Table_InvoiceStatus = resultSet.getString("STATUS");

            list.add(new InvoiceModification(i, Global_Table_InvoiceNumber, Global_Table_PersonName, Global_Table_DateOfInvoice, Global_Table_Total_Amount, Globle_Table_Paid_Amount, Globle_Table_Outstanding_Amount, Global_Table_InvoiceStatus));
            i++;
            flag = true;
        }

        Table_Invoice.setItems(list);

        Text_Field_InvoiceNumber.setText("");

    }

    @FXML
    private void clickOnMouseOnTableInvoice() {

        Table_Invoice.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                InvoiceModification list = Table_Invoice.getItems().get(Table_Invoice.getSelectionModel().getSelectedIndex());

                String InvoiceNumber = list.getPojo_InvoiceNumber();
                String InvoiceName = list.getPojo_PersonName();

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("View_Edit_Cancel_InvoiceUsingTable.fxml")); //  name of modifibable itemsFxml
                try {
                    loader.load();
                } catch (Exception e) {
                }

                View_Edit_Cancel_InvoiceUsingTableController display = loader.getController();  // here Obj Of  Controllel that call method and pass some value to initillze method.
                try {
                    display.setTextforViewEditInvoice(InvoiceNumber, InvoiceName); // change Method here  
                } catch (SQLException ex) {
                    Logger.getLogger(View_Edit_Cancel_InvoiceController.class.getName()).log(Level.SEVERE, null, ex);
                }
                p = loader.getRoot();
                stage = new Stage();
                stage.resizableProperty().setValue(Boolean.FALSE);// disible
                stage.initStyle(StageStyle.UTILITY);// hide 
                stage.setScene(new Scene(p));
                stage.showAndWait();

                try {
                    loadDataFromDatabaseToTable();
                } catch (SQLException ex) {
                    Logger.getLogger(View_Edit_Cancel_InvoiceController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

    }

}
