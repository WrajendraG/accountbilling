/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account_billing;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import requriedDatabaseConnection.DatabaseConnection;

/**
 * FXML Controller class
 *
 * @author Niteen
 */
public class Edit_Delete_Items_UsingTableController implements Initializable {

    Connection connection;
    Alert alert = new Alert(Alert.AlertType.WARNING);

    int SalePrice;
    int MRP;
    float perDiscount;
    float Discount;
    float SelectGST;
    float TGST;
    float DivGST;
    float rate;

    String GlobleItemName, GlobleCategoryName;
    @FXML
    private AnchorPane anchor;
    @FXML
    private TextField Text_ItemName;
    @FXML
    private ComboBox<String> ComboxUnits;
    @FXML
    private TextField TextStock;
    @FXML
    private Label unit1;
    @FXML
    private TextField TextSalePrice;
    @FXML
    private Label unit3;
    @FXML
    private TextField TextPurchasePrice;
    @FXML
    private Label unit2;
    @FXML
    private TextField TextMrp;
    @FXML
    private Label unit4;
    @FXML
    private TextField TextmMinSalePrice;
    @FXML
    private TextField TextSelValuePrice;
    @FXML
    private TextField TextHsnCode;
    @FXML
    private TextField txt_cgst_per;
    @FXML
    private TextField txt_sgst_per;
    @FXML
    private TextField txt_gst_per;
    @FXML
    private TextField txt_cgst_rupess;
    @FXML
    private TextField txt_sgst_rupess;
    @FXML
    private TextField txt_gst_rupess;
    @FXML
    private TextField txt_discount_rupess;
    @FXML
    private TextField txt_discount_per;
    @FXML
    private ComboBox<String> ComboxTaxtCategory;
    @FXML
    private RadioButton existingBarcode;
    @FXML
    private ToggleGroup checkbarcode;
    @FXML
    private RadioButton generateBarcode;
    @FXML
    private TextField scanedBarcode;
    @FXML
    private ComboBox<String> com_Category_Items;
    @FXML
    private Button Buttonedit;
    @FXML
    private Button ButtonDelete;
    @FXML
    private Button buttonUpdate;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        connection = DatabaseConnection.getConnection();

        //  ComboxTaxtCategory.getSelectionModel().select("Select");
    }

    public void setTextforViewEditItems(String ItemName, String Category) {
        GlobleItemName = ItemName;
        GlobleCategoryName = Category;   // for Linking Two Controllel

        try {
            loaddataFirsttime();
        } catch (SQLException ex) {
            Logger.getLogger(Edit_Delete_Items_UsingTableController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void makeEditableField(ActionEvent event) {

        ComboxUnits.getItems().addAll("Dozens", "Gms", "kgs.", "Meter", "Pcs.", "Tonne");

        ComboxTaxtCategory.getItems().addAll("0%", "5%", "12%", "18%", "28%");

        existingBarcode.setDisable(true);
        generateBarcode.setDisable(true);
        scanedBarcode.setEditable(false);
        Text_ItemName.setEditable(false);
        com_Category_Items.setDisable(true);

        ComboxUnits.setDisable(false);
        TextStock.setEditable(true);
        TextSalePrice.setEditable(true);
        TextPurchasePrice.setEditable(true);
        TextMrp.setEditable(true);
        TextmMinSalePrice.setEditable(true);
        TextSelValuePrice.setEditable(true);
        TextHsnCode.setEditable(true);
        ComboxTaxtCategory.setDisable(false);  // method Declared

        txt_cgst_per.setEditable(false);
        txt_cgst_rupess.setEditable(false);
        txt_sgst_per.setEditable(false);
        txt_sgst_rupess.setEditable(false);
        txt_gst_per.setEditable(false);
        txt_gst_rupess.setEditable(false);
        txt_discount_per.setEditable(false);
        txt_discount_rupess.setEditable(false);
        buttonUpdate.setDisable(false);
        Buttonedit.setDisable(true);
        ButtonDelete.setDisable(false);
        //     buttonUpdate.s
    }

    @FXML
    private void deleteItemsAction(ActionEvent event) throws SQLException {

        // delete Items as will as From Category Items using Transation 
        Connection con = DatabaseConnection.getConnection();

        con.setAutoCommit(false);

        String TempCategory = (String) com_Category_Items.getSelectionModel().getSelectedItem();

        Statement statement = con.createStatement();

        boolean flag1 = statement.execute("Delete from  ADD_ITEMS_DETAILS where BAR_CODE ='" + scanedBarcode.getText() + "' ");

        Statement statement2 = con.createStatement();
        boolean flag2 = statement2.execute("Delete from " + TempCategory + " where ITEMS_NAME ='" + Text_ItemName.getText() + "'");

        if (flag2 == false && flag1 == false) {

            con.commit();
            alert.setTitle("validation");
            alert.setHeaderText(null);
            alert.setContentText(" Items Sucessfully Deleted");
            alert.showAndWait();

            ComboxUnits.getItems().removeAll(ComboxUnits.getItems());
            ComboxTaxtCategory.getItems().removeAll(ComboxTaxtCategory.getItems());

            existingBarcode.setDisable(true);

            generateBarcode.setDisable(true);
            scanedBarcode.setEditable(false);
            scanedBarcode.clear();

            Text_ItemName.setEditable(false);
            Text_ItemName.clear();
            com_Category_Items.setDisable(true);

            ComboxUnits.setDisable(true);
            TextStock.setEditable(true);
            TextStock.clear();

            TextSalePrice.setEditable(true);
            TextSalePrice.clear();
            TextPurchasePrice.setEditable(true);
            TextPurchasePrice.clear();
            TextMrp.setEditable(true);
            TextMrp.clear();
            TextmMinSalePrice.setEditable(true);
            TextmMinSalePrice.clear();
            TextSelValuePrice.setEditable(true);
            TextSelValuePrice.clear();
            TextHsnCode.setEditable(true);
            TextHsnCode.clear();
            ComboxTaxtCategory.setDisable(true);  // method Declared

            txt_cgst_per.setEditable(false);
            txt_cgst_per.clear();
            txt_cgst_rupess.setEditable(false);
            txt_cgst_rupess.clear();
            txt_sgst_per.setEditable(false);
            txt_sgst_per.clear();
            txt_sgst_rupess.setEditable(false);
            txt_sgst_rupess.clear();
            txt_gst_per.setEditable(false);
            txt_gst_per.clear();
            txt_gst_rupess.setEditable(false);
            txt_gst_rupess.clear();
            txt_discount_per.setEditable(false);
            txt_discount_per.clear();
            txt_discount_rupess.setEditable(false);
            txt_discount_rupess.clear();
            buttonUpdate.setDisable(true);
            Buttonedit.setDisable(true);
            ButtonDelete.setDisable(true);

        } else {
            con.rollback();
            alert.setTitle("validation");
            alert.setHeaderText(null);
            alert.setContentText(" Items Not Deleted Try Again ");
            alert.showAndWait();

        }

    }

    @FXML
    private void updateChangesAction(ActionEvent event) throws SQLException {

        if (existingBarcode.isSelected()) {

            if (scanedBarcode.getText().length() == 0 && Text_ItemName.getText().length() == 0 && TextStock.getText().length() == 0 && TextSalePrice.getText().length() == 0 && TextPurchasePrice.getText().length() == 0 && TextMrp.getText().length() == 0 && TextmMinSalePrice.getText().length() == 0 && TextSelValuePrice.getText().length() == 0 && TextHsnCode.getText().length() == 0 && txt_cgst_per.getText().length() == 0 && txt_sgst_per.getText().length() == 0 && txt_gst_per.getText().length() == 0 && txt_cgst_rupess.getText().length() == 0 && txt_sgst_rupess.getText().length() == 0 && txt_gst_rupess.getText().length() == 0 && txt_discount_rupess.getText().length() == 0 && txt_discount_per.getText().length() == 0) {//&&cgst.getText().length()==0 &&sgstt.getText().length()==0 &&stockt.getText().length()==0){

                alert.setTitle("Validate");
                alert.setHeaderText(null);
                alert.setContentText("Fields should not be Empty");
                alert.showAndWait();

            } else {

                PreparedStatement preparedStatement = connection.prepareStatement("UPDATE ADD_ITEMS_DETAILS SET UNITS=?, STOCK=?, SALE_PRICE=?, PURCHASE_PRICE=?, MRP=?, MIN_SALE=?, SELF_VALUE_PRICE=?, HSN_CODE=?, TAXT_CATEGORY=?,"
                        + " CGST_PER=?, CGST_RUPESS=?, SGST_PER=?, SGST_RUPESS=?, GST_PER=?, GST_RUPESS=?, DISCOUNT_PER=?, DISCOUNT_RUPESS=?, RATE=? where BAR_CODE='" + scanedBarcode.getText() + "' ");

                preparedStatement.setString(1, (String) ComboxUnits.getSelectionModel().getSelectedItem());
                preparedStatement.setString(2, TextStock.getText());
                preparedStatement.setString(3, TextSalePrice.getText());
                preparedStatement.setString(4, TextPurchasePrice.getText());
                preparedStatement.setString(5, TextMrp.getText());
                preparedStatement.setString(6, TextmMinSalePrice.getText());
                preparedStatement.setString(7, TextSelValuePrice.getText());
                preparedStatement.setString(8, TextHsnCode.getText());
                preparedStatement.setString(9, (String) ComboxTaxtCategory.getSelectionModel().getSelectedItem());   // ps.setString(2, (String) combo_units.getSelectionModel().getSelectedItem());
                preparedStatement.setString(10, txt_cgst_per.getText());
                preparedStatement.setString(11, txt_cgst_rupess.getText());
                preparedStatement.setString(12, txt_sgst_per.getText());
                preparedStatement.setString(13, txt_sgst_rupess.getText());
                preparedStatement.setString(14, txt_gst_per.getText());
                preparedStatement.setString(15, txt_gst_rupess.getText());

                preparedStatement.setString(16, txt_discount_per.getText());
                preparedStatement.setString(17, txt_discount_rupess.getText());
                preparedStatement.setFloat(18, rate);

                int result = preparedStatement.executeUpdate();
                preparedStatement.close();

                if (result > 0) {

                    alert.setTitle("Validation");
                    alert.setHeaderText(null);
                    alert.setContentText(" Account Successfuly Updated ");
                    alert.showAndWait();

                    ComboxUnits.getItems().removeAll(ComboxUnits.getItems());
                    ComboxTaxtCategory.getItems().removeAll(ComboxTaxtCategory.getItems());

                    existingBarcode.setDisable(true);

                    generateBarcode.setDisable(true);
                    scanedBarcode.setEditable(false);
                    scanedBarcode.clear();

                    Text_ItemName.setEditable(false);
                    Text_ItemName.clear();
                    com_Category_Items.setDisable(true);

                    ComboxUnits.setDisable(true);
                    TextStock.setEditable(false);
                    TextStock.clear();

                    TextSalePrice.setEditable(false);
                    TextSalePrice.clear();
                    TextPurchasePrice.setEditable(false);
                    TextPurchasePrice.clear();
                    TextMrp.setEditable(false);
                    TextMrp.clear();
                    TextmMinSalePrice.setEditable(false);
                    TextmMinSalePrice.clear();
                    TextSelValuePrice.setEditable(false);
                    TextSelValuePrice.clear();
                    TextHsnCode.setEditable(false);
                    TextHsnCode.clear();
                    ComboxTaxtCategory.setDisable(true);  // method Declared

                    txt_cgst_per.setEditable(false);
                    txt_cgst_per.clear();
                    txt_cgst_rupess.setEditable(false);
                    txt_cgst_rupess.clear();
                    txt_sgst_per.setEditable(false);
                    txt_sgst_per.clear();
                    txt_sgst_rupess.setEditable(false);
                    txt_sgst_rupess.clear();
                    txt_gst_per.setEditable(false);
                    txt_gst_per.clear();
                    txt_gst_rupess.setEditable(false);
                    txt_gst_rupess.clear();
                    txt_discount_per.setEditable(false);
                    txt_discount_per.clear();
                    txt_discount_rupess.setEditable(false);
                    txt_discount_rupess.clear();

                    buttonUpdate.setDisable(true);
                    Buttonedit.setDisable(true);
                    ButtonDelete.setDisable(true);

                }

            }
        }
    }

    public void loaddataFirsttime() throws SQLException {

        Statement statement = connection.createStatement();
        ComboxUnits.getItems().removeAll(ComboxUnits.getItems());
        ComboxTaxtCategory.getItems().removeAll(ComboxTaxtCategory.getItems());

        ResultSet resultSet = statement.executeQuery("Select * From ADD_ITEMS_DETAILS where ITEMS_NAME='" + GlobleItemName + "' AND ITEMS_CATEGORY='" + GlobleCategoryName + "'");
        while (resultSet.next()) {

            scanedBarcode.setText(resultSet.getString("BAR_CODE"));
            com_Category_Items.getSelectionModel().select(resultSet.getString("ITEMS_CATEGORY"));
            Text_ItemName.setText(resultSet.getString("ITEMS_NAME"));
            //--------------------------------------------------------------------------------------------above is uneditable
            ComboxUnits.getSelectionModel().select(resultSet.getString("UNITS"));
            TextStock.setText(resultSet.getString("STOCK"));
            TextSalePrice.setText(resultSet.getString("SALE_PRICE"));
            TextPurchasePrice.setText(resultSet.getString("PURCHASE_PRICE"));
            TextMrp.setText(resultSet.getString("MRP"));
            TextmMinSalePrice.setText(resultSet.getString("MIN_SALE"));
            TextSelValuePrice.setText(resultSet.getString("SELF_VALUE_PRICE"));
            // combo_units.getSelectionModel().select("Select");

            TextHsnCode.setText(resultSet.getString("HSN_CODE"));

            ComboxTaxtCategory.getSelectionModel().select(resultSet.getString("TAXT_CATEGORY"));
            txt_cgst_per.setText(resultSet.getString("CGST_PER"));
            txt_cgst_rupess.setText(resultSet.getString("CGST_RUPESS"));
            txt_sgst_per.setText(resultSet.getString("SGST_PER"));
            txt_sgst_rupess.setText(resultSet.getString("SGST_RUPESS"));
            txt_gst_per.setText(resultSet.getString("GST_PER"));
            txt_gst_rupess.setText(String.valueOf(resultSet.getFloat("GST_RUPESS")));
            txt_discount_per.setText(resultSet.getString("DISCOUNT_PER"));
            txt_discount_rupess.setText(resultSet.getString("DISCOUNT_RUPESS"));

        }

        existingBarcode.setDisable(true);
        generateBarcode.setDisable(true);
        scanedBarcode.setEditable(false);
        com_Category_Items.setDisable(true);

        ComboxUnits.setDisable(true);
        TextStock.setEditable(false);
        TextSalePrice.setEditable(false);
        TextPurchasePrice.setEditable(false);
        TextMrp.setEditable(false);
        TextmMinSalePrice.setEditable(false);
        TextSelValuePrice.setEditable(false);
        TextHsnCode.setEditable(false);
        txt_cgst_per.setEditable(false);

        ComboxTaxtCategory.setDisable(true);
        txt_cgst_rupess.setEditable(false);
        txt_sgst_per.setEditable(false);
        txt_sgst_rupess.setEditable(false);
        txt_gst_per.setEditable(false);
        txt_gst_rupess.setEditable(false);
        txt_discount_per.setEditable(false);
        txt_discount_rupess.setEditable(false);
        Text_ItemName.setEditable(false);
        buttonUpdate.setDisable(true);

    }

    @FXML
    private void textCategoryAction(ActionEvent event) {

        ComboxTaxtCategory.getSelectionModel().getSelectedItem();

//        unit1.setText("(" + ComboxTaxtCategory.getSelectionModel().getSelectedItem() + ")");
//        unit2.setText("(" + ComboxTaxtCategory.getSelectionModel().getSelectedItem() + ")");
//        unit3.setText("(" + ComboxTaxtCategory.getSelectionModel().getSelectedItem() + ")");
//        unit4.setText("(" + ComboxTaxtCategory.getSelectionModel().getSelectedItem() + ")");
//
//    }
//
//    @FXML
//    private void handleTaxComboBoxAction(ActionEvent event) {
        if (ComboxTaxtCategory.getSelectionModel().getSelectedIndex() == 0) {
            SelectGST = 0;
            txt_cgst_per.setText("0");
            txt_sgst_per.setText("0");
            txt_gst_per.setText("0");

        }

        if (ComboxTaxtCategory.getSelectionModel().getSelectedIndex() == 1) {
            SelectGST = 5;
            txt_cgst_per.setText("2.5");
            txt_sgst_per.setText("2.5");
            txt_gst_per.setText("5");

        }
        if (ComboxTaxtCategory.getSelectionModel().getSelectedIndex() == 2) {
            SelectGST = 12;
            txt_cgst_per.setText("6");
            txt_sgst_per.setText("6");
            txt_gst_per.setText("12");
        }
        if (ComboxTaxtCategory.getSelectionModel().getSelectedIndex() == 3) {
            SelectGST = 18;
            txt_cgst_per.setText("9");
            txt_sgst_per.setText("9");
            txt_gst_per.setText("18");
        }
        if (ComboxTaxtCategory.getSelectionModel().getSelectedIndex() == 4) {
            SelectGST = 28;
            txt_cgst_per.setText("14");
            txt_sgst_per.setText("14");
            txt_gst_per.setText("28");

        }
//--------------------------------------------------------------------------------------
        SalePrice = Integer.parseInt(TextSalePrice.getText());
        MRP = Integer.parseInt(TextMrp.getText());

        Discount = MRP - SalePrice;

        txt_discount_rupess.setText(String.valueOf(Discount));

        perDiscount = (Discount / MRP) * 100;

        String temp11 = String.format("%.2f", perDiscount);
        txt_discount_per.setText(String.valueOf(temp11));
//-------------------------------------------------------------------------------------------
        TGST = ((SalePrice / (100 + SelectGST)) * SelectGST);

        rate = SalePrice - TGST;    // rate / cost = sale price - gst   // basic value

        String temp = String.format("%.2f", TGST);
        txt_gst_rupess.setText(String.valueOf(temp));

        DivGST = TGST / 2;
        String temp1 = String.format("%.2f", DivGST);
        txt_cgst_rupess.setText(String.valueOf(temp1));
        txt_sgst_rupess.setText(String.valueOf(temp1));

//-----------------------------------------------------------------------------------------------
    }
}
