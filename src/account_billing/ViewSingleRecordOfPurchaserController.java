/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account_billing;

import com.osiersystems.pojos.ViewAccounts;
import com.osiersystems.pojos.ViewSingleRecordOfPurchaser;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import requriedDatabaseConnection.DatabaseConnection;


/**
 * FXML Controller class
 *
 * @author G1 NOTEBOOK
 */
public class ViewSingleRecordOfPurchaserController implements Initializable {

    @FXML
    private TableView<ViewSingleRecordOfPurchaser> table_viewAccount;
    @FXML
    private TextField Text_Total_Purchases;
    @FXML
    private TextField Text_Total_Paid;
    @FXML
    private TextField Text_total_outstanding;

    /**
     * Initializes the controller class.
     */
    Connection connection;
    String invoicenop;
    String nameofsupplierp;
    Stage stage;
    Parent p;
    @FXML
    private TableColumn<ViewSingleRecordOfPurchaser, Integer> V_SR_srno;
    @FXML
    private TableColumn<ViewSingleRecordOfPurchaser, String> V_SR_date;
    @FXML
    private TableColumn<ViewSingleRecordOfPurchaser, String> V_SR_invoiceno;
    @FXML
    private TableColumn<ViewSingleRecordOfPurchaser, String> V_SR_Amount;
    @FXML
    private TableColumn<ViewSingleRecordOfPurchaser, String> V_SR_paid;
    @FXML
    private TableColumn<ViewSingleRecordOfPurchaser, String> V_SR_outstanding;
    @FXML
    private TableColumn<ViewSingleRecordOfPurchaser, String> V_SR_mode;
    @FXML
    private TextField Text_supplier_name;
    @FXML
    private TextField Text_pan_no;
    @FXML
    private TextField Text_contact;
    @FXML
    private TextField Text_email_id;
    @FXML
    private TextField Text_address;
    @FXML
    private TextField Text_contact_person;
    @FXML
    private TextField Text_invoice_no;
    Float Globaloutstanding;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        connection = DatabaseConnection.getConnection();
        V_SR_srno.setCellValueFactory(new PropertyValueFactory<>("Pojo_SrNo"));
        V_SR_srno.setStyle("-fx-alignment: CENTER");

        V_SR_date.setCellValueFactory(new PropertyValueFactory<>("Pojo_Date"));
        V_SR_date.setStyle("-fx-alignment: CENTER");

        V_SR_invoiceno.setCellValueFactory(new PropertyValueFactory<>("Pojo_Invoice_No"));
        V_SR_invoiceno.setStyle("-fx-alignment: CENTER");

        V_SR_Amount.setCellValueFactory(new PropertyValueFactory<>("Pojo_Amount"));
        V_SR_Amount.setStyle("-fx-alignment: CENTER");

        V_SR_paid.setCellValueFactory(new PropertyValueFactory<>("Pojo_Paid"));
        V_SR_paid.setStyle("-fx-alignment: CENTER");

        V_SR_outstanding.setCellValueFactory(new PropertyValueFactory<>("Pojo_Outstanding"));
        V_SR_outstanding.setStyle("-fx-alignment: CENTER");

        V_SR_mode.setCellValueFactory(new PropertyValueFactory<>("Pojo_Status"));
        V_SR_mode.setStyle("-fx-alignment: CENTER");
        try {
            loaddataFirsttime();
        } catch (SQLException ex) {
            Logger.getLogger(ViewSingleRecordOfPurchaserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
//          Text_From_Date.setConverter(new StringConverter<LocalDate>() {
//            String pattern = "yyyy-MM-dd";
//            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
//
//            {
//                Text_From_Date.setPromptText(pattern.toLowerCase());
//            }
//
//            @Override
//            public String toString(LocalDate date) {
//                if (date != null) {
//                    return dateFormatter.format(date);
//                } else {
//                    return "";
//                }
//            }
//
//            @Override
//            public LocalDate fromString(String string) {
//                if (string != null && !string.isEmpty()) {
//                    return LocalDate.parse(string, dateFormatter);
//                } else {
//                    return null;
//                }
//            }
//        });
        
//          Text_From_Date.setConverter(new StringConverter<LocalDate>() {
//            String pattern = "yyyy-MM-dd";
//            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
//
//            {
//                Text_From_Date.setPromptText(pattern.toLowerCase());
//            }
//
//            @Override
//            public String toString(LocalDate date) {
//                if (date != null) {
//                    return dateFormatter.format(date);
//                } else {
//                    return "";
//                }
//            }
//
//            @Override
//            public LocalDate fromString(String string) {
//                if (string != null && !string.isEmpty()) {
//                    return LocalDate.parse(string, dateFormatter);
//                } else {
//                    return null;
//                }
//            }
//        });

    }

    public void setTextforViewAccount(String invoiceno, String nameofsupplier) {
        invoicenop = invoiceno;

        nameofsupplierp = nameofsupplier;
        try {

            loaddataFirsttime();
        } catch (SQLException ex) {
            Logger.getLogger(ModificationCreateAccountController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void loaddataFirsttime() throws SQLException {

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select *from supplieraccount where BUSINESS_NAME='" + nameofsupplierp + "'");
        while (resultSet.next()) {

            Text_supplier_name.setText(resultSet.getString("BUSINESS_NAME"));
            Text_supplier_name.setEditable(false);
            Text_pan_no.setText(resultSet.getString("BUSINESS_PAN_NO"));
            Text_pan_no.setEditable(false);
            Text_contact.setText(resultSet.getString("business_phone_no"));
            Text_contact.setEditable(false);
            Text_email_id.setText(resultSet.getString("business_email"));
            Text_email_id.setEditable(false);
            Text_address.setText(resultSet.getString("BUSINESS_ADDRESS"));
            Text_address.setEditable(false);
            Text_contact_person.setText(resultSet.getString("CONTACT_PERSON"));
            Text_contact_person.setEditable(false);
            Text_invoice_no.setText(String.valueOf(invoicenop));
            Text_invoice_no.setEditable(false);
        }

        String sqlfor = "select *from purchase_history where invoice_no='" + invoicenop + "'";

        ObservableList<ViewSingleRecordOfPurchaser> list123 = FXCollections.observableArrayList();
        Statement stm1 = connection.createStatement();
        ResultSet resultSet1 = stm1.executeQuery(sqlfor);
        int i = 1;
        while (resultSet1.next()) {
            Globaloutstanding = resultSet1.getFloat("Outstanding");
            list123.add(new ViewSingleRecordOfPurchaser(i, invoicenop, resultSet1.getString("purchase_date"), String.valueOf(resultSet1.getFloat("Amoint")), String.valueOf(resultSet1.getFloat("paid")), String.valueOf(resultSet1.getFloat("Outstanding")), resultSet1.getString("purchase_mode")));
            i++;
            System.out.println("list=" + list123);
        }
        table_viewAccount.setItems(list123);

        String sql1 = "select amoint as total_purchases,sum(paid) as total_paid,outstanding as total_outstanding from purchase_history where invoice_no='" + invoicenop + "'";
        Statement stm2 = connection.createStatement();
        ResultSet resultSet2 = stm2.executeQuery(sql1);

        while (resultSet2.next()) {
            Text_Total_Purchases.setText(String.valueOf(resultSet2.getFloat("total_purchases")));
            Text_Total_Paid.setText(String.valueOf(resultSet2.getFloat("total_paid")));
            Text_total_outstanding.setText(String.valueOf(resultSet2.getFloat("total_outstanding")));
        }
        resultSet2.close();
        stm2.close();
        resultSet1.close();
        stm1.close();
        resultSet.close();
        statement.close();

    }

    @FXML
    private void setCellValueFromToTextField(MouseEvent event) {

    }

    @FXML
    private void Click_on_initialize_payment(ActionEvent event) throws SQLException {
        System.out.println("Golbal outstanding=" + (Globaloutstanding <=0));
        if (Globaloutstanding>0.0) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("ViewInitializePayment.fxml"));
            try {
                loader.load();
            } catch (Exception e) {
            }

            ViewInitializePaymentController display = loader.getController();
            display.setTextforViewAccount(invoicenop, nameofsupplierp);
            p = loader.getRoot();
            stage = new Stage();
            stage.resizableProperty().setValue(Boolean.FALSE);// disible
            stage.initStyle(StageStyle.UTILITY);// hide 
            stage.setScene(new Scene(p));
            stage.showAndWait();
            loaddataFirsttime();

        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Initialize Payment");
            alert.setHeaderText(null);
            alert.setContentText("You have alreay paid all amount.");
            alert.showAndWait();

        }

    }

}
