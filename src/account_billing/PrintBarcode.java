/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account_billing;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import requriedDatabaseConnection.DatabaseConnection;

/**
 *
 * @author Niteen
 */
public class PrintBarcode extends JFrame {

    JPanel controlPanel;
    JLabel label;
    Connection connection;
    Alert alert = new Alert(Alert.AlertType.WARNING);

    public void print() throws SQLException, IOException {

        connection = DatabaseConnection.getConnection();

        int qty = 0;

        JFrame frame = new JFrame();
        controlPanel = new JPanel();
        frame.add(controlPanel);
        controlPanel.setVisible(true);
        controlPanel.setBounds(50, 50, 500, 600);
        frame.setVisible(false);
        JScrollPane jsp = new JScrollPane(controlPanel);
        frame.add(jsp);
        StringBuilder table = new StringBuilder();

        table.append("<HTML><head>");
        table.append(" <style>");
        table.append("table, th, td {");
        table.append("border: 1px solid black;");
        table.append("border-collapse: collapse;");
        table.append("}");
        table.append("th, td {");
        table.append("    padding: 1px;");
        table.append("    text-align: left;");
        table.append("}");
        table.append("</style></head><body>");

        table.append("<table style=\"width:100%\">");//width=\"100%\" border=\"1\" cellspacing=\"1\"  cellpadding=\"1\" >");

        Statement stm = connection.createStatement();

        ResultSet resultSet = stm.executeQuery("select * from Barcode");

        while (resultSet.next()) {

            qty = resultSet.getInt("qty");
            InputStream fis = resultSet.getBinaryStream("barcode_Image");
            BufferedImage imgt = javax.imageio.ImageIO.read(fis);
            WritableImage newImg = SwingFXUtils.toFXImage(imgt, null);
            ImageView BarcodeImage = new ImageView();
            BarcodeImage.setImage(newImg);

            //resultSet.getString("ITEMS_NAME"), resultSet.getString("CODE"), BarcodeImage, resultSet.getInt("qty")
            for(int i = qty;  i>=0; i--)
            {
            table.append("<tr>");
            table.append("<th colspan=\"13\" align=\"left\">" + resultSet.getString("ITEMS_NAME") + " <br> " + BarcodeImage+ " </th>");
            table.append("</tr>");
            }
            
        }

        table.append("</table>");
        table.append("</body></HTML>");
        label = new JLabel(table.toString());
        //("OSIER SYSTEMS"); 
        label.setForeground(Color.black);
        label.setFont(new Font("Calibri", Font.BOLD, 10));
        label.setBounds(560, 50, 220, 100);
        controlPanel.add(label);
        // frame.setBounds(0,0,Utility.getDesktopResolution().get("width"),Utility.getDesktopResolution().get("height"));
        frame.setBackground(new Color(255, 179, 179));
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
    
    
    public void demoprint()
    {
    
    
    }

}
