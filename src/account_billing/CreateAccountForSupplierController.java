package account_billing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import validation.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import requriedDatabaseConnection.DatabaseConnection;

/**
 * FXML Controller class
 *
 * @author Rahul Jadhav
 */
public class CreateAccountForSupplierController implements Initializable {

    @FXML
    private TextField business_Name;
    @FXML
    private TextField business_Registraction_No;
    @FXML
    private TextField business_Pan_No;
    @FXML
    private TextField business_Gst_No;
    @FXML
    private TextField business_Email;
    @FXML
    private TextField business_Phone_No;
    @FXML
    private TextArea business_Address;
    @FXML
    private TextField business_Address_Pin_Code;
    @FXML
    private ComboBox<String> business_Address_State;
    @FXML
    private TextField bank_Name;
    @FXML
    private TextField Branch;
    @FXML
    private TextField account_No;
    @FXML
    private TextField ifsc_No;
    @FXML
    private TextField contact_Person;
    @FXML
    private TextField person_Email;
    @FXML
    private TextField person_Mobile_No;
    @FXML
    private TextField person_Pan_No;
    @FXML
    private RadioButton person_Gender_Male;
    @FXML
    private ToggleGroup gender;
    @FXML
    private RadioButton person_Gender_Female;
    @FXML
    private ComboBox<String> person_Address_State;
    @FXML
    private TextField person_Address_Pin_Code;
    @FXML
    private TextArea person_Address;
    @FXML
    private TextField business_Address_City;
    @FXML
    private TextField person_Address_City;

    /**
     * Initializes the controller class.
     */
    Connection connection = null;
    @FXML
    private BorderPane BorderPane;

    PreparedStatement preparedStatement = null;
    Statement st = null;
    ResultSet rs = null;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        String state[] = {"Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chhattisgarh", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala ", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal", "Andaman and Nicobar Islands", "Chandigarh", "Dadra and Nagar Haveli", "Daman and Diu ", "Delhi", "Lakshadweep", "Pondicherry"};

        business_Address_State.getItems().removeAll(business_Address_State.getItems());
        business_Address_State.getItems().addAll(state);
        business_Address_State.getSelectionModel().select("Maharashtra");

        //--------------------------------------------------------------------------------------------------------------------     
        person_Address_State.getItems().removeAll(person_Address_State.getItems());
        person_Address_State.getItems().addAll(state);
        person_Address_State.getSelectionModel().select("Maharashtra");

    }

    @FXML
    private void submit(ActionEvent event) throws SQLException {
        connection = DatabaseConnection.getConnection();

        String gender1 = "Male";

        if (person_Gender_Female.isSelected()) {
            gender1 = "Female";
        }

        if (business_Name.getText().isEmpty() || business_Address.getText().isEmpty() || business_Address_Pin_Code.getText().isEmpty() || business_Email.getText().isEmpty() || business_Gst_No.getText().isEmpty() || business_Pan_No.getText().isEmpty() || business_Phone_No.getText().isEmpty() || business_Address.getText().isEmpty() || person_Address.getText().isEmpty() || person_Address_Pin_Code.getText().isEmpty() || person_Email.getText().isEmpty() || person_Mobile_No.getText().isEmpty() || person_Pan_No.getText().isEmpty() || bank_Name.getText().isEmpty() || Branch.getText().isEmpty() || ifsc_No.getText().isEmpty() || account_No.getText().isEmpty()) {

            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Validation");
            alert.setHeaderText(null);
            alert.setContentText("All text field must be important");
            alert.showAndWait();

        } else {
            if (ValidateHere.panNumber(business_Pan_No.getText()) && ValidateHere.gstNumber(business_Gst_No.getText()) && ValidateHere.email(business_Email.getText()) && ValidateHere.number(business_Phone_No.getText()) && ValidateHere.ifscNumber(ifsc_No.getText()) && ValidateHere.email(person_Email.getText()) && ValidateHere.mobile(person_Mobile_No.getText()) && ValidateHere.panNumber(person_Pan_No.getText()) && ValidateHere.number(account_No.getText()) && ValidateHere.number(business_Registraction_No.getText()) && ValidateHere.pinNumber(business_Address_Pin_Code.getText()) && ValidateHere.pinNumber(person_Address_Pin_Code.getText())) {

                try {
                    String checkname = "select *from SUPPLIERACCOUNT where BUSINESS_NAME='" + business_Name.getText() + "'";

                    st = connection.createStatement();
                    rs = st.executeQuery(checkname);
                    if (!rs.next()) {
                        preparedStatement = connection.prepareStatement("insert into SUPPLIERACCOUNT values(supplier_id.NEXTVAL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                      


//   here table field and insert field miss mach some time you fix this . because data insert in to wrong coloum 





                    preparedStatement.setString(1, business_Name.getText());
                        preparedStatement.setString(2, business_Registraction_No.getText());
                        preparedStatement.setString(3, business_Pan_No.getText());
                        preparedStatement.setString(4, business_Gst_No.getText());
                        preparedStatement.setString(5, business_Email.getText());
                        preparedStatement.setString(6, business_Phone_No.getText());
                        preparedStatement.setString(7, business_Address.getText());
                        preparedStatement.setString(8, business_Address_Pin_Code.getText());
                        preparedStatement.setString(9, (String) business_Address_State.getSelectionModel().getSelectedItem());   // ps.setString(2, (String) combo_units.getSelectionModel().getSelectedItem());
                        preparedStatement.setString(10, business_Address_City.getText());
                        preparedStatement.setString(11, bank_Name.getText());
                        preparedStatement.setString(12, Branch.getText());
                        preparedStatement.setString(13, account_No.getText());
                        preparedStatement.setString(14, ifsc_No.getText());
                        preparedStatement.setString(15, contact_Person.getText());
                        preparedStatement.setString(16, person_Email.getText());
                        preparedStatement.setString(24, person_Mobile_No.getText());
                        preparedStatement.setString(18, person_Pan_No.getText());
                        preparedStatement.setString(19, gender1);
                        preparedStatement.setString(20, person_Address.getText());
                        preparedStatement.setString(21, person_Address_City.getText());
                        preparedStatement.setString(22, (String) person_Address_State.getSelectionModel().getSelectedItem());
                        preparedStatement.setString(23, person_Address_Pin_Code.getText());
                        preparedStatement.setString(24, "Active");

                        int result = preparedStatement.executeUpdate();

                        if (result > 0) {
                        

                            Alert alert = new Alert(Alert.AlertType.WARNING);
                            alert.setTitle("Validation");
                            alert.setHeaderText(null);
                            alert.setContentText(" Account Successfuly Created ");
                            alert.showAndWait();

                            business_Name.clear();
                            business_Registraction_No.clear();
                            business_Pan_No.clear();
                            business_Gst_No.clear();
                            business_Email.clear();
                            business_Phone_No.clear();
                            business_Address.clear();
                            business_Address_Pin_Code.clear();
                            business_Address_State.getSelectionModel().select("Maharashtra");
                            business_Address_City.clear();
                            bank_Name.clear();
                            Branch.clear();
                            account_No.clear();
                            ifsc_No.clear();
                            contact_Person.clear();
                            person_Email.clear();
                            person_Mobile_No.clear();
                            person_Pan_No.clear();
                            person_Address.clear();
                            person_Address_City.clear();
                            person_Address_State.getSelectionModel().select("Maharashtra");
                            person_Address_Pin_Code.clear();

                        } else {

                            Alert alert = new Alert(Alert.AlertType.WARNING);
                            alert.setTitle("Validation");
                            alert.setHeaderText(null);
                            alert.setContentText(" Try Again Account not created ");
                            alert.showAndWait();
                        }

                    } else {
                        if (rs != null) {
                            rs.close();
                        }
                        if (st != null) {
                            st.close();
                        }
                        System.out.println("account aready present with this name please chose another name");
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Validation");
                        alert.setHeaderText(null);
                        alert.setContentText("account aready present with this name please chose another business name");
                        alert.showAndWait();

                    }

                } catch (SQLException ex) {

                    Logger.getLogger(CreateAccountController.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    if (rs != null) {
                        rs.close();
                    }
                    if (st != null) {
                        st.close();
                    }
                    if (preparedStatement != null) {
                        preparedStatement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }

                }
            } else {

                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Validation");
                alert.setHeaderText(null);
                alert.setContentText("Try again");
                alert.showAndWait();
            }

        }

    }

    @FXML
    private void clear(ActionEvent event) {
        business_Name.clear();
        business_Registraction_No.clear();
        business_Pan_No.clear();
        business_Gst_No.clear();
        business_Email.clear();
        business_Phone_No.clear();
        business_Address.clear();
        business_Address_Pin_Code.clear();
        business_Address_State.getSelectionModel().select("Maharashtra");
        business_Address_City.clear();
        bank_Name.clear();
        Branch.clear();
        account_No.clear();
        ifsc_No.clear();
        contact_Person.clear();
        person_Email.clear();
        person_Mobile_No.clear();
        person_Pan_No.clear();
        person_Address.clear();
        person_Address_City.clear();
        person_Address_State.getSelectionModel().select("Maharashtra");
        person_Address_Pin_Code.clear();

    }

    private void Cancel(ActionEvent event) throws IOException, SQLException {
//        if (connection != null) {
//            connection.close();
//        }
//
//        Parent b = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
//        Scene a = new Scene(b);
//        Stage c = (Stage) ((Node) event.getSource()).getScene().getWindow();
//        c.setScene(a);
//        c.show();

    }

    @FXML
    private void cancelr(ActionEvent event) throws SQLException, IOException {
        if (connection != null) {
            connection.close();
        }

        Parent b = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene a = new Scene(b);
        Stage c = (Stage) ((Node) event.getSource()).getScene().getWindow();
        c.setScene(a);
        c.show();
    }

}
