/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account_billing;

import com.osiersystems.pojos.ADD_items_details;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import requriedDatabaseConnection.DatabaseConnection;

/**
 * FXML Controller class
 *
 * @author Niteen
 */
public class View_Edit_Cancel_InvoiceUsingTableController implements Initializable {

    Connection connection;

    String Globle_InvoiceNumber, Globle_InvoiveName;
    String status;
    int result;
    Alert alert = new Alert(Alert.AlertType.WARNING);

    @FXML
    private TextField invoice_Number;
    @FXML
    private TextField GST_Number;
    @FXML
    private TextField mobile_Number;
    @FXML
    private TextField net_Total_Amount;
    @FXML
    private TextField net_Total_Cgst_Amount;
    @FXML
    private TextField net_Total_Discount;
    @FXML
    private TextField net_Total_Sgst_Amount;

    @FXML
    private Button canceledInvoice;
    @FXML
    private Button updateInvoice;
    @FXML
    private Button EditInvoice;

    //private TextField remaining_stock;
    @FXML
    private TableView<ADD_items_details> invoice_table;
    @FXML
    private TableColumn<ADD_items_details, String> table_Sr_No;
    @FXML
    private TableColumn<ADD_items_details, String> table_Items;
    @FXML
    private TableColumn<ADD_items_details, String> table_Hsn_Code;
    @FXML
    private TableColumn<ADD_items_details, String> table_Units;
    @FXML
    private TableColumn<ADD_items_details, String> table_Rate;
    @FXML
    private TableColumn<ADD_items_details, String> table_Qty;
    @FXML
    private TableColumn<ADD_items_details, String> table_Amount;
    @FXML
    private TableColumn<ADD_items_details, String> table_Discount;
    @FXML
    private TableColumn<ADD_items_details, String> table_Taxable_Value;
    @FXML
    private TableColumn<ADD_items_details, String> table_Cgst;
    @FXML
    private TableColumn<ADD_items_details, String> table_Cgst_Rate;
    @FXML
    private TableColumn<ADD_items_details, String> table_Cgst_Amount;
    @FXML
    private TableColumn<ADD_items_details, String> table_Sgst;
    @FXML
    private TableColumn<ADD_items_details, String> table_Sgst_Rate;
    @FXML
    private TableColumn<ADD_items_details, String> table_Sgst_Amount;
    @FXML
    private TableColumn<ADD_items_details, String> table_Total;
    @FXML
    private TextField net_Total_Qty;
    @FXML
    private TextField net_Total_Total;
    @FXML
    private TextField net_Total_Taxable_value;
    @FXML
    private TextArea address;
    @FXML
    private TextField date;
    @FXML
    private TextField net_Total_Rate;

    @FXML
    private TextField PaidAmount;
    @FXML
    private TextField OutstandingAmount;
    @FXML
    private TextField Business_Name;
    @FXML
    private TextField Customer_Name;
    //-======================================================
    //  storing a multiple items in Invoice table within single row.
    String[] invoiceItemName, invoiceHsnCode, invoiceUnit;
    //all float from database
    String[] invoiceRate, invoiceAmount, invoiceDiscount, invoiceTaxableValue, invoiceCgstRate, invoiceCgstAmount, invoiceSgstRate, invoiceSgstAmount, invoiceTotal, invoiceMRP, invoiceSalePrice;
    // all int from databse
    String[] invoiceQty, invoiceStock, invoiceTaxableCategory;

    float Global_Total, Globle_paidAmount, GlobleOutStandingAmount;
    @FXML
    private AnchorPane anchor;
    @FXML
    private Label label_cgst;
    @FXML
    private Label label_sgst;
    @FXML
    private Button invoicePrint;

    //============================================================
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        connection = DatabaseConnection.getConnection();

        table_Sr_No.setCellValueFactory(new PropertyValueFactory<>("SrNo"));
        table_Sr_No.setStyle("-fx-alignment: CENTER");

        table_Items.setCellValueFactory(new PropertyValueFactory<>("item_name"));

        table_Hsn_Code.setCellValueFactory(new PropertyValueFactory<>("hsn_code"));
        table_Hsn_Code.setStyle("-fx-alignment: CENTER");
        table_Units.setCellValueFactory(new PropertyValueFactory<>("units"));
        table_Units.setStyle("-fx-alignment: CENTER");
        table_Rate.setCellValueFactory(new PropertyValueFactory<>("rate"));
        table_Rate.setStyle("-fx-alignment: CENTER");
        table_Qty.setCellValueFactory(new PropertyValueFactory<>("qty"));
        table_Qty.setStyle("-fx-alignment: CENTER");
        table_Amount.setCellValueFactory(new PropertyValueFactory<>("amount"));
        table_Amount.setStyle("-fx-alignment: CENTER");
        table_Discount.setCellValueFactory(new PropertyValueFactory<>("discount_rupess"));
        table_Discount.setStyle("-fx-alignment: CENTER");
        table_Taxable_Value.setCellValueFactory(new PropertyValueFactory<>("taxableValue"));
        table_Taxable_Value.setStyle("-fx-alignment: CENTER");
        table_Cgst_Rate.setCellValueFactory(new PropertyValueFactory<>("cgst_per"));
        table_Cgst_Rate.setStyle("-fx-alignment: CENTER");
        table_Cgst_Amount.setCellValueFactory(new PropertyValueFactory<>("cgst_rupess"));
        table_Cgst_Amount.setStyle("-fx-alignment: CENTER");
        table_Sgst_Rate.setCellValueFactory(new PropertyValueFactory<>("sgst_per"));
        table_Sgst_Rate.setStyle("-fx-alignment: CENTER");
        table_Sgst_Amount.setCellValueFactory(new PropertyValueFactory<>("sgst_rupess"));
        table_Sgst_Amount.setStyle("-fx-alignment: CENTER");
        table_Total.setCellValueFactory(new PropertyValueFactory<>("total"));
        table_Total.setStyle("-fx-alignment: CENTER");

        makeuneditable();

    }

    public void setTextforViewEditInvoice(String InvoiceNumber, String InvoiceName) throws SQLException {

        Globle_InvoiceNumber = InvoiceNumber;
        Globle_InvoiveName = InvoiceName;
        loaddataFirsttime();

    }

    public void loaddataFirsttime() throws SQLException {

        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("Select * From INVOICE_DETAILS where INVOICE_NUMBER='" + Globle_InvoiceNumber + "' AND CUSTOMER_NAME='" + Globle_InvoiveName + "'");
        while (resultSet.next()) {

            invoice_Number.setText(resultSet.getString("INVOICE_NUMBER"));
            date.setText(resultSet.getString("INVOICE_DATE"));
            GST_Number.setText(resultSet.getString("PAN_NUMBER"));
            mobile_Number.setText(resultSet.getString("MOBILE_NUMBER"));
            address.setText(resultSet.getString("ADDRESS"));
            Business_Name.setText(resultSet.getString("CUSTOMER_NAME"));
            Customer_Name.setText(resultSet.getString("CONTACT_PERSON"));

            // items are split and store temp(billCalculation) table for farther calculation
            String temp1 = resultSet.getString("ITEMS");
            invoiceItemName = temp1.split(",");
            int sizeItemName = invoiceItemName.length;  // length is same all of 

            String temp2 = resultSet.getString("HSN_CODE");
            invoiceHsnCode = temp2.split(",");

            String temp3 = resultSet.getString("UNITS"); //this table is not present invoicetable
            invoiceUnit = temp3.split(",");
            String temp4 = resultSet.getString("RATE");
            invoiceRate = temp4.split(",");

            String temp5 = resultSet.getString("QTY");
            invoiceQty = temp5.split(",");

            String temp6 = resultSet.getString("AMOUNT");
            invoiceAmount = temp6.split(",");

            String temp7 = resultSet.getString("DISCOUNT");
            invoiceDiscount = temp7.split(",");

            String temp8 = resultSet.getString("TAXABLE_VALUE");
            invoiceTaxableValue = temp8.split(",");

            String temp9 = resultSet.getString("CGST_RATE");
            invoiceCgstRate = temp9.split(",");

            String temp10 = resultSet.getString("CGST_AMOUNT");
            invoiceCgstAmount = temp10.split(",");

            String temp11 = resultSet.getString("SGST_RATE");
            invoiceSgstRate = temp11.split(",");

            String temp12 = resultSet.getString("SGST_AMOUNT");
            invoiceSgstAmount = temp12.split(",");

            String temp13 = resultSet.getString("TOTAL");
            invoiceTotal = temp13.split(",");
//
//            String temp14 = resultSet.getString("STOCK");
//            invoiceStock = temp14.split(",");
//            String temp15 = resultSet.getString("TAXABLECATEGORY");
//            invoiceTaxableCategory = temp15.split(",");
//            String temp16 = resultSet.getString("MRP");
//            invoiceMRP = temp16.split(",");
//            String temp17 = resultSet.getString("SALE_PRICE");
//            invoiceSalePrice = temp17.split(",");

// Store global variable here   set to pojo constraucter
            ObservableList<ADD_items_details> list = FXCollections.observableArrayList();

            int j = 1;
//invoiceUnit[i] , Integer.valueOf(invoiceStock[i]), Integer.valueOf(invoiceTaxableCategory[i]), Float.valueOf(invoiceMRP[i]),   Float.valueOf(invoiceSalePrice[i])

            for (int i = 0; i <= sizeItemName - 1; i++) {

                list.add(new ADD_items_details(j, invoiceItemName[i], invoiceHsnCode[i], invoiceUnit[i], Float.valueOf(invoiceRate[i]),
                        Integer.valueOf(invoiceQty[i]), Float.valueOf(invoiceAmount[i]), Float.valueOf(invoiceDiscount[i]), Float.valueOf(invoiceTaxableValue[i]),
                        Float.valueOf(invoiceCgstRate[i]), Float.valueOf(invoiceCgstAmount[i]), Float.valueOf(invoiceSgstRate[i]), Float.valueOf(invoiceSgstAmount[i]),
                        Float.valueOf(invoiceTotal[i]), 0, 0, 56.25f, 65025f));
                j++;
            }

            invoice_table.setItems(list);

            net_Total_Rate.setText(resultSet.getString("NET_TOTAL_TAXABLE_VALUE"));
            net_Total_Qty.setText(resultSet.getString("NET_TOTAL_QTY"));
            net_Total_Amount.setText(resultSet.getString("NET_TOTAL_AMOUNT"));
            net_Total_Discount.setText(resultSet.getString("NET_TOTAL_DISCOUNT"));
            net_Total_Taxable_value.setText(resultSet.getString("NET_TOTAL_TAXABLE_VALUE"));
            net_Total_Cgst_Amount.setText(resultSet.getString("NET_TOTAL_CGST_AMOUNT"));
            net_Total_Sgst_Amount.setText(resultSet.getString("NET_TOTAL_SGST_AMOUNT"));
            net_Total_Total.setText(resultSet.getString("NET_TOTAL_TOTAL"));

            Global_Total = Float.parseFloat(resultSet.getString("NET_TOTAL_TOTAL"));

            PaidAmount.setText(resultSet.getString("PAID"));
            OutstandingAmount.setText(resultSet.getString("OUTSTANDING"));

        }

    }

    @FXML
    private void canceledInvoiceAction(ActionEvent event) throws SQLException {

        Connection con = DatabaseConnection.getConnection();

        con.setAutoCommit(false);

        PreparedStatement ps = con.prepareStatement("update INVOICE_DETAILS Set STATUS= ? where INVOICE_NUMBER='" + invoice_Number.getText() + "' ");

        ps.setString(1, "Canceled");

        result = ps.executeUpdate();

        if (result > 0) {

            PreparedStatement ps2 = con.prepareStatement("update RECEIPT_DETAILS Set STATUS= ? where INVOICE_NUMBER='" + invoice_Number.getText() + "' ");

            ps2.setString(1, "Canceled");
            int result2 = ps2.executeUpdate();

            alert.setTitle("Validate");
            alert.setHeaderText(null);
            alert.setContentText(" Invoice Caneled successfuly ");
            alert.showAndWait();
            makeuneditable();
            con.commit();

        } else {
            alert.setTitle("Validate");
            alert.setHeaderText(null);
            alert.setContentText(" Invoice not Caneled ");
            alert.showAndWait();
            con.rollback();

        }
        ps.close();

        // update status here calceled
    }

    @FXML
    private void updateInvoiceAction(ActionEvent event) throws SQLException {

// update invoice payment status
        PreparedStatement ps = connection.prepareStatement("update INVOICE_DETAILS Set  PAID=?, OUTSTANDING=? , STATUS=? where INVOICE_NUMBER='" + invoice_Number.getText() + "' ");

        ps.setString(1, PaidAmount.getText());
        ps.setString(2, OutstandingAmount.getText());

        if (Float.parseFloat(OutstandingAmount.getText()) >= 0.0) {

            status = "Pending";

        }
        if (Float.parseFloat(OutstandingAmount.getText()) == 0.0) {

            status = "Paid";

        }
        ps.setString(3, status);

        result = ps.executeUpdate();
        if (result > 0) {

            alert.setTitle("Validate");
            alert.setHeaderText(null);
            alert.setContentText(" Invoice Update successfuly ");
            alert.showAndWait();
            makeuneditable();

        } else {
            alert.setTitle("Validate");
            alert.setHeaderText(null);
            alert.setContentText(" Invoice not update ");
            alert.showAndWait();

        }
        ps.close();
    }

    @FXML
    private void EditInvoiceAction(ActionEvent event) {

//        invoice_Number.setDisable(true);
//        date.setDisable(true);
//
//        Business_Name.setEditable(true);
//        Customer_Name.setEditable(true);
//        GST_Number.setEditable(true);
//        mobile_Number.setEditable(true);
//        address.setEditable(true);
//
//        invoice_table.setEditable(true);
        PaidAmount.setEditable(true);

        OutstandingAmount.setEditable(false);
        canceledInvoice.setDisable(false);
        updateInvoice.setDisable(false);
        EditInvoice.setDisable(true);

    }

    public void makeuneditable() {

        invoice_Number.setDisable(true);
        date.setDisable(true);

        Business_Name.setEditable(false);
        Customer_Name.setEditable(false);
        GST_Number.setEditable(false);
        mobile_Number.setEditable(false);
        address.setEditable(false);

        invoice_table.setEditable(false);

        PaidAmount.setEditable(false);
        OutstandingAmount.setEditable(false);

        canceledInvoice.setDisable(true);
        updateInvoice.setDisable(true);
        EditInvoice.setDisable(false);
    }

//    public void loadDataFromDatabaseToTable() throws SQLException {
//
//        ObservableList<ADD_items_details> list = FXCollections.observableArrayList();
//        Statement stm = connection.createStatement();
//
//        ResultSet resultSet = stm.executeQuery("select * from BILL_CALCULATION");
//        int i = 1;
//        while (resultSet.next()) {
//
//            list.add(new ADD_items_details(i, resultSet.getString("ITEMS"), resultSet.getString("HSN_CODE"), resultSet.getString("UNITS"), resultSet.getFloat("RATE"),
//                    resultSet.getInt("QTY"), resultSet.getFloat("AMOUNT"), resultSet.getFloat("DISCOUNT"), resultSet.getFloat("TAXABLE_VALUE"),
//                    resultSet.getFloat("CGST_RATE"), resultSet.getFloat("CGST_AMOUNT"), resultSet.getFloat("SGST_RATE"), resultSet.getFloat("SGST_AMOUNT"),
//                    resultSet.getFloat("TOTAL"), resultSet.getInt("STOCK"), resultSet.getInt("TAXABLECATEGORY"), resultSet.getFloat("MRP"),
//                    resultSet.getFloat("SALE_PRICE")));
//            i++;
//        }
//
//        invoice_table.setItems(list);
//
//        resultSet.close();
//
//        //   Load net total 
//        Statement statement = connection.createStatement();
//        ResultSet resultSet1 = statement.executeQuery("SELECT SUM(RATE) as r ,SUM(QTY) as q , SUM(AMOUNT) as a,SUM(DISCOUNT) as d,SUM(TAXABLE_VALUE) as txtvalue ,SUM(CGST_AMOUNT) as c,SUM(SGST_AMOUNT) as s,SUM(TOTAL) as t FROM BILL_CALCULATION");
//        while (resultSet1.next()) {
//
//            net_Total_Rate.setText(String.valueOf(resultSet1.getFloat("r")));
//            net_Total_Qty.setText(String.valueOf(resultSet1.getInt("q")));
//            net_Total_Amount.setText(String.valueOf(resultSet1.getFloat("a")));
//            net_Total_Discount.setText(String.valueOf(resultSet1.getFloat("d")));
//            net_Total_Taxable_value.setText(String.valueOf(resultSet1.getFloat("txtvalue")));
//            net_Total_Cgst_Amount.setText(String.valueOf(resultSet1.getFloat("c")));
//            net_Total_Sgst_Amount.setText(String.valueOf(resultSet1.getFloat("s")));
//            net_Total_Total.setText(String.valueOf(resultSet1.getFloat("t")));
//
//            Global_Total = resultSet1.getFloat("t");
//
//            PaidAmount.setText(String.valueOf(resultSet1.getFloat("t")));
//            Globle_paidAmount = Float.parseFloat(PaidAmount.getText());
//            GlobleOutStandingAmount = (Global_Total - Globle_paidAmount);
//            OutstandingAmount.setText(String.valueOf(GlobleOutStandingAmount));
//
//        }
//    }
//
//    public void ClearTable() throws SQLException {
//
//        //PreparedStatement preparedStatement = connection.prepareStatement("TRUNCATE TABLE BILL_CALCULATION");
//        Statement statement = connection.createStatement();
//
//        int result = statement.executeUpdate("DELETE From BILL_CALCULATION");
//        if (result == 0) {
//            Customer_Name.clear();
//            GST_Number.clear();
//            mobile_Number.clear();
//            address.clear();
//
//            PaidAmount.clear();
//            OutstandingAmount.clear();
//
//            loadDataFromDatabaseToTable();
//
//        }
//    }
    @FXML
    private void PaidAmountCalculation(KeyEvent event) {

        Globle_paidAmount = Float.parseFloat(PaidAmount.getText());
        GlobleOutStandingAmount = (Global_Total - Globle_paidAmount);
        OutstandingAmount.setText(String.valueOf(GlobleOutStandingAmount));
    }

    @FXML
    private void invoicePrintAction(ActionEvent event) {

    }

}
