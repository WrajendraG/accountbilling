/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account_billing;

import com.osiersystems.pojos.InvoiceModification;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.StringConverter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import requriedDatabaseConnection.DatabaseConnection;

/**
 * FXML Controller class
 *
 * @author ACER
 */
public class DayWiseSaleController implements Initializable {

    @FXML
    private DatePicker byDate;
    @FXML
    private Button Display;
    @FXML
    private TableView<InvoiceModification> Table_Invoice;
    @FXML
    private TableColumn<InvoiceModification, Integer> Table_InvoiceSrNo;
    @FXML
    private TableColumn<InvoiceModification, String> Table_InvoiceNumber;
    @FXML
    private TableColumn<InvoiceModification, String> Table_PersonName;
    @FXML
    private TableColumn<InvoiceModification, String> Table_DateOfInvoice;
    @FXML
    private TableColumn<InvoiceModification, String> Table_Total_Amount;
    @FXML
    private TableColumn<InvoiceModification, String> Table_Paid_Amount;
    @FXML
    private TableColumn<InvoiceModification, String> Table_Outstanding_Amount;
    @FXML
    private TableColumn<InvoiceModification, String> Table_InvoiceStatus;
    @FXML
    private ToggleGroup radio_Box_InvoiceFilter;

    @FXML
    private RadioButton Radio_All;
    @FXML
    private RadioButton Radio_ByDate;
    @FXML
    private RadioButton Radio_ByDayToDay;

    @FXML
    private DatePicker StratDate;
    @FXML
    private DatePicker EndDate;

    Connection connection;

    boolean flag = false;
    String Global_SqlQuery = null;
    String Global_Table_InvoiceNumber;
    String Global_Table_PersonName;
    String Global_Table_DateOfInvoice;
    String Global_Table_Total_Amount;
    String Global_Table_InvoiceStatus;
    String Globle_Table_Paid_Amount;
    String Globle_Table_Outstanding_Amount;
    Alert alert = new Alert(Alert.AlertType.WARNING);

    float Global_NetTotal_Paid, Global_NetTotal_TotalAmount, Global_NetTotal_Outstandig;

    @FXML
    private TextField NetTotal_TotalAmount;
    @FXML
    private TextField NetTotal_Outstandig;
    @FXML
    private Button exportToExcel;
    @FXML
    private TextField NetTotal_Paid;
    @FXML
    private TextField CanceledAmount;
    float Globle_NetTotal_Canceled_Amount;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        connection = DatabaseConnection.getConnection();
        Table_InvoiceSrNo.setCellValueFactory(new PropertyValueFactory<>("Pojo_InvoiceSrNo"));
        Table_InvoiceSrNo.setStyle("-fx-alignment: CENTER");

        Table_InvoiceNumber.setCellValueFactory(new PropertyValueFactory<>("Pojo_InvoiceNumber"));
        Table_InvoiceNumber.setStyle("-fx-alignment: CENTER");

        Table_PersonName.setCellValueFactory(new PropertyValueFactory<>("Pojo_PersonName"));
        Table_PersonName.setStyle("-fx-alignment: LEFT");

        Table_DateOfInvoice.setCellValueFactory(new PropertyValueFactory<>("Pojo_DateOfInvoice"));
        Table_DateOfInvoice.setStyle("-fx-alignment: CENTER");

        Table_Total_Amount.setCellValueFactory(new PropertyValueFactory<>("Pojo_Total_Amount"));
        Table_Total_Amount.setStyle("-fx-alignment: CENTER");

        Table_Paid_Amount.setCellValueFactory(new PropertyValueFactory<>("Pojo_Paid_Amount"));
        Table_Paid_Amount.setStyle("-fx-alignment: CENTER");

        Table_Outstanding_Amount.setCellValueFactory(new PropertyValueFactory<>("Pojo_Outstanding_Amount"));
        Table_Outstanding_Amount.setStyle("-fx-alignment: CENTER");

        Table_InvoiceStatus.setCellValueFactory(new PropertyValueFactory<>("Pojo_InvoiceStatus"));
        Table_InvoiceStatus.setStyle("-fx-alignment: CENTER");

        byDate.setConverter(new StringConverter<LocalDate>() {

            String pattern = "dd-MMM-yyyy";

            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            {

                byDate.setPromptText(pattern.toLowerCase());

            }

            @Override

            public String toString(LocalDate date) {

                if (date != null) {

                    return dateFormatter.format(date);

                } else {

                    return "";

                }

            }

            @Override

            public LocalDate fromString(String string) {

                if (string != null && !string.isEmpty()) {

                    return LocalDate.parse(string, dateFormatter);

                } else {

                    return null;

                }

            }

        });
        StratDate.setConverter(new StringConverter<LocalDate>() {

            String pattern = "dd-MMM-yyyy";

            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            {

                StratDate.setPromptText(pattern.toLowerCase());

            }

            @Override

            public String toString(LocalDate date) {

                if (date != null) {

                    return dateFormatter.format(date);

                } else {

                    return "";

                }

            }

            @Override

            public LocalDate fromString(String string) {

                if (string != null && !string.isEmpty()) {

                    return LocalDate.parse(string, dateFormatter);

                } else {

                    return null;

                }

            }

        });

        EndDate.setConverter(new StringConverter<LocalDate>() {

            String pattern = "dd-MMM-yyyy";

            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            {

                EndDate.setPromptText(pattern.toLowerCase());

            }

            @Override

            public String toString(LocalDate date) {

                if (date != null) {

                    return dateFormatter.format(date);

                } else {

                    return "";

                }

            }

            @Override

            public LocalDate fromString(String string) {

                if (string != null && !string.isEmpty()) {

                    return LocalDate.parse(string, dateFormatter);

                } else {

                    return null;

                }

            }

        });

        try {
            radio_All_Action();
        } catch (SQLException ex) {
            Logger.getLogger(DayWiseSaleController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void radio_All_Action() throws SQLException {

        if (Radio_All.isSelected()) {
            flag = false;  //  reinitialize again because flag updated (true) at the time of all bill load .

            byDate.getEditor().clear();
            byDate.setDisable(true);

            StratDate.getEditor().clear();
            StratDate.setDisable(true);

            EndDate.getEditor().clear();
            EndDate.setDisable(true);

            Display.setDisable(true);

            Global_SqlQuery = "SELECT * FROM  INVOICE_DETAILS";

            loadDataFromDatabaseToTable();

        }
    }

    @FXML
    private void radio_ByDateAction(ActionEvent event) {

        byDate.setDisable(false);
        StratDate.getEditor().clear();
        StratDate.setDisable(true);
        EndDate.getEditor().clear();
        EndDate.setDisable(true);
        Display.setDisable(false);
    }

    @FXML
    private void radio_ByDayToDayAction(ActionEvent event) {
        byDate.getEditor().clear();
        byDate.setDisable(true);
        StratDate.setDisable(false);
        EndDate.setDisable(false);
        Display.setDisable(false);

    }

    public void loadDataFromDatabaseToTable() throws SQLException {

        Global_NetTotal_TotalAmount = 0;
        Global_NetTotal_Paid = 0;
        Global_NetTotal_Outstandig = 0;
        Globle_NetTotal_Canceled_Amount = 0;
        ObservableList<InvoiceModification> list = FXCollections.observableArrayList();
        Statement stm = connection.createStatement();
        ResultSet resultSet = stm.executeQuery(Global_SqlQuery);
        int i = 1;
        while (resultSet.next()) {

            Global_Table_InvoiceNumber = resultSet.getString("INVOICE_NUMBER");
            Global_Table_PersonName = resultSet.getString("CUSTOMER_NAME");
            Global_Table_DateOfInvoice = resultSet.getString("INVOICE_DATE");

            Global_Table_Total_Amount = resultSet.getString("NET_TOTAL_TOTAL");
            Globle_Table_Paid_Amount = resultSet.getString("PAID");
            Globle_Table_Outstanding_Amount = resultSet.getString("OUTSTANDING");
            Global_Table_InvoiceStatus = resultSet.getString("STATUS");

            if (Global_Table_InvoiceStatus.equalsIgnoreCase("Canceled")) {
                // cancled amount can add within net total
                Globle_NetTotal_Canceled_Amount = Globle_NetTotal_Canceled_Amount + Float.parseFloat(Global_Table_Total_Amount);

            } else {
                Global_NetTotal_TotalAmount = (Global_NetTotal_TotalAmount + Float.parseFloat(Global_Table_Total_Amount)); //for nettotal calculation
                Global_NetTotal_Paid = (Global_NetTotal_Paid + Float.parseFloat(Globle_Table_Paid_Amount));
                Global_NetTotal_Outstandig = (Global_NetTotal_Outstandig + Float.parseFloat(Globle_Table_Outstanding_Amount));
            }

            list.add(new InvoiceModification(i, Global_Table_InvoiceNumber, Global_Table_PersonName, Global_Table_DateOfInvoice, Global_Table_Total_Amount, Globle_Table_Paid_Amount, Globle_Table_Outstanding_Amount, Global_Table_InvoiceStatus));
            i++;
            flag = true;
        }

        Table_Invoice.setItems(list);

        NetTotal_TotalAmount.setText(String.valueOf(Global_NetTotal_TotalAmount));
        NetTotal_Paid.setText(String.valueOf(Global_NetTotal_Paid));
        NetTotal_Outstandig.setText(String.valueOf(Global_NetTotal_Outstandig));

        CanceledAmount.setText(String.valueOf(Globle_NetTotal_Canceled_Amount));

    }

    @FXML
    private void DisplayAction(ActionEvent event) throws SQLException {

        if (Radio_ByDate.isSelected()) {

            Global_SqlQuery = "select  * from INVOICE_DETAILS where INVOICE_DATE = '" + ((TextField) byDate.getEditor()).getText() + "'";

            loadDataFromDatabaseToTable();

        }
        if (Radio_ByDayToDay.isSelected()) {

            Global_SqlQuery = "select  * from INVOICE_DETAILS where INVOICE_DATE between '" + ((TextField) StratDate.getEditor()).getText() + "' AND ' " + ((TextField) EndDate.getEditor()).getText() + "' ";
            loadDataFromDatabaseToTable();
        }

    }

    @FXML
    private void exportToExcelAction(ActionEvent event) throws SQLException, FileNotFoundException, IOException {

        Global_SqlQuery = "SELECT * FROM  INVOICE_DETAILS";

        if (Radio_ByDate.isSelected()) {

            Global_SqlQuery = "select  * from INVOICE_DETAILS where INVOICE_DATE = '" + ((TextField) byDate.getEditor()).getText() + "'";

        }
        if (Radio_ByDayToDay.isSelected()) {

            Global_SqlQuery = "select  * from INVOICE_DETAILS where INVOICE_DATE between '" + ((TextField) StratDate.getEditor()).getText() + "' AND ' " + ((TextField) EndDate.getEditor()).getText() + "' ";

        }

        PreparedStatement ps = connection.prepareStatement(Global_SqlQuery);

        ResultSet rs = ps.executeQuery();

        XSSFWorkbook wb = new XSSFWorkbook();

        XSSFSheet sheet = wb.createSheet("Sale_Report");

        XSSFRow header = sheet.createRow(0);

        header.createCell(0).setCellValue("INVOICE_NUMBER");

        header.createCell(1).setCellValue("INVOICE_DATE");

        header.createCell(2).setCellValue("COMPENY_NAME");

        header.createCell(3).setCellValue("CONTACT_PERSON");

        header.createCell(4).setCellValue("NET_TOTAL_TOTAL");

        header.createCell(5).setCellValue("PAID");
        header.createCell(6).setCellValue("OUTSTANDING");
        header.createCell(7).setCellValue("STATUS");

        sheet.autoSizeColumn(0);

        sheet.autoSizeColumn(1);

        sheet.autoSizeColumn(2);

        sheet.autoSizeColumn(3);

        sheet.autoSizeColumn(4);
        sheet.autoSizeColumn(5);
        sheet.autoSizeColumn(6);
        sheet.autoSizeColumn(7);

        sheet.setColumnWidth(1, 256 * 20);

        sheet.setZoom(150);

        int index = 1;

        while (rs.next()) {

            System.out.println("in while" + index);

            XSSFRow row = sheet.createRow(index);

            row.createCell(0).setCellValue(rs.getString("INVOICE_NUMBER"));

            row.createCell(1).setCellValue(rs.getString("INVOICE_DATE"));

            row.createCell(2).setCellValue(rs.getString("CUSTOMER_NAME"));

            row.createCell(3).setCellValue(rs.getString("CONTACT_PERSON"));

            row.createCell(4).setCellValue(rs.getString("NET_TOTAL_TOTAL"));
            row.createCell(5).setCellValue(rs.getString("PAID"));
            row.createCell(6).setCellValue(rs.getString("OUTSTANDING"));
            row.createCell(7).setCellValue(rs.getString("STATUS"));

            index++;

        }

        XSSFRow row = sheet.createRow(index);
        row.createCell(0).setCellValue("---");
        row.createCell(1).setCellValue("---");
        row.createCell(2).setCellValue("---");
        row.createCell(3).setCellValue("---");
        row.createCell(4).setCellValue("---");
        row.createCell(5).setCellValue("---");
        row.createCell(6).setCellValue("---");
        row.createCell(7).setCellValue("---");

        XSSFRow row1 = sheet.createRow(index + 1);
        row1.createCell(0).setCellValue("");
        row1.createCell(1).setCellValue("");
        row1.createCell(2).setCellValue("");//////////////////

        row1.createCell(3).setCellValue("");
        row1.createCell(4).setCellValue(NetTotal_TotalAmount.getText());//////////////////
        row1.createCell(5).setCellValue(NetTotal_Paid.getText());
        row1.createCell(6).setCellValue(NetTotal_Outstandig.getText());
        row1.createCell(7).setCellValue("");

        FileOutputStream fileout = new FileOutputStream("SaleReport.XLSX");

        wb.write(fileout);

        fileout.close();

        rs.close();

        ps.close();

        connection.close();

        alert.setTitle("Success");
        alert.setContentText("Your Export completed successfully");

        alert.showAndWait();

    }

}
