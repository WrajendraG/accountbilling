package account_billing;

import com.osiersystems.pojos.ViewPurchase;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.StringConverter;
import requriedDatabaseConnection.DatabaseConnection;

/**
 * FXML Controller class
 *
 * @author Rahul Jadhav
 */
public class ViewPurchaseController implements Initializable {

    @FXML
    private TableView<ViewPurchase> table_viewAccount;
    @FXML
    private ToggleGroup RadioButton;
    @FXML
    private RadioButton radio_button_DeActive;
    @FXML
    private TableColumn<ViewPurchase, String> V_Purchase_Srno;
    @FXML
    private TableColumn<ViewPurchase, String> V_P_Name_Of_Supplier;

    @FXML
    private TableColumn<ViewPurchase, String> V_P_Invoice_No;

    @FXML
    private TableColumn<ViewPurchase, String> V_P_Date;

    // String globle_column_purchase_date;
    @FXML
    private TableColumn<ViewPurchase, String> V_P_Amount;

    // Float globle_Column_Amount;
    @FXML
    private TableColumn<ViewPurchase, String> V_P_Paid;

    // Float globle_column_Paid;
    @FXML
    private TableColumn<ViewPurchase, String> V_P_OutSanding;

    // Float globle_column_Outstanding;
    @FXML
    private TableColumn<ViewPurchase, String> V_P_Status;

    // String globle_column_Purchase_Mode;
    @FXML
    private TextField text_By_Name;
    @FXML
    private DatePicker text_By_Date;
    @FXML
    private DatePicker Text_From_Date;
    @FXML
    private DatePicker Text_To_Date;
    @FXML
    private RadioButton Filter_All;
    @FXML
    private RadioButton Filter_Paid;

    /**
     * Initializes the controller class.
     */
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    String globle_Sql;
    String sql;

    Stage stage;
    Parent p;
    @FXML
    private TextField Text_Total_Purchases;
    @FXML
    private TextField Text_Total_Paid;
    @FXML
    private TextField Text_total_outstanding;
    @FXML
    private RadioButton Radio_By_Date;
    @FXML
    private ToggleGroup Radiobuttonsfordate;
    @FXML
    private RadioButton Radio_From_To_Date;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Filter_All.setSelected(true);
        connection = DatabaseConnection.getConnection();
        V_Purchase_Srno.setCellValueFactory(new PropertyValueFactory<>("Pojo_SrNo"));
        V_Purchase_Srno.setStyle("-fx-alignment: CENTER");

        V_P_Name_Of_Supplier.setCellValueFactory(new PropertyValueFactory<>("Pojo_Name_of_Supplier"));
        V_P_Name_Of_Supplier.setStyle("-fx-alignment: CENTER");

        V_P_Invoice_No.setCellValueFactory(new PropertyValueFactory<>("Pojo_Invoice_No"));
        V_P_Invoice_No.setStyle("-fx-alignment: CENTER");

        V_P_Date.setCellValueFactory(new PropertyValueFactory<>("Pojo_Date"));
        V_P_Date.setStyle("-fx-alignment: CENTER");

        V_P_Amount.setCellValueFactory(new PropertyValueFactory<>("Pojo_Amount"));
        V_P_Amount.setStyle("-fx-alignment: CENTER");

        V_P_Paid.setCellValueFactory(new PropertyValueFactory<>("Pojo_Paid"));
        V_P_Paid.setStyle("-fx-alignment: CENTER");

        V_P_OutSanding.setCellValueFactory(new PropertyValueFactory<>("Pojo_Outstanding"));
        V_P_OutSanding.setStyle("-fx-alignment: CENTER");

        V_P_Status.setCellValueFactory(new PropertyValueFactory<>("Pojo_Status"));
        V_P_Status.setStyle("-fx-alignment: CENTER");

        try {

            globle_Sql = "select * from purchase_details";
            sql = "select sum(amount) as total_purchases,sum(paid) as total_paid,sum(outstanding) as total_outstanding from purchase_details";

            loadDataFromDatabaseToTable();
        } catch (SQLException ex) {
            Logger.getLogger(ViewCreatedAccountController.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            bindItems1();

        } catch (SQLException ex) {
            Logger.getLogger(ViewCreatedAccountController.class.getName()).log(Level.SEVERE, null, ex);
        }
        setCellValueFromToTextField();
        //set date Pattern to FromDate

        Text_From_Date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "yyyy-MM-dd";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            {
                Text_From_Date.setPromptText(pattern.toLowerCase());
            }

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
        Text_To_Date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "yyyy-MM-dd";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            {
                Text_To_Date.setPromptText(pattern.toLowerCase());
            }

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

    }

    public void bindItems1() throws SQLException {

        try {
            text_By_Name.setText("");
            ObservableList categoryList1 = FXCollections.observableArrayList();
            // txt_ItemName.setText(null);
            categoryList1.removeAll(categoryList1);

            connection = DatabaseConnection.getConnection();
            preparedStatement = connection.prepareStatement(" select NAME from purchase_details");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                categoryList1.add(resultSet.getString("NAME"));

            }
            ArrayList observableList1 = new ArrayList<String>(new HashSet<String>(categoryList1));

            org.controlsfx.control.textfield.TextFields.bindAutoCompletion(text_By_Name, observableList1);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

    }

    @FXML
    private void radioBoxSearchAll(ActionEvent event) throws SQLException {
        // System.out.println("am inside All man...");
        globle_Sql = "select * from purchase_details";
        sql = "select sum(amount) as total_purchases,sum(paid) as total_paid,sum(outstanding) as total_outstanding from purchase_details ";

        loadDataFromDatabaseToTable();
    }

    @FXML
    private void setCellValueFromToTextField(MouseEvent event) {
        //System.out.println("hey am click on table row");
    }

    @FXML
    private void SearchButton(ActionEvent event) throws SQLException {

//*********************************************************************************************************
//condition for any  Three  will get selected
        if ((Filter_All.isSelected() || Filter_Paid.isSelected() || radio_button_DeActive.isSelected()) && !("".equals(text_By_Name.getText())) && (!("".equals(Text_From_Date.getEditor().getText())) && !("".equals(Text_From_Date.getEditor().getText())))) {
            System.out.println("yes you are ready for ALL three  selected");
            globle_Sql = "select * from purchase_details where name='" + text_By_Name.getText() + "' AND purchase_date between date('" + Text_From_Date.getEditor().getText() + "') AND date('" + Text_To_Date.getEditor().getText() + "')";
            sql = "select sum(amount) as total_purchases,sum(paid) as total_paid,sum(outstanding) as total_outstanding from purchase_details where name='" + text_By_Name.getText() + "' AND purchase_date between date('" + Text_From_Date.getEditor().getText() + "') AND date('" + Text_To_Date.getEditor().getText() + "') ";

            if (Filter_Paid.isSelected()) {
                System.out.println("yes you are ready for ALL three with paid");
                globle_Sql = globle_Sql + " AND outstanding <=0";
                sql = sql + " AND outstanding <=0";
            } else if (radio_button_DeActive.isSelected()) {
                System.out.println("yes you are ready for ALL three with outstanding");
                globle_Sql = globle_Sql + " AND outstanding >0";
                sql = sql + " AND outstanding >0";
            }
        } //Condtion for any two will selected
        else if ((!("".equals(text_By_Name.getText())) && Filter_All.isSelected()) || (!("".equals(text_By_Name.getText())) && Filter_Paid.isSelected()) || (!("".equals(text_By_Name.getText())) && radio_button_DeActive.isSelected()) || ((!("".equals(Text_From_Date.getEditor().getText())) && !("".equals(Text_From_Date.getEditor().getText()))) && Filter_All.isSelected()) || ((!("".equals(Text_From_Date.getEditor().getText())) && !("".equals(Text_From_Date.getEditor().getText()))) && Filter_Paid.isSelected()) || ((!("".equals(Text_From_Date.getEditor().getText())) && !("".equals(Text_From_Date.getEditor().getText()))) && radio_button_DeActive.isSelected()) || (!("".equals(text_By_Name.getText())) && !("".equals(Text_From_Date.getEditor().getText())) && !("".equals(Text_From_Date.getEditor().getText())))) {
            // System.out.println("you are in any two selected");
            globle_Sql = "select * from purchase_details";
            sql = "select sum(amount) as total_purchases,sum(paid) as total_paid,sum(outstanding) as total_outstanding from purchase_details ";

            if (!("".equals(text_By_Name.getText())) && (!("".equals(Text_From_Date.getEditor().getText())) && !("".equals(Text_From_Date.getEditor().getText())))) {

                globle_Sql = globle_Sql + " where name='" + text_By_Name.getText() + "' AND purchase_date between date('" + Text_From_Date.getEditor().getText() + "') AND date('" + Text_To_Date.getEditor().getText() + "')";
                sql = sql + "where name='" + text_By_Name.getText() + "' AND purchase_date between date('" + Text_From_Date.getEditor().getText() + "') AND date('" + Text_To_Date.getEditor().getText() + "')";
            }
            if (!("".equals(text_By_Name.getText()))) {
                globle_Sql = globle_Sql + " where name='" + text_By_Name.getText() + "'";
                sql = sql + " where name='" + text_By_Name.getText() + "'";
                if (Filter_Paid.isSelected()) {
                    // System.out.println("you are in  two text_By_Name  and Filter_Paid selected");
                    globle_Sql = globle_Sql + " AND outstanding <=0";
                    sql = sql + "AND outstanding <=0";
                } else if (radio_button_DeActive.isSelected()) {
                    // System.out.println("you are in  two text_By_Name  and outstanding selected");
                    globle_Sql = globle_Sql + " AND outstanding >0";
                    sql = sql + "AND outstanding >0";
                }
            }
            if ((!("".equals(Text_From_Date.getEditor().getText())) && !("".equals(Text_From_Date.getEditor().getText())))) {
                globle_Sql = "select * from purchase_details where  purchase_date between date('" + Text_From_Date.getEditor().getText() + "') AND date('" + Text_To_Date.getEditor().getText() + "')";
                sql = "select sum(amount) as total_purchases,sum(paid) as total_paid,sum(outstanding) as total_outstanding from purchase_details where  purchase_date between date('" + Text_From_Date.getEditor().getText() + "') AND date('" + Text_To_Date.getEditor().getText() + "')";

                if (Filter_Paid.isSelected()) {
                    //System.out.println("you are in  two Text_From_Date  and Filter_Paid selected");
                    globle_Sql = globle_Sql + " AND outstanding <=0";
                    sql = sql + " AND outstanding <=0";
                } else if (radio_button_DeActive.isSelected()) {
                    System.out.println("you are in  two Text_From_Date  and outstanding selected");
                    globle_Sql = globle_Sql + " AND outstanding >0";
                    sql = sql + "AND outstanding >0";
                }
            }

        } //condition for any  one  will get selected
        else if ((Filter_All.isSelected() || Filter_Paid.isSelected() || radio_button_DeActive.isSelected()) || !("".equals(text_By_Name.getText())) || (!("".equals(Text_From_Date.getEditor().getText())) && !("".equals(Text_From_Date.getEditor().getText())))) {
            //System.out.println("yes you are ready for any one  selected");
            globle_Sql = "select * from purchase_details";
            sql = "select sum(amount) as total_purchases,sum(paid) as total_paid,sum(outstanding) as total_outstanding from purchase_details";

            if (!("".equals(text_By_Name.getText()))) {
                // System.out.println("yes you are ready for text_By_Name  selected");
                globle_Sql = globle_Sql + " where name = '" + text_By_Name.getText() + "'";
                sql = sql + "where name = '" + text_By_Name.getText() + "'";
            } else if ((!("".equals(Text_From_Date.getEditor().getText())) && !("".equals(Text_From_Date.getEditor().getText())))) {
                // System.out.println("yes you are ready for Text_From_Date  selected");
                globle_Sql = globle_Sql + " where purchase_date between date('" + Text_From_Date.getEditor().getText() + "') AND date('" + Text_To_Date.getEditor().getText() + "')";
                sql = sql + "where purchase_date between date('" + Text_From_Date.getEditor().getText() + "') AND date('" + Text_To_Date.getEditor().getText() + "')";
            } else if ((Filter_All.isSelected() || Filter_Paid.isSelected() || radio_button_DeActive.isSelected())) {
                if (Filter_Paid.isSelected()) {
                    //  System.out.println("yes you are ready for Filter_Paid  selected");
                    globle_Sql = globle_Sql + " where outstanding <=0";
                    sql = sql + "where outstanding <=0";
                } else if (radio_button_DeActive.isSelected()) {
                    //System.out.println("yes you are ready for Filter_outstanding selected");
                    globle_Sql = globle_Sql + " where outstanding >0";
                    sql = sql + "where outstanding >0";
                }
            }

        }
        System.out.println("query for one out of three=" + globle_Sql);
        System.out.println("query for one out of three=" + sql);
        loadDataFromDatabaseToTable();

    }

    @FXML
    private void ResetButton(ActionEvent event) throws SQLException {
        text_By_Name.setText(null);
        text_By_Date.setValue(null);

        Text_From_Date.setValue(null);
        Text_To_Date.setValue(null);

        globle_Sql = "select * from purchase_details ";
        sql = "select sum(amount) as total_purchases,sum(paid) as total_paid,sum(outstanding) as total_outstanding from purchase_details ";

        loadDataFromDatabaseToTable();
    }

    @FXML
    private void radioBoxSearchPaid(ActionEvent event) throws SQLException {
        globle_Sql = "select * from purchase_details where  outstanding <=0";
        sql = "select sum(amount) as total_purchases,sum(paid) as total_paid,sum(outstanding) as total_outstanding from purchase_details where  outstanding <=0";

        loadDataFromDatabaseToTable();
    }

    @FXML
    private void radioBoxSearchOutStanding(ActionEvent event) throws SQLException {

        globle_Sql = "select * from purchase_details where  outstanding >0";
        sql = "select sum(amount) as total_purchases,sum(paid) as total_paid,sum(outstanding) as total_outstanding from purchase_details where  outstanding >0";

        loadDataFromDatabaseToTable();
    }

    public void loadDataFromDatabaseToTable() throws SQLException {
           connection = DatabaseConnection.getConnection();
        ObservableList<ViewPurchase> list = FXCollections.observableArrayList();
        Statement stm = connection.createStatement();

        ResultSet resultSet = stm.executeQuery(globle_Sql);
        int i = 1;
        while (resultSet.next()) {

            String globle_column_NAME = resultSet.getString("NAME");
            String globle_column_Invoice_NO = resultSet.getString("Invoice_NO");

           

            String globle_column_purchase_date = resultSet.getString("purchase_date");
            String globle_Column_Amount = String.valueOf(resultSet.getFloat("Amount"));
            String globle_column_Paid = String.valueOf(resultSet.getFloat("Paid"));
            String globle_column_Outstanding = String.valueOf(resultSet.getFloat("Outstanding"));
            String globle_column_Purchase_Mode = resultSet.getString("Purchase_Mode");

            list.add(new ViewPurchase(i, globle_column_NAME, globle_column_Invoice_NO, globle_column_purchase_date, globle_Column_Amount, globle_column_Paid, globle_column_Outstanding, globle_column_Purchase_Mode));
            i++;

        }
        table_viewAccount.setItems(list);

        Statement stm1 = connection.createStatement();

        ResultSet resultSet1 = stm1.executeQuery(sql);

        while (resultSet1.next()) {
            Text_Total_Purchases.setText(String.valueOf(resultSet1.getFloat("total_purchases")));
            Text_Total_Paid.setText(String.valueOf(resultSet1.getFloat("total_paid")));
            Text_total_outstanding.setText(String.valueOf(resultSet1.getFloat("total_outstanding")));

        }
        resultSet1.close();
        stm1.close();
        resultSet.close();
        stm.close();

    }

    private void setCellValueFromToTextField() {
        System.out.println("we are on custom click");
        table_viewAccount.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                ViewPurchase list = table_viewAccount.getItems().get(table_viewAccount.getSelectionModel().getSelectedIndex());

                String Invoice_No = list.getPojo_Invoice_No();
                String Name_of_Supplier = list.getPojo_Name_of_Supplier();

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("ViewSingleRecordOfPurchaser.fxml"));
                try {
                    loader.load();
                } catch (Exception e) {
                }
                System.out.println("Invoice_No in viewPurchase" + Invoice_No);
                System.out.println("Name_of_Supplier in viewPurchase" + Name_of_Supplier);
                ViewSingleRecordOfPurchaserController display = loader.getController();
                display.setTextforViewAccount(Invoice_No, Name_of_Supplier);
                p = loader.getRoot();
                stage = new Stage();
                stage.resizableProperty().setValue(Boolean.FALSE);// disible
                stage.initStyle(StageStyle.UTILITY);// hide 
                stage.setScene(new Scene(p));
                stage.showAndWait();

                try {
                    loadDataFromDatabaseToTable();
                } catch (SQLException ex) {
                    Logger.getLogger(ViewCreatedAccountController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
    }
}
