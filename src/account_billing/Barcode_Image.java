package account_billing;


import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;

public class Barcode_Image {


                    public static byte[] createImage(String barcodeLogic) throws IOException

                               {


                            Code128Bean code128 = new Code128Bean();
                            code128.setHeight(8f); // 15f
                            code128.setModuleWidth(0.2); // 0.3
                            code128.setQuietZone(10);
                            code128.doQuietZone(true);
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            BitmapCanvasProvider canvas = new BitmapCanvasProvider(baos, "image/x-png", 200, BufferedImage.TYPE_BYTE_BINARY, false, 0); //300
                            code128.generateBarcode(canvas, barcodeLogic);
                            canvas.finish();

                              byte img [] = baos.toByteArray();

                              return  img;



                                  }
}
