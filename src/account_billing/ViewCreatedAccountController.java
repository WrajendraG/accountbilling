package account_billing;

import com.osiersystems.pojos.ViewAccounts;
import com.qoppa.pdf.javascript.Field;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import org.apache.poi.hssf.record.formula.functions.False;
import requriedDatabaseConnection.DatabaseConnection;

/**
 * FXML Controller class
 *
 * @author Niteen
 */
public class ViewCreatedAccountController implements Initializable {

    @FXML
    private TableView<ViewAccounts> table_viewAccount;

    @FXML
    private TableColumn<ViewAccounts, Integer> column_Srno;
    int globle_column_Srno = 0;

    @FXML
    private TableColumn<ViewAccounts, String> column_BusinessName;
    String globle_column_BusinessName;

    @FXML
    private TableColumn<ViewAccounts, Integer> column_MobileNumber;
    long globle_column_MobileNumber;

    @FXML
    private TableColumn<ViewAccounts, String> column_EmailID;
    String globle_column_EmailID;

    @FXML
    private TableColumn<ViewAccounts, String> column_City;
    String globle_column_City;

    @FXML
    private TableColumn<ViewAccounts, String> column_CountactPerson;
    String globle_column_CountactPerson;

    @FXML
    private TableColumn<ViewAccounts, String> column_Status;
    String globle_column_Status;

    @FXML
    private TableColumn<ViewAccounts, Button> column_Action;
    Label globle_column_Action;

    @FXML
    private TextField text_BusinessName;
    @FXML
    private TextField text_CoutactPerson;
    @FXML
    private TextField text_City;

    Connection connection;

    Stage stage;
    Parent p;

    String globle_Sql;
    int check = 0;

    // String globle_business_EmailID;
    Alert alert = new Alert(Alert.AlertType.WARNING);
    @FXML
    private RadioButton radio_button_Active;

    @FXML
    private ToggleGroup RadioButton;
    @FXML
    private RadioButton radio_button_DeActive;
    @FXML
    private RadioButton radio_button_All;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        connection = DatabaseConnection.getConnection();

        /*
           
           private Integer SrNo;
    private String BusinessName;
    private Integer MobileNumber;
    private String EmailID;
    private String City;
    private String CountactPerson;
    private Button Action;
         */
        column_Srno.setCellValueFactory(new PropertyValueFactory<>("SrNo"));
        column_Srno.setStyle("-fx-alignment: CENTER");

        column_BusinessName.setCellValueFactory(new PropertyValueFactory<>("BusinessName"));
        column_BusinessName.setStyle("-fx-alignment: LEFT");

        column_MobileNumber.setCellValueFactory(new PropertyValueFactory<>("MobileNumber"));
        column_MobileNumber.setStyle("-fx-alignment: CENTER");

        column_EmailID.setCellValueFactory(new PropertyValueFactory<>("EmailID"));
        column_EmailID.setStyle("-fx-alignment: LEFT");

        column_City.setCellValueFactory(new PropertyValueFactory<>("City"));
        column_City.setStyle("-fx-alignment: LEFT");

        column_CountactPerson.setCellValueFactory(new PropertyValueFactory<>("CountactPerson"));
        column_CountactPerson.setStyle("-fx-alignment: LEFT");

        column_Status.setCellValueFactory(new PropertyValueFactory<>("Status"));
        column_Status.setStyle("-fx-alignment: LEFT");

        column_Action.setCellValueFactory(new PropertyValueFactory<>("Action"));
        column_Action.setStyle("-fx-alignment: CENTER");

        try {
            bindItems1();
            bindItems2();
            bindItems3();
        } catch (SQLException ex) {
            Logger.getLogger(ViewCreatedAccountController.class.getName()).log(Level.SEVERE, null, ex);
        }

        // TODO
        text_BusinessName.setText("");
        text_CoutactPerson.setText("");
        text_City.setText("");

        try {

            globle_Sql = "select * from CUSTOMER_ACCOUNT ";
            loadDataFromDatabaseToTable();
        } catch (SQLException ex) {
            Logger.getLogger(ViewCreatedAccountController.class.getName()).log(Level.SEVERE, null, ex);
        }

        setCellValueFromToTextField();

    }

    public void bindItems1() throws SQLException {
        text_BusinessName.setText(null);
        ObservableList categoryList1 = FXCollections.observableArrayList();
        // txt_ItemName.setText(null);
        categoryList1.removeAll(categoryList1);

        connection = DatabaseConnection.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(" select BUSINESS_NAME from CUSTOMER_ACCOUNT");
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {

            categoryList1.add(resultSet.getString("BUSINESS_NAME"));

        }
        ArrayList observableList1 = new ArrayList<String>(new HashSet<String>(categoryList1));

        org.controlsfx.control.textfield.TextFields.bindAutoCompletion(text_BusinessName, observableList1);

        preparedStatement.close();

    }

    public void bindItems2() throws SQLException {
        text_BusinessName.setText(null);
        ObservableList categoryList2 = FXCollections.observableArrayList();
        // txt_ItemName.setText(null);
        categoryList2.removeAll(categoryList2);

        connection = DatabaseConnection.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(" select CONTACT_PERSON from CUSTOMER_ACCOUNT ");
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {

            categoryList2.add(resultSet.getString("CONTACT_PERSON"));

        }
        ArrayList observableList2 = new ArrayList<String>(new HashSet<String>(categoryList2));

        org.controlsfx.control.textfield.TextFields.bindAutoCompletion(text_CoutactPerson, observableList2);

        preparedStatement.close();

    }

    public void bindItems3() throws SQLException {
        text_BusinessName.setText(null);
        ObservableList categoryList3 = FXCollections.observableArrayList();
        // txt_ItemName.setText(null);
        categoryList3.removeAll(categoryList3);

        connection = DatabaseConnection.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("select PERSON_ADDRESS_CITY from CUSTOMER_ACCOUNT ");
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {

            categoryList3.add(resultSet.getString("PERSON_ADDRESS_CITY"));

        }
        ArrayList observableList3 = new ArrayList<String>(new HashSet<String>(categoryList3));
        org.controlsfx.control.textfield.TextFields.bindAutoCompletion(text_City, observableList3);

        preparedStatement.close();

    }

    public void loadDataFromDatabaseToTable() throws SQLException {

        ObservableList<ViewAccounts> list = FXCollections.observableArrayList();
        Statement stm = connection.createStatement();
        ResultSet resultSet = stm.executeQuery(globle_Sql);
        int i = 1;
        while (resultSet.next()) {
            check = 1;
            // sr i
            // globle_business_EmailID = resultSet.getString("BUSINESS_EMAIL");
            globle_column_BusinessName = resultSet.getString("BUSINESS_NAME");
            globle_column_MobileNumber = Long.parseLong(resultSet.getString("PERSON_MOBILE_NO"));
            globle_column_EmailID = resultSet.getString("PERSON_EMAIL");
            globle_column_City = resultSet.getString("PERSON_ADDRESS_CITY");
            globle_column_CountactPerson = resultSet.getString("CONTACT_PERSON");
            globle_column_Status = resultSet.getString("STATUS");
            globle_column_Action = new Label("Click Here");

            list.add(new ViewAccounts(i, globle_column_BusinessName, globle_column_MobileNumber, globle_column_EmailID, globle_column_City, globle_column_CountactPerson, globle_column_Status, globle_column_Action));
            i++;

        }

        table_viewAccount.setItems(list);

        text_BusinessName.setText("");
        text_CoutactPerson.setText("");
        text_City.setText("");

    }

    @FXML
    private void SearchAction(ActionEvent event) throws SQLException {

        if (!(text_BusinessName.getText().isEmpty()) && !(text_CoutactPerson.getText().isEmpty()) && !(text_City.getText().isEmpty())) {
            globle_Sql = "select * from CUSTOMER_ACCOUNT where BUSINESS_NAME ='" + text_BusinessName.getText() + "' AND CONTACT_PERSON='" + text_CoutactPerson.getText() + "' AND PERSON_ADDRESS_CITY='" + text_City.getText() + "' ";

            if (radio_button_Active.isSelected()) {

                globle_Sql = globle_Sql + " AND status = 'Active'";

            } else if (radio_button_DeActive.isSelected()) {

                globle_Sql = globle_Sql + "  AND status = 'Deactive' ";

            }

            loadDataFromDatabaseToTable();

            if (check == 0) {
                alert.setTitle("Validate");
                alert.setHeaderText(null);
                alert.setContentText("Record Not Found.....");
                alert.showAndWait();

            }
            check = 0;     /// Reset  check for next filter...

        } else if ((!(text_BusinessName.getText().isEmpty()) && !(text_CoutactPerson.getText().isEmpty())) || (!(text_BusinessName.getText().isEmpty()) && !(text_City.getText().isEmpty())) || (!(text_CoutactPerson.getText().isEmpty()) && !(text_City.getText().isEmpty()))) {

            if ((!(text_BusinessName.getText().isEmpty()) && !(text_CoutactPerson.getText().isEmpty()))) {

                globle_Sql = "select *from CUSTOMER_ACCOUNT where BUSINESS_NAME='" + text_BusinessName.getText() + "' AND CONTACT_PERSON='" + text_CoutactPerson.getText() + "'";

                if (radio_button_Active.isSelected()) {

                    globle_Sql = globle_Sql + " AND status = 'Active'";

                } else if (radio_button_DeActive.isSelected()) {

                    globle_Sql = globle_Sql + "  AND status = 'Deactive' ";

                }

                loadDataFromDatabaseToTable();
                if (check == 0) {
                    alert.setTitle("Validate");
                    alert.setHeaderText(null);
                    alert.setContentText("Record Not Found.....");
                    alert.showAndWait();

                }
                check = 0;     /// Reset  check for next filter...

            } else if ((!(text_BusinessName.getText().isEmpty()) && !(text_City.getText().isEmpty()))) {

                globle_Sql = "select * from CUSTOMER_ACCOUNT where BUSINESS_NAME='" + text_BusinessName.getText() + "' AND PERSON_ADDRESS_CITY='" + text_City.getText() + "'";

                if (radio_button_Active.isSelected()) {

                    globle_Sql = globle_Sql + " AND status = 'Active'";

                } else if (radio_button_DeActive.isSelected()) {

                    globle_Sql = globle_Sql + "  AND status = 'Deactive' ";

                }

                loadDataFromDatabaseToTable();

                if (check == 0) {
                    alert.setTitle("Validate");
                    alert.setHeaderText(null);
                    alert.setContentText("Record Not Found.....");
                    alert.showAndWait();

                }
                check = 0;     /// Reset  check for next filter...

            } else if ((!(text_CoutactPerson.getText().isEmpty()) && !(text_City.getText().isEmpty()))) {

                globle_Sql = "select *from CUSTOMER_ACCOUNT where CONTACT_PERSON='" + text_CoutactPerson.getText() + "' AND PERSON_ADDRESS_CITY='" + text_City.getText() + "'";

                if (radio_button_Active.isSelected()) {

                    globle_Sql = globle_Sql + " AND status = 'Active'";

                } else if (radio_button_DeActive.isSelected()) {

                    globle_Sql = globle_Sql + "  AND status = 'Deactive' ";

                }

                loadDataFromDatabaseToTable();

                if (check == 0) {
                    alert.setTitle("Validate");
                    alert.setHeaderText(null);
                    alert.setContentText("Record Not Found.....");
                    alert.showAndWait();

                }
                check = 0;     /// Reset  check for next filter...

            }
        } else if (!(text_BusinessName.getText().isEmpty()) || !(text_CoutactPerson.getText().isEmpty()) || !(text_City.getText().isEmpty())) {

            if (!(text_BusinessName.getText().isEmpty())) {
                globle_Sql = "select * from CUSTOMER_ACCOUNT where BUSINESS_NAME='" + text_BusinessName.getText() + "' ";

                if (radio_button_Active.isSelected()) {

                    globle_Sql = globle_Sql + " AND status = 'Active'";

                } else if (radio_button_DeActive.isSelected()) {

                    globle_Sql = globle_Sql + "  AND status = 'Deactive' ";

                }

                loadDataFromDatabaseToTable();

                if (check == 0) {
                    alert.setTitle("Validate");
                    alert.setHeaderText(null);
                    alert.setContentText("Record Not Found.....");
                    alert.showAndWait();

                }
                check = 0;     /// Reset  check for next filter...

            } else if (!(text_CoutactPerson.getText().isEmpty())) {
                globle_Sql = "select * from CUSTOMER_ACCOUNT where CONTACT_PERSON='" + text_CoutactPerson.getText() + "' ";

                if (radio_button_Active.isSelected()) {

                    globle_Sql = globle_Sql + " AND status = 'Active'";

                } else if (radio_button_DeActive.isSelected()) {

                    globle_Sql = globle_Sql + "  AND status = 'Deactive' ";

                }
                loadDataFromDatabaseToTable();

                if (check == 0) {
                    alert.setTitle("Validate");
                    alert.setHeaderText(null);
                    alert.setContentText("Record Not Found.....");
                    alert.showAndWait();

                }
                check = 0;     /// Reset  check for next filter...

            } else if (!(text_City.getText().isEmpty())) {
                globle_Sql = "select * from CUSTOMER_ACCOUNT where PERSON_ADDRESS_CITY='" + text_City.getText() + "' ";

                if (radio_button_Active.isSelected()) {

                    globle_Sql = globle_Sql + " AND status = 'Active'";

                } else if (radio_button_DeActive.isSelected()) {

                    globle_Sql = globle_Sql + "  AND status = 'Deactive' ";

                }

                loadDataFromDatabaseToTable();

                if (check == 0) {
                    alert.setTitle("Validate");
                    alert.setHeaderText(null);
                    alert.setContentText("Record Not Found.....");
                    alert.showAndWait();

                }
                check = 0;     /// Reset  check for next filter...

            }

        } else {

            alert.setTitle("Validate");
            alert.setHeaderText(null);
            alert.setContentText("Some Thing Wrong Record Not Found.....");
            alert.showAndWait();
        }

    }

    @FXML
    private void resetTextFields(ActionEvent event) throws SQLException {

        text_BusinessName.setText(null);
        text_CoutactPerson.setText(null);
        text_City.setText(null);

        globle_Sql = "select * from CUSTOMER_ACCOUNT ";
        loadDataFromDatabaseToTable();
    }

    @FXML
    private void radioBoxSearchAll() throws SQLException {

        globle_Sql = "select * from CUSTOMER_ACCOUNT ";
        loadDataFromDatabaseToTable();

    }

    @FXML
    private void radioBoxSearchActive() throws SQLException {
        globle_Sql = "select * from CUSTOMER_ACCOUNT where  status = 'Active'";
        loadDataFromDatabaseToTable();

    }

    @FXML
    private void radioBoxSearchDeactive() throws SQLException {
        globle_Sql = "select * from CUSTOMER_ACCOUNT where  status = 'Deactive' ";
        loadDataFromDatabaseToTable();

    }

    private Object getTableRow() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @FXML
    private void setCellValueFromToTextField() {

        table_viewAccount.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                ViewAccounts list = table_viewAccount.getItems().get(table_viewAccount.getSelectionModel().getSelectedIndex());

                String temp = list.getBusinessName();

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("ModificationCreateAccount.fxml"));
                try {
                    loader.load();
                } catch (Exception e) {
                }

                ModificationCreateAccountController display = loader.getController();
                display.setTextforViewAccount(temp);
                p = loader.getRoot();
                stage = new Stage();
                stage.resizableProperty().setValue(Boolean.FALSE);// disible
                stage.initStyle(StageStyle.UTILITY);// hide 
                stage.setScene(new Scene(p));
                stage.showAndWait();

                try {
                    loadDataFromDatabaseToTable();
                } catch (SQLException ex) {
                    Logger.getLogger(ViewCreatedAccountController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

    }

    public void helperMethodOfTwoController() throws SQLException {
        stage.close();
        loadDataFromDatabaseToTable();

    }

}
