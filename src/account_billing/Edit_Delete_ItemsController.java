/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account_billing;

import com.osiersystems.pojos.Edit_Delete_Items;
import com.osiersystems.pojos.ViewAccounts;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import requriedDatabaseConnection.DatabaseConnection;

/**
 * FXML Controller class
 *
 * @author Niteen
 */
public class Edit_Delete_ItemsController implements Initializable {

    @FXML
    private TextField Text_Field_ItemsName;
    @FXML
    private Button Button_Search;
    @FXML
    private ComboBox<String> Combox_Category;
    @FXML
    private RadioButton radio_Box_AllItems;
    @FXML
    private ToggleGroup radio_Box_CategoryFilter;
    @FXML
    private RadioButton radio_Box_ByCategory;
    @FXML
    private TableView<Edit_Delete_Items> Table_Items;
    @FXML
    private TableColumn<Edit_Delete_Items, Integer> Table_Items_SrNo;
    @FXML
    private TableColumn<Edit_Delete_Items, String> Table_Items_Product;
    @FXML
    private TableColumn<Edit_Delete_Items, String> Table_Items_Category;
    @FXML
    private TableColumn<Edit_Delete_Items, String> Table_Items_Price;
    @FXML
    private TableColumn<Edit_Delete_Items, String> Table_Items_GST_Rate;
    @FXML
    private TableColumn<Edit_Delete_Items, String> Table_Items_Percentage;

    String Global_SqlQuery = null;
    String Global_Table_Items_Product;
    String Global_Table_Items_Category;
    String Global_Table_Items_Price;
    String Global_Table_Items_GST_Rate;
    String Global_Table_Items_Percentage;
    Connection connection;

    String Categeory;
    boolean flag = false;

    Stage stage;
    Parent p;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        connection = DatabaseConnection.getConnection();

        Table_Items_SrNo.setCellValueFactory(new PropertyValueFactory<>("Table_Items_SrNo"));
        Table_Items_SrNo.setStyle("-fx-alignment: CENTER");

        Table_Items_Product.setCellValueFactory(new PropertyValueFactory<>("Table_Items_Product"));
        Table_Items_Product.setStyle("-fx-alignment: LEFT");

        Table_Items_Category.setCellValueFactory(new PropertyValueFactory<>("Table_Items_Category"));
        Table_Items_Category.setStyle("-fx-alignment: CENTER");

        Table_Items_Price.setCellValueFactory(new PropertyValueFactory<>("Table_Items_Price"));
        Table_Items_Price.setStyle("-fx-alignment: CENTER");

        Table_Items_GST_Rate.setCellValueFactory(new PropertyValueFactory<>("Table_Items_GST_Rate"));
        Table_Items_GST_Rate.setStyle("-fx-alignment: CENTER");

        Table_Items_Percentage.setCellValueFactory(new PropertyValueFactory<>("Table_Items_Percentage"));
        Table_Items_Percentage.setStyle("-fx-alignment: CENTER");

        try {
            Table_ItemsOnMouseClickedAction();
            radio_BoxAllItemsAction();
        } catch (SQLException ex) {
            Logger.getLogger(Edit_Delete_ItemsController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void Button_SearchAction(ActionEvent event) throws SQLException {

        if (radio_Box_AllItems.isSelected()) {
            Combox_Category.setDisable(true);

            Global_SqlQuery = "select * From ADD_ITEMS_DETAILS where ITEMS_NAME = '" + Text_Field_ItemsName.getText() + "' ";
            loadDataFromDatabaseToTable();
        }

        if (radio_Box_ByCategory.isSelected()) {

            if (Text_Field_ItemsName.getText().isEmpty()) {

                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Validate");
                alert.setHeaderText(null);
                alert.setContentText("Enter Text Field First Then Search");
                alert.showAndWait();

            } else {

                Combox_Category.setDisable(false);
                Categeory = (String) Combox_Category.getSelectionModel().getSelectedItem();
                Global_SqlQuery = "select * From ADD_ITEMS_DETAILS where ITEMS_NAME = '" + Text_Field_ItemsName.getText() + "' and Items_Category = '" + Categeory + "' ";
                loadDataFromDatabaseToTable();

                if (flag == false) {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Chacking Items");
                    alert.setHeaderText(null);
                    alert.setContentText("This type of filter Items Not Found");
                    alert.showAndWait();

                }

            }
        }

    }

    @FXML
    private void Combox_Category_Action(ActionEvent event) throws SQLException {

        Categeory = (String) Combox_Category.getSelectionModel().getSelectedItem();
        Global_SqlQuery = "select * From ADD_ITEMS_DETAILS where Items_Category = '" + Categeory + "' ";
        loadDataFromDatabaseToTable();
        bindItems1();

        if (flag == false) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Chacking Items");
            alert.setHeaderText(null);
            alert.setContentText("This type of filter Items Not Found");
            alert.showAndWait();

        }

    }

    @FXML
    private void radio_BoxAllItemsAction() throws SQLException {

        if (radio_Box_AllItems.isSelected()) {
            Combox_Category.setDisable(true);

            Global_SqlQuery = "SELECT * FROM  ADD_ITEMS_DETAILS";

            loadDataFromDatabaseToTable();

        }

        if (flag == false) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Chacking Items");
            alert.setHeaderText(null);
            alert.setContentText("This type of filter Items Not Found");
            alert.showAndWait();

        }
    }

    @FXML
    private void radio_Box_ByCategoryAction(ActionEvent event) throws SQLException {

        if (radio_Box_ByCategory.isSelected()) {

            Combox_Category.setDisable(false);
            bindCategory();
            //   loadDataFromDatabaseToTable();

        }

    }

    @FXML
    private void Table_ItemsOnMouseClickedAction() {

        Table_Items.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                Edit_Delete_Items list = Table_Items.getItems().get(Table_Items.getSelectionModel().getSelectedIndex());

                String tempitem = list.getTable_Items_Product();
                String tempcategory = list.getTable_Items_Category();

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("Edit_Delete_Items_UsingTable.fxml")); //  name of modifibable itemsFxml
                try {
                    loader.load();
                } catch (Exception e) {
                }

                Edit_Delete_Items_UsingTableController display = loader.getController();  // here Obj Of  Controllel that call method and pass some value to initillze method.
                display.setTextforViewEditItems(tempitem, tempcategory); // change Method here  
                p = loader.getRoot();
                stage = new Stage();
                stage.resizableProperty().setValue(Boolean.FALSE);// disible
                stage.initStyle(StageStyle.UTILITY);// hide 
                stage.setScene(new Scene(p));
                stage.showAndWait();

                try {
                    loadDataFromDatabaseToTable();
                } catch (SQLException ex) {
                    Logger.getLogger(Edit_Delete_ItemsController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

    }

    public void loadDataFromDatabaseToTable() throws SQLException {

        ObservableList<Edit_Delete_Items> list = FXCollections.observableArrayList();
        Statement stm = connection.createStatement();
        ResultSet resultSet = stm.executeQuery(Global_SqlQuery);
        int i = 1;
        while (resultSet.next()) {
            flag = true;
            Global_Table_Items_Product = resultSet.getString("ITEMS_NAME");
            Global_Table_Items_Category = resultSet.getString("ITEMS_CATEGORY");
            Global_Table_Items_Price = resultSet.getString("SALE_PRICE");

            Global_Table_Items_GST_Rate = resultSet.getString("GST_PER");
            String Local_Table_Items_GST_Rate = Global_Table_Items_GST_Rate + " %";

            Global_Table_Items_Percentage = resultSet.getString("DISCOUNT_PER");
            float temp = Float.parseFloat(Global_Table_Items_Percentage); // for removing max point value
            String temp2 = String.format("%.2f", temp);
            String Local_Table_Items_Percentage = temp2 + " %";

            list.add(new Edit_Delete_Items(i, Global_Table_Items_Product, Global_Table_Items_Category, Global_Table_Items_Price, Local_Table_Items_GST_Rate, Local_Table_Items_Percentage));
            i++;

        }

        Table_Items.setItems(list);

        Text_Field_ItemsName.setText("");

    }

    public void bindItems1() {

        Categeory = (String) Combox_Category.getSelectionModel().getSelectedItem();

        ObservableList categoryList = FXCollections.observableArrayList();
        categoryList.removeAll(categoryList);
        Text_Field_ItemsName.clear();
        // TextField filter = TextFields.createClearableTextField();
        //    Text_Field_ItemsName.createClearableTextField();
        try {

            connection = DatabaseConnection.getConnection();

            String query = "select ITEMS_NAME from " + Categeory + " ";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                categoryList.add(resultSet.getString("Items_Name"));
            }

            org.controlsfx.control.textfield.TextFields.bindAutoCompletion(Text_Field_ItemsName, categoryList);

            preparedStatement.close();

        } catch (SQLException ex) {
            Logger.getLogger(Edit_Delete_ItemsController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void bindCategory() {

        //    Categeory = (String) Combox_Category.getSelectionModel().getSelectedItem();
        ObservableList categoryList2 = FXCollections.observableArrayList();

        categoryList2.removeAll(categoryList2);

        try {

            connection = DatabaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("select CATEGORY_NAME from CATEGORY");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                categoryList2.add(resultSet.getString("CATEGORY_NAME"));
            }

            Combox_Category.getItems().removeAll(Combox_Category.getItems());
            Combox_Category.getItems().addAll(categoryList2);
            Combox_Category.getSelectionModel().select("Select");

            preparedStatement.close();

        } catch (SQLException ex) {
            Logger.getLogger(Edit_Delete_ItemsController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

    }
}
