package account_billing;

import com.osiersystems.pojos.NillStock;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import requriedDatabaseConnection.DatabaseConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Nill_StockController implements Initializable {

    @FXML
    private TableView<NillStock> table;

    @FXML
    private TableColumn<NillStock, Integer> sr_No;
    @FXML
    private TableColumn<NillStock, String> Items_name;
       @FXML
    private TableColumn<NillStock, String> Category;
    @FXML
    private TableColumn<NillStock, Integer> stock;
    @FXML
    private TableColumn<NillStock, Integer> table_Purchasing_Price;
    @FXML
    private TableColumn<NillStock, Integer> table_Qty;
    @FXML
    private TableColumn<NillStock, String> table_unit;
    @FXML
    private TableColumn<NillStock, Integer> table_Total;

    //--------------------------------------------------------------------------------------------------------
    private ObservableList<NillStock> list = FXCollections.observableArrayList();
    @FXML
    private TextField netTotal_Purchasing_Price;
    @FXML
    private TextField netTotal_Qty;
    @FXML
    private TextField netTotal_Total;
//--------------------------------------------------------------------------------------

    @FXML
    private TextField txtItems;
    @FXML
    private TextField txtStock;
    @FXML
    private TextField txtPurchingPrice;
    @FXML
    private TextField txtQty;
    @FXML
    private TextField txtUnit;

    @FXML
    private TextField txtTotal;

    // calculation purpose variable
    int t = 0;
    
    Connection connection;

    String itemsCalculationPurpose, unitCalculationPurpose ,Global_Category;
    float puchasingpriceCalculationPurpose, requriedAmountCalculationPurpose;
    int stockcalculationPrupose, qtyCalculationPurpose;
    //------------------------------------------------------------------------------
 String Global_SqlQuery;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // add all entries in table

        sr_No.setCellValueFactory(new PropertyValueFactory<>("srno"));     // fxml table variable and pojo variable mapping is must requried 
        sr_No.setStyle("-fx-alignment: CENTER");   /// table data center purpose
        Items_name.setCellValueFactory(new PropertyValueFactory<>("item_Name"));
        Items_name.setStyle("-fx-alignment: LEFT");
         Category.setCellValueFactory(new PropertyValueFactory<>("Category"));
        Category.setStyle("-fx-alignment: LEFT");
        stock.setCellValueFactory(new PropertyValueFactory<>("stock"));
        stock.setStyle("-fx-alignment: CENTER");
        table_Purchasing_Price.setCellValueFactory(new PropertyValueFactory<>("purchasing_Price"));
        table_Purchasing_Price.setStyle("-fx-alignment: CENTER");
        table_Qty.setCellValueFactory(new PropertyValueFactory<>("qty"));
        table_Qty.setStyle("-fx-alignment: CENTER");
        table_unit.setCellValueFactory(new PropertyValueFactory<>("unit"));
        table_unit.setStyle("-fx-alignment: CENTER");
        table_Total.setCellValueFactory(new PropertyValueFactory<>("total"));
        table_Total.setStyle("-fx-alignment: CENTER");

        //    table_Qty.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn());
        table.setEditable(true);
        txtItems.setEditable(false);
        txtStock.setEditable(false);
        txtPurchingPrice.setEditable(false);
        txtUnit.setEditable(false);
        txtTotal.setEditable(false);

        //  table_Qty.setCellFactory(TextFieldTableCell.forTableColumn());
//        table_Qty.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<NillStock, Integer>>() {
//            @Override
//            public void handle(TableColumn.CellEditEvent<NillStock, Integer> event) {
//          event.getTableView().getItems().get(event.getTablePosition().getRow()).setQty(event.getNewValue());
//            }
//        });
        try {
            setCellValueFromToTextField();
        } catch (SQLException ex) {
            Logger.getLogger(Nill_StockController.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {

            connection = DatabaseConnection.getConnection();
            Statement s = connection.createStatement();
            int a = s.executeUpdate("TRUNCATE TABLE NILSTOCK");
            System.out.println("TRUNCATE Successfully " + a);
            s.close();
            if (a < 0) {
                System.out.println("TRUNCATE Successfully ");
            } else {
                System.out.println(" not TRUNCATE  ");
            }

            int t = 1;
            connection = DatabaseConnection.getConnection();    // get connection  using common package .databaseConnection     
            Statement stm = connection.createStatement();

            ResultSet rs = stm.executeQuery("select * from ADD_ITEMS_DETAILS where stock <= 0");

            while (rs.next()) {

                itemsCalculationPurpose = rs.getString("ITEMS_NAME");
                Global_Category= rs.getString("ITEMS_CATEGORY");
                stockcalculationPrupose = Integer.parseInt(rs.getString("STOCK"));
                puchasingpriceCalculationPurpose = Integer.parseInt(rs.getString("PURCHASE_PRICE"));  // set all database value in poja class  using  obsreable list
                unitCalculationPurpose = rs.getString("UNITS");

                qtyCalculationPurpose = 1;
                requriedAmountCalculationPurpose = puchasingpriceCalculationPurpose;
                insertData();

                t = 1;
            }
            if (t == 0) {

                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Validatin");
                alert.setHeaderText(null);
                alert.setContentText("Nill Stock is not Avalible if you check stock in deatils go to balance stock windows");
                alert.showAndWait();

            }

            stm.close();

        } catch (Exception e) {

            e.printStackTrace();
        }

    }
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    public void insertData() throws SQLException {

        PreparedStatement ps = connection.prepareStatement("insert into NILSTOCK values(?,?,?,?,?,?,?)");
// Store global variable here 
        ps.setString(1, itemsCalculationPurpose);
        ps.setInt(2, stockcalculationPrupose);
        ps.setFloat(3, puchasingpriceCalculationPurpose);
        ps.setInt(4, qtyCalculationPurpose);
        ps.setString(5, unitCalculationPurpose);
        ps.setFloat(6, requriedAmountCalculationPurpose);
        ps.setString(7, Global_Category);

        int result = ps.executeUpdate();

        if (result > 0) {
            System.out.println(" Items Insert successfuly  in table ");
            loadDataFromDatabaseToTable();
        } else {
            System.out.println(" not Insert successfuly in table ");
        }

        ps.close();
    }

    @FXML
    private void Update(ActionEvent event) throws SQLException {

        PreparedStatement ps = connection.prepareStatement("UPDATE NILSTOCK SET ITEMNAME= ?, STOCK=? , PURCHASINGPRICE=?, QTY=?, UNIT=? , REQURIEDAMOUNT=? WHERE ITEMNAME = '" + itemsCalculationPurpose + "'");

        ps.setString(1, itemsCalculationPurpose);
        ps.setInt(2, stockcalculationPrupose);
        ps.setFloat(3, puchasingpriceCalculationPurpose);
        ps.setInt(4, qtyCalculationPurpose);
        ps.setString(5, unitCalculationPurpose);
        ps.setFloat(6, requriedAmountCalculationPurpose);
      
        int result = ps.executeUpdate();
      
        
        if (result > 0) {

            loadDataFromDatabaseToTable();

        } else {

            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Validate owner name");
            alert.setHeaderText(null);
            alert.setContentText(" Enter all filed for update");
            alert.showAndWait();

        }

        

    }
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    private void setCellValueFromToTextField() throws SQLException {

        table.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                NillStock list = table.getItems().get(table.getSelectionModel().getSelectedIndex());

                txtItems.setText("" + list.getItem_Name());
                itemsCalculationPurpose = list.getItem_Name();

                txtStock.setText("" + list.getStock());
                stockcalculationPrupose = list.getStock();

                txtPurchingPrice.setText("" + list.getPurchasing_Price());
                puchasingpriceCalculationPurpose = list.getPurchasing_Price();

                txtQty.setText("" + list.getQty());
                qtyCalculationPurpose = list.getQty();

                txtUnit.setText("" + list.getUnit());
                unitCalculationPurpose = list.getUnit();

                txtTotal.setText("" + list.getTotal());
                requriedAmountCalculationPurpose = list.getTotal();

            }
        });

    }
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    public void loadDataFromDatabaseToTable() throws SQLException 
    {

        ObservableList<NillStock> list = FXCollections.observableArrayList();
        Statement stm = connection.createStatement();

        ResultSet resultSet = stm.executeQuery("select * from NILSTOCK");
            int i=1;
        while (resultSet.next()) {
            // this value pass to pojo constructur and that value get using observable list to table.
            list.add(new NillStock(
                    i,
                    resultSet.getString("ITEMNAME"),
                     resultSet.getString("CATEGORY"),
                    resultSet.getInt("STOCK"),
                    resultSet.getFloat("PURCHASINGPRICE"),
                    resultSet.getInt("QTY"),
                    resultSet.getString("UNIT"),
                    resultSet.getFloat("REQURIEDAMOUNT")
            ));

            i++;
        }

        table.setItems(list);

        resultSet.close();

        //   Load net total 
        Statement statement = connection.createStatement();
        ResultSet resultSet1 = statement.executeQuery("SELECT SUM(QTY) as q , SUM(PURCHASINGPRICE) as p,SUM(REQURIEDAMOUNT) as t FROM NILSTOCK");
        while (resultSet1.next()) {

            netTotal_Qty.setText(String.valueOf(resultSet1.getInt("q")));
            netTotal_Purchasing_Price.setText(String.valueOf(resultSet1.getFloat("p")));
            netTotal_Total.setText(String.valueOf(resultSet1.getFloat("t")));

            txtItems.clear();
            txtStock.clear();
            txtPurchingPrice.clear();
            txtQty.clear();
            txtTotal.clear();
            txtUnit.clear();
            //    units.

            //---------------------------------------------------------------------- global variable set 0 default
            itemsCalculationPurpose = null;

            unitCalculationPurpose = null;
            puchasingpriceCalculationPurpose = 0;
            requriedAmountCalculationPurpose = 0;
            stockcalculationPrupose = 0;
            qtyCalculationPurpose = 0;

        }

    }

    @FXML
    private void qtyCalculation(KeyEvent event) {

        try {

            float tempamount = Float.parseFloat(txtPurchingPrice.getText());
            qtyCalculationPurpose = Integer.parseInt(txtQty.getText());

            requriedAmountCalculationPurpose = (qtyCalculationPurpose * tempamount);

            txtTotal.setText(String.valueOf(requriedAmountCalculationPurpose));

        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    @FXML
    private void exportAction(ActionEvent event) throws SQLException, FileNotFoundException, IOException {
        
        
        Global_SqlQuery = "SELECT * FROM  NILSTOCK";


        PreparedStatement ps = connection.prepareStatement(Global_SqlQuery);

        ResultSet rs = ps.executeQuery();

        XSSFWorkbook wb = new XSSFWorkbook();

        XSSFSheet sheet = wb.createSheet("Nil_Stock");

        XSSFRow header = sheet.createRow(0);

        header.createCell(0).setCellValue("ITEMNAME");

        header.createCell(1).setCellValue("CATEGORY");

        header.createCell(2).setCellValue("STOCK");

        header.createCell(3).setCellValue("PURCHASING PRICE");

        header.createCell(4).setCellValue("QTY");

        header.createCell(5).setCellValue("UNIT");
        header.createCell(6).setCellValue("REQURIED AMOUNT");
    

        sheet.autoSizeColumn(0);

        sheet.autoSizeColumn(1);

        sheet.autoSizeColumn(2);

        sheet.autoSizeColumn(3);

        sheet.autoSizeColumn(4);
        sheet.autoSizeColumn(5);
        sheet.autoSizeColumn(6);
      

        sheet.setColumnWidth(1, 256 * 20);

        sheet.setZoom(150);

        int index = 1;

        while (rs.next()) {

            System.out.println("in while" + index);

            XSSFRow row = sheet.createRow(index);

            
     
            row.createCell(0).setCellValue(rs.getString("ITEMNAME"));

            row.createCell(1).setCellValue(rs.getString("CATEGORY"));

            row.createCell(2).setCellValue(rs.getString("STOCK"));

            row.createCell(3).setCellValue(rs.getString("PURCHASINGPRICE"));

            row.createCell(4).setCellValue(rs.getString("QTY"));
            row.createCell(5).setCellValue(rs.getString("UNIT"));
            row.createCell(6).setCellValue(rs.getString("REQURIEDAMOUNT"));
          

            index++;

        }

        XSSFRow row = sheet.createRow(index);
        row.createCell(0).setCellValue("---");
        row.createCell(1).setCellValue("---");
        row.createCell(2).setCellValue("---");
        row.createCell(3).setCellValue("Net Total Purchasing price");
        row.createCell(4).setCellValue("Net Total Qty");
        row.createCell(5).setCellValue("---");
        row.createCell(6).setCellValue("Net Total Amount");
    

        XSSFRow row1 = sheet.createRow(index + 1);
        row1.createCell(0).setCellValue("");
        row1.createCell(1).setCellValue("");
        row1.createCell(2).setCellValue("");//////////////////

        row1.createCell(3).setCellValue(netTotal_Purchasing_Price.getText());
        row1.createCell(4).setCellValue(netTotal_Qty.getText());//////////////////
        row1.createCell(5).setCellValue("");
        row1.createCell(6).setCellValue(netTotal_Total.getText());
       

        FileOutputStream fileout = new FileOutputStream("NilStock.XLSX");

        wb.write(fileout);

        fileout.close();

        rs.close();

        ps.close();

            Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Alert");
        alert.setHeaderText(null);
        alert.setContentText("Data Export Successfully !");
        alert.showAndWait();

    }
}


