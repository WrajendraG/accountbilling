package account_billing;

//DATABASE 
import com.osiersystems.pojos.GstReport;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.StringConverter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import requriedDatabaseConnection.DatabaseConnection;

public class DayToDaySaleController implements Initializable {

    @FXML
    private Button display;
    @FXML
    private TableView<GstReport> GSTTable;
    @FXML
    private TableColumn<GstReport, Integer> Column_SrNo;
    @FXML
    private TableColumn<GstReport, String> Column_InvoiceNumber;
    @FXML
    private TableColumn<GstReport, String> Column_BuyerName;
    @FXML
    private TableColumn<GstReport, String> Column_Date;
    @FXML
    private TableColumn<GstReport, Float> Column_TaxableValue;
    @FXML
    private TableColumn<GstReport, Float> Column_Cgst;
    @FXML
    private TableColumn<GstReport, Float> Column_Sgst;
    @FXML
    private TableColumn<GstReport, Float> Column_TotalTaxableAmount;

    @FXML
    private RadioButton Radio_BoxAllReports;
    @FXML
    private RadioButton Radio_BoxByDate;
    @FXML
    private DatePicker DatePicker_Date;
    @FXML
    private RadioButton Radio_BoxDaytoDay;
    @FXML
    private DatePicker DatePicker_StartDate;
    @FXML
    private DatePicker DatePicker_EndDate;
    @FXML
    private TextField NetTotal_TotalTaxableAmount;
    @FXML
    private TextField NetTotal_Cgst;
    @FXML
    private TextField NetTotal_Sgst;
    @FXML
    private TextField NetTotal_TaxableValue;
    @FXML
    private Button exportToExcel;

    Connection connection;
    String Global_SqlQuery = null;
    String Global_Table_InvoiceNumber;
    String Global_Table_BuyerName;
    String Global_Table_Date;
    String Global_Table_TaxableValue;
    String Global_Table_Cgst;
    String Globle_Table_Sgst;
    String Globle_Table_TotalTaxableAmount;
    Alert alert = new Alert(Alert.AlertType.WARNING);
    boolean flag = false;

    float Global_NetTotal_TaxableValue, Global_NetTotal__Cgst, Global_NetTotal_Sgst, Global_NetTotalTaxableAmount;

    ObservableList<GstReport> list;
    @FXML
    private ToggleGroup radioBoxGroup;
    @FXML
    private TextField TextField_CanceledInvoice;

    float Globle_NetTotal_Canceled_Amount;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        connection = DatabaseConnection.getConnection();
        Column_SrNo.setCellValueFactory(new PropertyValueFactory<>("Pojo_SrNo"));
        Column_SrNo.setStyle("-fx-alignment: CENTER");

        Column_InvoiceNumber.setCellValueFactory(new PropertyValueFactory<>("Pojo_InvoiceNumber"));
        Column_InvoiceNumber.setStyle("-fx-alignment: CENTER");

        Column_BuyerName.setCellValueFactory(new PropertyValueFactory<>("Pojo_BuyerName"));
        Column_BuyerName.setStyle("-fx-alignment: LEFT");

        Column_Date.setCellValueFactory(new PropertyValueFactory<>("Pojo_Date"));
        Column_Date.setStyle("-fx-alignment: CENTER");

        Column_TaxableValue.setCellValueFactory(new PropertyValueFactory<>("Pojo_TaxableValue"));
        Column_TaxableValue.setStyle("-fx-alignment: CENTER");

        Column_Cgst.setCellValueFactory(new PropertyValueFactory<>("Pojo_Cgst"));
        Column_Cgst.setStyle("-fx-alignment: CENTER");

        Column_Sgst.setCellValueFactory(new PropertyValueFactory<>("Pojo_Sgst"));
        Column_Sgst.setStyle("-fx-alignment: CENTER");

        Column_TotalTaxableAmount.setCellValueFactory(new PropertyValueFactory<>("Pojo_TotalTaxableAmount"));
        Column_TotalTaxableAmount.setStyle("-fx-alignment: CENTER");

        DatePicker_Date.setConverter(new StringConverter<LocalDate>() {

            String pattern = "dd-MMM-yyyy";

            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            {

                DatePicker_Date.setPromptText(pattern.toLowerCase());

            }

            @Override

            public String toString(LocalDate date) {

                if (date != null) {

                    return dateFormatter.format(date);

                } else {

                    return "";

                }

            }

            @Override

            public LocalDate fromString(String string) {

                if (string != null && !string.isEmpty()) {

                    return LocalDate.parse(string, dateFormatter);

                } else {

                    return null;

                }

            }

        });
        DatePicker_StartDate.setConverter(new StringConverter<LocalDate>() {

            String pattern = "dd-MMM-yyyy";

            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            {

                DatePicker_StartDate.setPromptText(pattern.toLowerCase());

            }

            @Override

            public String toString(LocalDate date) {

                if (date != null) {

                    return dateFormatter.format(date);

                } else {

                    return "";

                }

            }

            @Override

            public LocalDate fromString(String string) {

                if (string != null && !string.isEmpty()) {

                    return LocalDate.parse(string, dateFormatter);

                } else {

                    return null;

                }

            }

        });

        DatePicker_EndDate.setConverter(new StringConverter<LocalDate>() {

            String pattern = "dd-MMM-yyyy";

            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            {

                DatePicker_EndDate.setPromptText(pattern.toLowerCase());

            }

            @Override

            public String toString(LocalDate date) {

                if (date != null) {

                    return dateFormatter.format(date);

                } else {

                    return "";

                }

            }

            @Override

            public LocalDate fromString(String string) {

                if (string != null && !string.isEmpty()) {

                    return LocalDate.parse(string, dateFormatter);

                } else {

                    return null;

                }

            }

        });

        try {
            radio_BoxAllReportsAction();
        } catch (SQLException ex) {
            Logger.getLogger(DayToDaySaleController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void displayAction(ActionEvent event) throws SQLException {

        if (Radio_BoxByDate.isSelected()) {

            Global_SqlQuery = "select  * from INVOICE_DETAILS where INVOICE_DATE = '" + ((TextField) DatePicker_Date.getEditor()).getText() + "'";

            loadDataFromDatabaseToTable();

        }
        if (Radio_BoxDaytoDay.isSelected()) {

            Global_SqlQuery = "select  * from INVOICE_DETAILS where INVOICE_DATE between '" + ((TextField) DatePicker_StartDate.getEditor()).getText() + "' AND ' " + ((TextField) DatePicker_EndDate.getEditor()).getText() + "' ";
            loadDataFromDatabaseToTable();// or (status="pending" ) or status== paid 
        }

    }

    @FXML
    private void radio_BoxAllReportsAction() throws SQLException {

        if (Radio_BoxAllReports.isSelected()) {

            flag = false;  //  reinitialize again because flag updated (true) at the time of all bill load .

            DatePicker_Date.getEditor().clear();
            DatePicker_Date.setDisable(true);

            DatePicker_StartDate.getEditor().clear();
            DatePicker_StartDate.setDisable(true);

            DatePicker_EndDate.getEditor().clear();
            DatePicker_EndDate.setDisable(true);

            display.setDisable(true);

            Global_SqlQuery = "SELECT * FROM  INVOICE_DETAILS";
            loadDataFromDatabaseToTable();

        }
    }

    @FXML
    private void radio_BoxByDateAction(ActionEvent event) {

        DatePicker_Date.getEditor().clear();
        DatePicker_Date.setDisable(false);

        DatePicker_StartDate.getEditor().clear();
        DatePicker_StartDate.setDisable(true);

        DatePicker_EndDate.getEditor().clear();
        DatePicker_EndDate.setDisable(true);

        display.setDisable(false);
    }

    @FXML
    private void radio_BoxDaytoDayAction(ActionEvent event) {

        DatePicker_Date.getEditor().clear();
        DatePicker_Date.setDisable(true);

        DatePicker_StartDate.getEditor().clear();
        DatePicker_StartDate.setDisable(false);

        DatePicker_EndDate.getEditor().clear();
        DatePicker_EndDate.setDisable(false);

        display.setDisable(false);
    }

    public void loadDataFromDatabaseToTable() throws SQLException {

        Global_NetTotal_TaxableValue = 0;
        Global_NetTotal_Sgst = 0;
        Global_NetTotal__Cgst = 0;
        Global_NetTotalTaxableAmount = 0;

        Globle_NetTotal_Canceled_Amount = 0;
//ObservableList<GstReport>
        list = FXCollections.observableArrayList();
        list.clear();
        Statement stm = connection.createStatement();
        ResultSet resultSet = stm.executeQuery(Global_SqlQuery);
        int i = 1;
        while (resultSet.next()) {

            Global_Table_InvoiceNumber = resultSet.getString("INVOICE_NUMBER");
            Global_Table_BuyerName = resultSet.getString("CUSTOMER_NAME");
            Global_Table_Date = resultSet.getString("INVOICE_DATE");

            Global_Table_TaxableValue = resultSet.getString("NET_TOTAL_TAXABLE_VALUE");

            Global_Table_Cgst = resultSet.getString("NET_TOTAL_CGST_AMOUNT");

            Globle_Table_Sgst = resultSet.getString("NET_TOTAL_SGST_AMOUNT");

            Globle_Table_TotalTaxableAmount = resultSet.getString("NET_TOTAL_TOTAL");

            String status = resultSet.getString("STATUS");  //  here canceled status then can't add within net total . 

            if (status.equalsIgnoreCase("Canceled")) {

                Globle_NetTotal_Canceled_Amount = Globle_NetTotal_Canceled_Amount + Float.parseFloat(Globle_Table_TotalTaxableAmount);

            } else {
                
                Global_NetTotal_TaxableValue = (Global_NetTotal_TaxableValue + Float.parseFloat(Global_Table_TaxableValue)); //for nettotal calculation
                Global_NetTotal__Cgst = (Global_NetTotal__Cgst + Float.parseFloat(Global_Table_Cgst));
                Global_NetTotal_Sgst = (Global_NetTotal_Sgst + Float.parseFloat(Globle_Table_Sgst));
                Global_NetTotalTaxableAmount = (Global_NetTotalTaxableAmount + Float.parseFloat(Globle_Table_TotalTaxableAmount));

            }

            list.add(new GstReport(i, Global_Table_InvoiceNumber, Global_Table_BuyerName, Global_Table_Date, Float.parseFloat(Global_Table_TaxableValue), Float.parseFloat(Global_Table_Cgst), Float.parseFloat(Globle_Table_Sgst), Float.parseFloat(Globle_Table_TotalTaxableAmount)));
            i++;
            flag = true;

        }

        GSTTable.setItems(list);

        NetTotal_TaxableValue.setText(String.valueOf(Global_NetTotal_TaxableValue));
        NetTotal_Cgst.setText(String.valueOf(Global_NetTotal__Cgst));
        NetTotal_Sgst.setText(String.valueOf(Global_NetTotal_Sgst));
        NetTotal_TotalTaxableAmount.setText(String.valueOf(Global_NetTotalTaxableAmount));

        TextField_CanceledInvoice.setText(String.valueOf(Globle_NetTotal_Canceled_Amount));

    }

    @FXML
    private void exportToExcelAction(ActionEvent event) throws SQLException, FileNotFoundException, IOException {

        Global_SqlQuery = "SELECT * FROM  INVOICE_DETAILS";

        if (Radio_BoxByDate.isSelected()) {

            Global_SqlQuery = "select  * from INVOICE_DETAILS where INVOICE_DATE = '" + ((TextField) DatePicker_Date.getEditor()).getText() + "'";

        }
        if (Radio_BoxDaytoDay.isSelected()) {

            Global_SqlQuery = "select  * from INVOICE_DETAILS where INVOICE_DATE between '" + ((TextField) DatePicker_StartDate.getEditor()).getText() + "' AND ' " + ((TextField) DatePicker_EndDate.getEditor()).getText() + "' ";

        }

        PreparedStatement ps = connection.prepareStatement(Global_SqlQuery);

        ResultSet rs = ps.executeQuery();

        XSSFWorkbook wb = new XSSFWorkbook();

        XSSFSheet sheet = wb.createSheet("Gst_Report");

        XSSFRow header = sheet.createRow(0);

        header.createCell(0).setCellValue("INVOICE NUMBER");

        header.createCell(1).setCellValue("BUYER NAME");

        header.createCell(2).setCellValue("DATE OF INVOICE");

        header.createCell(3).setCellValue("TAXABLE VALUE");

        header.createCell(4).setCellValue("TOTAL CGST");
        header.createCell(5).setCellValue("TOTAL SGST");
        header.createCell(6).setCellValue("TOTAL TAXABLE AMOUNT");

        sheet.autoSizeColumn(0);

        sheet.autoSizeColumn(1);

        sheet.autoSizeColumn(2);

        sheet.autoSizeColumn(3);

        sheet.autoSizeColumn(4);
        sheet.autoSizeColumn(5);
        sheet.autoSizeColumn(6);

        sheet.setColumnWidth(1, 256 * 20);

        sheet.setZoom(150);

        int index = 1;

        while (rs.next()) {

            System.out.println("in while" + index);

            XSSFRow row = sheet.createRow(index);

            row.createCell(0).setCellValue(rs.getString("INVOICE_NUMBER"));
            row.createCell(1).setCellValue(rs.getString("CUSTOMER_NAME"));
            row.createCell(2).setCellValue(rs.getString("INVOICE_DATE"));
            row.createCell(3).setCellValue(rs.getString("NET_TOTAL_TAXABLE_VALUE"));
            row.createCell(4).setCellValue(rs.getString("NET_TOTAL_CGST_AMOUNT"));
            row.createCell(5).setCellValue(rs.getString("NET_TOTAL_SGST_AMOUNT"));
            row.createCell(6).setCellValue(rs.getString("NET_TOTAL_TOTAL"));

            index++;

        }

        XSSFRow row = sheet.createRow(index);
        row.createCell(0).setCellValue("---");
        row.createCell(1).setCellValue("---");
        row.createCell(2).setCellValue("---");
        row.createCell(3).setCellValue("NET TOTAL TAXABLE VALUE");
        row.createCell(4).setCellValue("NET TOTAL CGST AMOUNT");
        row.createCell(5).setCellValue("NET TOTAL SGST AMOUNT");
        row.createCell(6).setCellValue("NET TOTAL TOTAL");

        XSSFRow row1 = sheet.createRow(index + 1);
        row1.createCell(0).setCellValue("");
        row1.createCell(1).setCellValue("");
        row1.createCell(2).setCellValue("");//////////////////

        row1.createCell(3).setCellValue(NetTotal_TaxableValue.getText());
        row1.createCell(4).setCellValue(NetTotal_Cgst.getText());//////////////////
        row1.createCell(5).setCellValue(NetTotal_Sgst.getText());
        row1.createCell(6).setCellValue(NetTotal_TotalTaxableAmount.getText());

        FileOutputStream fileout = new FileOutputStream("GstReport.XLSX");

        wb.write(fileout);

        fileout.close();

        rs.close();

        ps.close();

        connection.close();

        alert.setTitle("Success");
        alert.setContentText("Your Export completed successfully");

        alert.showAndWait();

    }

}
