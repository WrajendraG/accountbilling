package account_billing;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.JOptionPane;
import requriedDatabaseConnection.DatabaseConnection;

/**
 * FXML Controller class
 *
 * @author ACER
 */
public class AddItemsController implements Initializable {

    @FXML
    private Button save;

    @FXML
    private Label unit1, unit2, unit3, unit4;
    JOptionPane jp = new JOptionPane();
    @FXML
    private AnchorPane anchor;

    int SalePrice;
    int MRP;
    float perDiscount;
    float Discount;
    float SelectGST;
    float TGST;
    float DivGST;
    float rate;

    @FXML
    private TextField txt_item_name;
    @FXML
    private ComboBox<String> combo_units;
    @FXML
    private TextField txt_stock;
    @FXML
    private TextField txt_sale_price;
    @FXML
    private TextField txt_purchase_price;
    @FXML
    private TextField txt_mrp;
    @FXML
    private TextField txt_min_sale;
    @FXML
    private TextField txt_self_value_price;
    @FXML
    private TextField txt_hsn_code;
    @FXML
    private TextField txt_cgst_per;
    @FXML
    private TextField txt_sgst_per;
    @FXML
    private TextField txt_gst_per;
    @FXML
    private TextField txt_cgst_rupess;
    @FXML
    private TextField txt_sgst_rupess;
    @FXML
    private TextField txt_gst_rupess;
    @FXML
    private TextField txt_discount_rupess;
    @FXML
    private TextField txt_discount_per;
    @FXML
    private ComboBox<String> combo_taxt_category;

    int CategoryCode = 100;
    int PerticularItemsCode = 1000;
    int finalBarcodeLogic = 0;

    //   int Qty = 0;
    Connection connection;

    Alert alert = new Alert(Alert.AlertType.WARNING);

    @FXML
    private RadioButton existingBarcode;
    @FXML
    private ToggleGroup checkbarcode;
    @FXML
    private RadioButton generateBarcode;
    @FXML
    private TextField scanedBarcode;

    String itemName = null;
    String CategoryName = null;
    @FXML
    private ComboBox<String> com_Category_Items;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        connection = DatabaseConnection.getConnection();

//        txt_item_name.setEditable(true);
//        combo_units.setEditable(true);
//        txt_stock.setEditable(true);
//        txt_sale_price.setEditable(true);
//        txt_purchase_price.setEditable(true);
//        txt_mrp.setEditable(true);
//        txt_min_sale.setEditable(true);
//        txt_self_value_price.setEditable(true);
//        txt_hsn_code.setEditable(true);
//        txt_cgst_per.setEditable(false);
//        txt_sgst_per.setEditable(false);
//        txt_gst_per.setEditable(true);
//        txt_cgst_rupess.setEditable(false);
//        txt_gst_rupess.setEditable(false);
//        txt_discount_rupess.setEditable(false);
//        txt_discount_per.setEditable(false);
//        combo_taxt_category.setEditable(false);
        // Add items to combo_units
        combo_units.getItems().removeAll(combo_units.getItems());
        combo_units.getItems().addAll("Dozens", "Gms", "kgs.", "Meter", "Pcs.", "Tonne");
        combo_units.getSelectionModel().select("Select");
        //Add items to combo_taxt_category
        combo_taxt_category.getItems().removeAll(combo_taxt_category.getItems());
        combo_taxt_category.getItems().addAll("0%", "5%", "12%", "18%", "28%");
        combo_taxt_category.getSelectionModel().select("Select");

        //----------------------------------------// Auto complete Items  list code ----------------------------------------------------
//        
//        try {
//
//            connection = DatabaseConnection.getConnection();
//
//            String query = "select CATEGORY_NAME from CATEGORY";
//            PreparedStatement preparedStatement = connection.prepareStatement(query);
//            ResultSet resultSet = preparedStatement.executeQuery();
//
//            updatedList = FXCollections.observableArrayList();
//            updatedList.clear();
//            while (resultSet.next()) {
//
//                updatedList.add(resultSet.getString("category_name"));
//            }
//
//            org.controlsfx.control.textfield.TextFields.bindAutoCompletion(txtfield_Category, updatedList);
//
//            preparedStatement.close();
//        } catch (SQLException ex) {
//            Logger.getLogger(AddItemsController.class.getName()).log(Level.SEVERE, null, ex);
//        }
        loadcatageryAgain();

        radioButtonActionOnExistingBarcode();

    }

//-------------------------------------------------------------------------------------------
    @FXML
    private void addItem(ActionEvent event) throws SQLException, ClassNotFoundException {

        if (existingBarcode.isSelected()) {

            if (scanedBarcode.getText().length() == 0 && txt_item_name.getText().length() == 0 && txt_stock.getText().length() == 0 && txt_sale_price.getText().length() == 0 && txt_purchase_price.getText().length() == 0 && txt_mrp.getText().length() == 0 && txt_min_sale.getText().length() == 0 && txt_self_value_price.getText().length() == 0 && txt_hsn_code.getText().length() == 0 && txt_cgst_per.getText().length() == 0 && txt_sgst_per.getText().length() == 0 && txt_gst_per.getText().length() == 0 && txt_cgst_rupess.getText().length() == 0 && txt_sgst_rupess.getText().length() == 0 && txt_gst_rupess.getText().length() == 0 && txt_discount_rupess.getText().length() == 0 && txt_discount_per.getText().length() == 0) {//&&cgst.getText().length()==0 &&sgstt.getText().length()==0 &&stockt.getText().length()==0){

                alert.setTitle("Validate");
                alert.setHeaderText(null);
                alert.setContentText("Fields should not be Empty");
                alert.showAndWait();

            } else {

                boolean ckeckItemPresentOrNot = checkItemsNameWithinForPerticularCategory();    // this is used for user dont add prouduct after add directly.
                if (ckeckItemPresentOrNot == false) {

                    PreparedStatement ps = connection.prepareStatement("insert into add_items_details values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

                    if ((txt_item_name.getText()).length() != 0) {
                        ps.setString(1, txt_item_name.getText());
                    } else {
                        ps.setString(1, "");
                    }

                    ps.setString(2, (String) combo_units.getSelectionModel().getSelectedItem());

                    if ((txt_stock.getText()).length() != 0) {

                        ps.setString(3, txt_stock.getText());
                    } else {
                        ps.setString(3, "");

                    }

                    if ((txt_sale_price.getText()).length() != 0) {

                        ps.setString(4, txt_sale_price.getText());

                    } else {
                        ps.setString(4, "");
                    }

                    if ((txt_purchase_price.getText()).length() != 0) {
                        ps.setString(5, txt_purchase_price.getText());
                    } else {
                        ps.setString(5, "");
                    }

                    if ((txt_mrp.getText()).length() != 0) {
                        ps.setString(6, txt_mrp.getText());

                    } else {
                        ps.setString(6, "");
                    }

                    if ((txt_min_sale.getText()).length() != 0) {
                        ps.setString(7, txt_min_sale.getText());
                    } else {
                        ps.setString(7, "");
                    }

                    if ((txt_self_value_price.getText()).length() != 0) {
                        ps.setString(8, txt_self_value_price.getText());
                    } else {
                        ps.setString(8, "");
                    }

                    if ((txt_hsn_code.getText()).length() != 0) {
                        ps.setString(9, txt_hsn_code.getText());
                    } else {
                        ps.setString(9, "");
                    }
                    ps.setString(10, (String) combo_taxt_category.getSelectionModel().getSelectedItem());

                    //ps.setString(10, (String) combo_taxt_category.getSelectionModel().getSelectedItem());
                    if ((txt_cgst_per.getText()).length() != 0) {
                        ps.setString(11, txt_cgst_per.getText());
                    } else {
                        ps.setString(11, "");
                    }

                    if ((txt_cgst_rupess.getText()).length() != 0) {
                        ps.setString(12, txt_cgst_rupess.getText());
                    } else {
                        ps.setString(12, "");
                    }

                    if ((txt_sgst_per.getText()).length() != 0) {
                        ps.setString(13, txt_sgst_per.getText());
                    } else {
                        ps.setString(13, "");

                    }

                    if ((txt_sgst_rupess.getText()).length() != 0) {
                        ps.setString(14, txt_sgst_rupess.getText());
                    } else {
                        ps.setString(14, "");
                    }

                    if ((txt_gst_per.getText()).length() != 0) {
                        ps.setString(15, txt_gst_per.getText());
                    } else {
                        ps.setString(15, "");
                    }

                    if ((txt_gst_rupess.getText()).length() != 0) {
                        ps.setString(16, txt_gst_rupess.getText());
                    } else {
                        ps.setString(16, "");
                    }

                    if ((txt_discount_per.getText()).length() != 0) {
                        ps.setString(17, String.valueOf(perDiscount));
                    } else {
                        ps.setString(17, "");
                    }

                    if ((txt_discount_rupess.getText()).length() != 0) {
                        ps.setString(18, txt_discount_rupess.getText());
                    } else {
                        ps.setString(18, "");
                    }

                    ps.setFloat(19, rate);
                    ps.setString(20, scanedBarcode.getText());

                    //GENERATED
                    ps.setString(21, "GENERATED");

                    CategoryName = (String) com_Category_Items.getSelectionModel().getSelectedItem();
                    ps.setString(22, CategoryName);
                    
                    int result = ps.executeUpdate();

                    resetGlobalvriable();

                    if (result > 0) {

                        alert.setTitle(" Message");
                        alert.setHeaderText(null);
                        alert.setContentText("Existing barcode Items have Added  Successfully ! ");
                        alert.showAndWait();

                    }
                } else {
                    // we can use update query here .

                    alert.setTitle("Message");
                    alert.setHeaderText(null);
                    alert.setContentText("Items Name Present in Stock . You can Add new Item Name First else  you can update same product item stock");
                    alert.showAndWait();
                }

            }

        } else {

            // Qty = Integer.parseInt(txt_stock.getText());
            if (txt_item_name.getText().length() == 0 && txt_stock.getText().length() == 0 && txt_sale_price.getText().length() == 0 && txt_purchase_price.getText().length() == 0 && txt_mrp.getText().length() == 0 && txt_min_sale.getText().length() == 0 && txt_self_value_price.getText().length() == 0 && txt_hsn_code.getText().length() == 0 && txt_cgst_per.getText().length() == 0 && txt_sgst_per.getText().length() == 0 && txt_gst_per.getText().length() == 0 && txt_cgst_rupess.getText().length() == 0 && txt_sgst_rupess.getText().length() == 0 && txt_gst_rupess.getText().length() == 0 && txt_discount_rupess.getText().length() == 0 && txt_discount_per.getText().length() == 0) {//&&cgst.getText().length()==0 &&sgstt.getText().length()==0 &&stockt.getText().length()==0){

                alert.setTitle("Validate");
                alert.setHeaderText(null);
                alert.setContentText("Fields should not be null");
                alert.showAndWait();

            } else {

                getCategoryForPerticularItems();      // initilize category name 

                checkCategoryItemsCode();   // Initilize  items code

                String temp = "" + CategoryCode + "" + "" + PerticularItemsCode + "";    // initilize this vriable using above three methods for making barcode logic
                finalBarcodeLogic = Integer.parseInt(temp);

                //   generatet number of barcode
                boolean ckeckItemPresentOrNot = checkItemsNameWithinForPerticularCategory();    // this is used for user dont add prouduct after add directly.

                if (ckeckItemPresentOrNot == false) {

                    PreparedStatement ps = connection.prepareStatement("insert into add_items_details values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

                    if ((txt_item_name.getText()).length() != 0) {
                        ps.setString(1, txt_item_name.getText());
                    } else {
                        ps.setString(1, "");
                    }

                    ps.setString(2, (String) combo_units.getSelectionModel().getSelectedItem());

                    if ((txt_stock.getText()).length() != 0) {

                        ps.setString(3, txt_stock.getText());    // here stok is 1 because multiple barcode is generated .
                    } else {
                        ps.setString(3, "");

                    }

                    if ((txt_sale_price.getText()).length() != 0) {

                        ps.setString(4, txt_sale_price.getText());

                    } else {
                        ps.setString(4, "");
                    }

                    if ((txt_purchase_price.getText()).length() != 0) {
                        ps.setString(5, txt_purchase_price.getText());
                    } else {
                        ps.setString(5, "");
                    }

                    if ((txt_mrp.getText()).length() != 0) {
                        ps.setString(6, txt_mrp.getText());

                    } else {
                        ps.setString(6, "");
                    }

                    if ((txt_min_sale.getText()).length() != 0) {
                        ps.setString(7, txt_min_sale.getText());
                    } else {
                        ps.setString(7, "");
                    }

                    if ((txt_self_value_price.getText()).length() != 0) {
                        ps.setString(8, txt_self_value_price.getText());
                    } else {
                        ps.setString(8, "");
                    }

                    if ((txt_hsn_code.getText()).length() != 0) {
                        ps.setString(9, txt_hsn_code.getText());
                    } else {
                        ps.setString(9, "");
                    }
                    ps.setString(10, (String) combo_taxt_category.getSelectionModel().getSelectedItem());

                    //ps.setString(10, (String) combo_taxt_category.getSelectionModel().getSelectedItem());
                    if ((txt_cgst_per.getText()).length() != 0) {
                        ps.setString(11, txt_cgst_per.getText());
                    } else {
                        ps.setString(11, "");
                    }

                    if ((txt_cgst_rupess.getText()).length() != 0) {
                        ps.setString(12, txt_cgst_rupess.getText());
                    } else {
                        ps.setString(12, "");
                    }

                    if ((txt_sgst_per.getText()).length() != 0) {
                        ps.setString(13, txt_sgst_per.getText());
                    } else {
                        ps.setString(13, "");

                    }

                    if ((txt_sgst_rupess.getText()).length() != 0) {
                        ps.setString(14, txt_sgst_rupess.getText());
                    } else {
                        ps.setString(14, "");
                    }

                    if ((txt_gst_per.getText()).length() != 0) {
                        ps.setString(15, txt_gst_per.getText());
                    } else {
                        ps.setString(15, "");
                    }

                    if ((txt_gst_rupess.getText()).length() != 0) {
                        ps.setString(16, txt_gst_rupess.getText());
                    } else {
                        ps.setString(16, "");
                    }

                    if ((txt_discount_per.getText()).length() != 0) {
                        ps.setString(17, String.valueOf(perDiscount));
                    } else {
                        ps.setString(17, "");
                    }

                    if ((txt_discount_rupess.getText()).length() != 0) {
                        ps.setString(18, txt_discount_rupess.getText());
                    } else {
                        ps.setString(18, "");
                    }

                    ps.setFloat(19, rate);

                    ps.setString(20, String.valueOf(finalBarcodeLogic));
                    ps.setString(21, "NON-GENERATED");

                    CategoryName = (String) com_Category_Items.getSelectionModel().getSelectedItem();
                    ps.setString(22, CategoryName);

                    int result = 0;
                    result = ps.executeUpdate();

                    resetGlobalvriable();

                    if (result > 0) {

                        alert.setTitle(" Info");
                        alert.setHeaderText(null);
                        alert.setContentText(" items has added  with barcode Successfully within database you can print barcode future");
                        alert.showAndWait();

                    } else {
                        alert.setTitle(" Info");
                        alert.setHeaderText(null);
                        alert.setContentText("Item not Store in Database Try Again");
                        alert.showAndWait();
                    }

                } else {

                    // may i update you are product
                    alert.setTitle("Message");
                    alert.setHeaderText(null);
                    alert.setContentText(" Items Name Present in Stock . You can Add new Item Name First else  you can update same product item stock");
                    alert.showAndWait();
                }

            }

        }

    }

    @FXML
    private void handleUnitComboBoxAction(ActionEvent event) {

        combo_units.getSelectionModel().getSelectedItem();

        unit1.setText("(" + combo_units.getSelectionModel().getSelectedItem() + ")");
        unit2.setText("(" + combo_units.getSelectionModel().getSelectedItem() + ")");
        unit3.setText("(" + combo_units.getSelectionModel().getSelectedItem() + ")");
        unit4.setText("(" + combo_units.getSelectionModel().getSelectedItem() + ")");

    }

    @FXML
    private void handleTaxComboBoxAction(ActionEvent event) {

        if (combo_taxt_category.getSelectionModel().getSelectedIndex() == 0) {
            SelectGST = 0;
            txt_cgst_per.setText("0");
            txt_sgst_per.setText("0");
            txt_gst_per.setText("0");

        }

        if (combo_taxt_category.getSelectionModel().getSelectedIndex() == 1) {
            SelectGST = 5;
            txt_cgst_per.setText("2.5");
            txt_sgst_per.setText("2.5");
            txt_gst_per.setText("5");

        }
        if (combo_taxt_category.getSelectionModel().getSelectedIndex() == 2) {
            SelectGST = 12;
            txt_cgst_per.setText("6");
            txt_sgst_per.setText("6");
            txt_gst_per.setText("12");
        }
        if (combo_taxt_category.getSelectionModel().getSelectedIndex() == 3) {
            SelectGST = 18;
            txt_cgst_per.setText("9");
            txt_sgst_per.setText("9");
            txt_gst_per.setText("18");
        }
        if (combo_taxt_category.getSelectionModel().getSelectedIndex() == 4) {
            SelectGST = 28;
            txt_cgst_per.setText("14");
            txt_sgst_per.setText("14");
            txt_gst_per.setText("28");

        }
//--------------------------------------------------------------------------------------
        SalePrice = Integer.parseInt(txt_sale_price.getText());
        MRP = Integer.parseInt(txt_mrp.getText());

        Discount = MRP - SalePrice;

        txt_discount_rupess.setText(String.valueOf(Discount));

        perDiscount = (Discount / MRP) * 100;
        
        txt_discount_per.setText(String.valueOf(perDiscount + "%"));
//-------------------------------------------------------------------------------------------
        TGST = ((SalePrice / (100 + SelectGST)) * SelectGST);

        rate = SalePrice - TGST;    // rate / cost = sale price - gst   // basic value

        String temp = String.format("%.2f", TGST);
        txt_gst_rupess.setText(String.valueOf(temp));

        DivGST = TGST / 2;
        String temp1 = String.format("%.2f", DivGST);
        txt_cgst_rupess.setText(String.valueOf(temp1));
        txt_sgst_rupess.setText(String.valueOf(temp1));

//-----------------------------------------------------------------------------------------------
    }

    @FXML
    private void ExitAction(ActionEvent event) throws IOException {
        resetGlobalvriable();
    }

    @FXML
    private void deleteCategory(ActionEvent event) throws SQLException {

        // String category = txtfield_Category.getText();
        CategoryName = (String) com_Category_Items.getSelectionModel().getSelectedItem();

        alert.setTitle("Validate owner name");
        alert.setHeaderText(null);
        alert.setContentText(" Selected Category id " + CategoryName + "");
        alert.showAndWait();

        if (CategoryName.isEmpty()) {

            alert.setTitle("validation");
            alert.setHeaderText(null);
            alert.setContentText(" select first catgeory then you can delete");
            alert.showAndWait();

        } else {

            Connection con = DatabaseConnection.getConnection();

            con.setAutoCommit(false);

            try {

                Statement statement = con.createStatement();

                boolean flag1 = statement.execute("Delete from  category where CATEGORY_NAME ='" + CategoryName + "' ");

                Statement statement2 = con.createStatement();
                boolean flag2 = statement2.execute("DROP TABLE " + CategoryName + " ");

                con.commit();
                loadcatageryAgain();
                if (flag2 == false && flag1 == false) {

                    alert.setTitle("validation");
                    alert.setHeaderText(null);
                    alert.setContentText(" " + CategoryName + " category Sucessfully Deleted ! and regarding Items also delete");
                    alert.showAndWait();
                    com_Category_Items.getSelectionModel().select("Select");
                    loadcatageryAgain();

                } else {
                    alert.setTitle("validation");
                    alert.setHeaderText(null);
                    alert.setContentText("" + CategoryName + " category is not Deleted ! you can those category is deleted those are available autocomplete ");
                    alert.showAndWait();

                    com_Category_Items.getSelectionModel().select("Select");
                    con.rollback();

                }
            } catch (Exception e) {
                alert.setTitle("validation");
                alert.setHeaderText(null);
                alert.setContentText(" category is not delete = " + CategoryName + "I am In Exception ");
                alert.showAndWait();

                con.rollback();

            }

            //  here
        }
    }

    @FXML
    private void addCategory(ActionEvent event) throws SQLException {

        displayCategory();    // Categoryname come from display category via Global Vriable

        String category = CategoryName;    // this come from popup

        if (category.isEmpty()) {

            alert.setTitle("validation");
            alert.setHeaderText(null);
            alert.setContentText(" Enter first Item catgeory then you can add new catgory");
            alert.showAndWait();

        } else {

            boolean check = checkCategory();

            if (check == true) {

                alert.setTitle("Validation");
                alert.setHeaderText(null);
                alert.setContentText(" Category is already Exist use that else select new category");
                alert.showAndWait();
            }
            if (check == false) {

                getCategoryCode();

                Connection con = DatabaseConnection.getConnection();
                con.setAutoCommit(false);

                try {

                    Statement statement = con.createStatement();

                    boolean flag1 = statement.execute("CREATE TABLE " + category + "( Items_Name varchar2(50) NOT NULL , code number NOT NULL)");

                    String sql = "INSERT INTO CATEGORY (CATEGORY_NAME, CODE) VALUES (?, ?)";

                    PreparedStatement preparedStatement = con.prepareStatement(sql);

                    preparedStatement.setString(1, category);
                    preparedStatement.setInt(2, CategoryCode);

                    int rowEffected = preparedStatement.executeUpdate();
                    con.commit();
                    loadcatageryAgain();
                    if (rowEffected > 0) {

                        alert.setTitle("Validation");
                        alert.setHeaderText(null);
                        alert.setContentText("Catageroy is Added Sucessfully ");
                        int q = CategoryCode;
                        alert.showAndWait();
                        //loadcatageryAgain();

                    } else {

                        alert.setTitle("Validation");
                        alert.setHeaderText(null);
                        alert.setContentText("Catageroy is not Added Try Again");
                        alert.showAndWait();

                        con.rollback();

                    }
                    if (flag1 == false) {

                    } else {
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Validate owner name");
                        alert.setHeaderText(null);
                        alert.setContentText("Category Specific Table Not Created");
                        alert.showAndWait();
                        con.rollback();
                    }
                } catch (Exception e) {
                    alert.setTitle("Validation");
                    alert.setHeaderText(null);
                    alert.setContentText(" I AM IN EXCEPTION ");
                    alert.showAndWait();

                    con.rollback();
                }
            }

        }

        // add in cataagery table txtfield_category data 
    }

//    @FXML
//    private void searchCategoryonPressed(KeyEvent event) throws SQLException {
//        // when i was click in category drop down list is show      // if catogry does not avalable in database show message first to add catogary first then add items.
//
//                 getCategoryCode();
//    }
    public boolean checkCategory() {

        //    String check = (String) com_Category_Items.getSelectionModel().getSelectedItem();
        String check = CategoryName;
        boolean b = false;

        if (check.isEmpty()) {

            alert.setTitle("Validate owner name");
            alert.setHeaderText(null);
            alert.setContentText(" Try Again");
            alert.showAndWait();

        } else {
//select code from category where CODE=(select max(code)from category);
            try {

                PreparedStatement preparedStatement = connection.prepareStatement("select CATEGORY_NAME from category where CATEGORY_NAME = '" + check + "' ");
                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    b = true;
                    //        Item_category = resultSet.getString("CATEGORY_NAME");

                }

                preparedStatement.close();

            } catch (java.sql.SQLIntegrityConstraintViolationException e) {

                alert.setTitle("Validation");
                alert.setHeaderText(null);
                alert.setContentText(" Category is already Present in database");
                alert.showAndWait();

            } catch (SQLException sql) {
                sql.printStackTrace();
                System.out.println("DB connectivity issue");

            }
        }
        return b;
    }

    public void loadcatageryAgain() {
        //----------------------------------------// Auto complete Items  list code ----------------------------------------------------

        ObservableList updatedList = FXCollections.observableArrayList();
        updatedList.clear();

        try {

            connection = DatabaseConnection.getConnection();

            String query = "select CATEGORY_NAME from CATEGORY";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                updatedList.add(resultSet.getString("category_name"));
            }

            com_Category_Items.getItems().removeAll(com_Category_Items.getItems());
            com_Category_Items.getItems().addAll(updatedList);
            com_Category_Items.getSelectionModel().select("Select");

            preparedStatement.close();

        } catch (SQLException ex) {
            Logger.getLogger(AddItemsController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean getCategoryItemsCode() throws SQLException {

        String tableName = (String) com_Category_Items.getSelectionModel().getSelectedItem();

        boolean check = false;
        int firstTimeCodeIncrementer = 0;
        PreparedStatement ps = connection.prepareStatement("select code from " + tableName + " where code = (select max(code) from " + tableName + ")");
        ResultSet resultSet1 = ps.executeQuery();

        while (resultSet1.next()) {

            PerticularItemsCode = resultSet1.getInt("code");

            firstTimeCodeIncrementer = 1;
            check = true;
        }
        if (firstTimeCodeIncrementer == 1) {
            PerticularItemsCode = (PerticularItemsCode + 1);
        }
        if (firstTimeCodeIncrementer == 0) {
            PerticularItemsCode = 1000;
        }

        ps.close();
        return check;
    }

    public void resetGlobalvriable() {

        // if you are google vriable then dont res
        CategoryCode = 100;
        PerticularItemsCode = 1000;
        finalBarcodeLogic = 0;

        txt_item_name.setText(null);
        combo_units.getSelectionModel().select("select");
        txt_stock.setText(null);
        txt_sale_price.setText(null);
        txt_purchase_price.setText(null);
        txt_mrp.setText(null);

        txt_min_sale.setText(null);
        txt_self_value_price.setText(null);
        txt_hsn_code.setText(null);
        combo_taxt_category.getSelectionModel().select("0%");
        txt_cgst_per.setText(null);
        txt_sgst_per.setText(null);
        txt_gst_per.setText(null);
        txt_cgst_rupess.setText(null);
        txt_sgst_rupess.setText(null);
        txt_gst_rupess.setText(null);
        txt_discount_rupess.setText(null);
        txt_discount_per.setText(null);

        //  txtfield_Category.setText(null);
        com_Category_Items.getSelectionModel().select("Select");
        scanedBarcode.setText(null);

        existingBarcode.setSelected(true);
        scanedBarcode.setVisible(true);
        scanedBarcode.setEditable(true);
        itemName = null;
        CategoryName = null;

    }

    public void getCategoryCode() throws SQLException {

        int checkForCodeIncrement = 0;
        PreparedStatement ps = connection.prepareStatement("select code from category where CODE=(select max(code)from category)");  // here is category table is fixed
        ResultSet resultSet1 = ps.executeQuery();

        while (resultSet1.next()) {
            CategoryCode = resultSet1.getInt("code");

            checkForCodeIncrement = 1;
        }
        if (checkForCodeIncrement == 1) {
            CategoryCode = (CategoryCode + 1);
        }
        if (checkForCodeIncrement == 0) {
            CategoryCode = 100;
        }

        ps.close();

    }

    public boolean getCategoryForPerticularItems() throws SQLException {

        boolean check = false;
        String categoryName = (String) com_Category_Items.getSelectionModel().getSelectedItem();

        PreparedStatement ps = connection.prepareStatement("select code from category where category_name ='" + categoryName + "'");
        ResultSet resultSet1 = ps.executeQuery();

        while (resultSet1.next()) {
            CategoryCode = resultSet1.getInt("code");
            check = true;
        }
        ps.close();

        return check;
    }

    public boolean checkItemsNameWithinForPerticularCategory() throws SQLException {

        boolean check = false;
        //  String categoryName = (String) com_Category_Items.getSelectionModel().getSelectedItem();

        PreparedStatement ps = connection.prepareStatement("select ITEMS_NAME from ADD_Items_Details where BAR_CODE = '" + finalBarcodeLogic + "'");
        ResultSet resultSet1 = ps.executeQuery();

        while (resultSet1.next()) {
            //    itemName = resultSet1.getString("Items_name");
            check = true;
        }
        ps.close();

        return check;
    }

    @FXML
    private void radioButtonAction(ActionEvent event) {

        if (generateBarcode.isSelected()) {

            scanedBarcode.requestFocus();
            scanedBarcode.setEditable(false);
            scanedBarcode.setVisible(false);

            ///     txt_stock.setEditable(true);
        }

    }

    @FXML
    private void radioButtonActionOnExistingBarcode() {
        scanedBarcode.setVisible(true);
        scanedBarcode.setEditable(true);
        scanedBarcode.requestFocus();

        //    txt_stock.setText("1");
        //  txt_stock.setEditable(false);
    }

    public void displayItems() {
        Stage popupwindow = new Stage();
        popupwindow.initModality(Modality.APPLICATION_MODAL);
        popupwindow.initStyle(StageStyle.UNIFIED);
        popupwindow.resizableProperty().setValue(Boolean.FALSE);
        AnchorPane anchorPane = new AnchorPane();

        Label label = new Label("Enter Items Name As Per Category !");
        label.setLayoutX(6.0);
        label.setLayoutY(32.0);

        TextField textField = new TextField();
        textField.setLayoutX(68.0);
        textField.setLayoutY(78.0);
        textField.prefHeight(25.0);
        textField.prefWidth(172.0);
        Button button1 = new Button("Save");
        button1.setLayoutX(68.0);
        button1.setLayoutY(142.0);
        button1.setMnemonicParsing(false);

        Button button2 = new Button("Cancel");
        button2.setLayoutX(188.0);
        button2.setLayoutY(142.0);
        button2.setMnemonicParsing(false);

        anchorPane.getChildren().addAll(label, textField, button1, button2);

        anchorPane.prefHeight(234.0);
        anchorPane.prefWidth(340.0);
        button1.setOnAction(e -> popupwindow.close());
        button2.setOnAction(e -> popupwindow.close());

        Scene scene1 = new Scene(anchorPane);

        popupwindow.setScene(scene1);
        popupwindow.showAndWait();
        itemName = textField.getText();

    }

    public void displayCategory() {

        Stage popupwindow = new Stage();
        popupwindow.initModality(Modality.APPLICATION_MODAL);
        popupwindow.initStyle(StageStyle.UNIFIED);
        popupwindow.resizableProperty().setValue(Boolean.FALSE);
        AnchorPane anchorPane = new AnchorPane();

        Label label = new Label("Category Does Not Allowed Space and Spical Charater !");
        label.setLayoutX(6.0);
        label.setLayoutY(32.0);

        TextField textField = new TextField();
        textField.setLayoutX(68.0);
        textField.setLayoutY(78.0);
        textField.prefHeight(25.0);
        textField.prefWidth(172.0);
        Button button1 = new Button("Save");
        button1.setLayoutX(68.0);
        button1.setLayoutY(142.0);
        button1.setMnemonicParsing(false);

        Button button2 = new Button("Cancel");
        button2.setLayoutX(188.0);
        button2.setLayoutY(142.0);
        button2.setMnemonicParsing(false);

        anchorPane.getChildren().addAll(label, textField, button1, button2);

        anchorPane.prefHeight(234.0);
        anchorPane.prefWidth(340.0);
        button1.setOnAction(e -> popupwindow.close());
        button2.setOnAction(e -> popupwindow.close());

        Scene scene1 = new Scene(anchorPane);

        popupwindow.setScene(scene1);
        popupwindow.showAndWait();

        CategoryName = textField.getText();

    }

    @FXML
    private void addCategoryItems(ActionEvent event) throws SQLException {

        displayItems();

        String category = (String) com_Category_Items.getSelectionModel().getSelectedItem();

        if ((category.equals("Select"))) {

            alert.setTitle("validation");
            alert.setHeaderText(null);
            alert.setContentText(" Select first catagery which items add in which category");
            alert.showAndWait();
            txt_item_name.clear();

        } else {

            if (itemName.isEmpty()) {    // itemname come from global from display items();

                alert.setTitle("Info");
                alert.setHeaderText(null);
                alert.setContentText("Enter Items Name using Add Symbol");
                alert.showAndWait();

            } else {

                getCategoryItemsCode(); //  here Check Item Name 

                boolean check = checkItemsNameWithinForPerticularCategory();  // just chacking category is present or not;
                if (check == true) {

                    alert.setTitle("Validation");
                    alert.setHeaderText(null);
                    alert.setContentText(" Items is already Present ! We can use again type item in Item Name Text Box");
                    alert.showAndWait();
                }
                if (check == false) {

                    String sql = "INSERT INTO " + category + " (ITEMS_NAME, CODE) VALUES (?,?)";  // insert into Item in perticular table 

                    PreparedStatement ps3 = connection.prepareStatement(sql);

                    ps3.setString(1, itemName);
                    ps3.setInt(2, PerticularItemsCode);

                    int rowEffected = ps3.executeUpdate();

                    if (rowEffected > 0) {

                        alert.setTitle("Validation");
                        alert.setHeaderText(null);
                        alert.setContentText(" " + itemName + " is added Sucessfully ! ");   // Insert item Successfully in Perticular catagery.
                        alert.showAndWait();
                        bindItems1();

                    } else {

                        alert.setTitle("Validation");
                        alert.setHeaderText(null);
                        alert.setContentText("Item Name is not Added Try Again");
                        alert.showAndWait();

                    }
                    ps3.close();
                }

            }

        }

    }

    @FXML
    private void deleteCategoryItems(ActionEvent event) {

        String Cat = (String) com_Category_Items.getSelectionModel().getSelectedItem();

        String item = txt_item_name.getText();

        if (Cat.isEmpty()) {

            alert.setTitle("Validate owner name");
            alert.setHeaderText(null);
            alert.setContentText("If We want Delete Item then Select Category First");
            alert.showAndWait();

        } else {

            if (item.isEmpty()) {

                alert.setTitle("validation");
                alert.setHeaderText(null);
                alert.setContentText(" select first Item Name then you can delete");
                alert.showAndWait();

            } else {

                try {

                    Statement statement = connection.createStatement();

                    boolean flag1 = statement.execute("Delete from  " + Cat + " where Items_name ='" + item + "' ");

                    if (flag1 == true) {

                        alert.setTitle("Validate owner name");
                        alert.setHeaderText(null);
                        alert.setContentText("Delete Sucessfully Items");
                        alert.showAndWait();
                        bindItems1();
                    }

                } catch (SQLException e) {

                    alert.setTitle("Validate owner name");
                    alert.setHeaderText(null);
                    alert.setContentText(" Some problem in delete items");
                    alert.showAndWait();
                }
            }

        }

    }

    @FXML
    private void bindItems(ActionEvent event) {

        bindItems1();
    }

    public void bindItems1() {

        String category = (String) com_Category_Items.getSelectionModel().getSelectedItem();

        ObservableList categoryList = FXCollections.observableArrayList();

        categoryList.removeAll(categoryList);

        try {

            connection = DatabaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("select Items_Name from "+category+" ");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                categoryList.add(resultSet.getString("ITEMS_NAME"));
            }

            org.controlsfx.control.textfield.TextFields.bindAutoCompletion(txt_item_name, categoryList);

            preparedStatement.close();

        } catch (SQLException ex) {
            Logger.getLogger(AddItemsController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean checkCategoryItemsCode() throws SQLException {

        String tableName = (String) com_Category_Items.getSelectionModel().getSelectedItem();

        boolean check = false;
        int firstTimeCodeIncrementer = 0;
        PreparedStatement ps = connection.prepareStatement("select code from " + tableName + " where Items_Name = '" + txt_item_name.getText() + "'");
        ResultSet resultSet1 = ps.executeQuery();

        while (resultSet1.next()) {

            PerticularItemsCode = resultSet1.getInt("code");

            check = true;
        }
        ps.close();
        return check;
    }

}
