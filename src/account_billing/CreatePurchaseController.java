package account_billing;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import requriedDatabaseConnection.DatabaseConnection;

public class CreatePurchaseController implements Initializable {

    @FXML
    private TextField Purchase_Name;
    @FXML
    private DatePicker Purchase_Date;
    @FXML
    private TextField Purchase_Paid;
    @FXML
    private TextField Purchase_Invoice_No;
    @FXML
    private TextField Purchase_Amount;
    @FXML
    private TextField Purchase_Outstanding;
    @FXML
    private ComboBox<String> Purchase_Mode;
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    Statement stm = null;
    float Global_Total, Globle_paidAmount, GlobleOutStandingAmount;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // TODO
        String state[] = {"Cash", "Online"};

        Purchase_Mode.getItems().removeAll(Purchase_Mode.getItems());
        Purchase_Mode.getItems().addAll(state);
        Purchase_Mode.getSelectionModel().select("Cash");

        //--------------------------------------------------------------------------------------------------------------------     
        try {
            bindItems1();

        } catch (SQLException ex) {
            Logger.getLogger(ViewCreatedAccountController.class.getName()).log(Level.SEVERE, null, ex);
        }

        Purchase_Date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "yyyy-MM-dd";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            {
                Purchase_Date.setPromptText(pattern.toLowerCase());
            }

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

    }

    public void bindItems1() throws SQLException {

        try {
            Purchase_Name.setText("");
            ObservableList categoryList1 = FXCollections.observableArrayList();

            categoryList1.removeAll(categoryList1);

            connection = DatabaseConnection.getConnection();
            preparedStatement = connection.prepareStatement(" select BUSINESS_NAME from SUPPLIERACCOUNT");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                categoryList1.add(resultSet.getString("BUSINESS_NAME"));

            }
            ArrayList observableList1 = new ArrayList<String>(new HashSet<String>(categoryList1));

            org.controlsfx.control.textfield.TextFields.bindAutoCompletion(Purchase_Name, observableList1);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    @FXML
    private void Purchase_Save(ActionEvent event) throws ParseException, SQLException {

        if (((TextField) Purchase_Date.getEditor()).getText().isEmpty() || Purchase_Name.getText().isEmpty() || Purchase_Paid.getText().isEmpty() || Purchase_Invoice_No.getText().isEmpty() || Purchase_Amount.getText().isEmpty() || Purchase_Outstanding.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Validation");
            alert.setHeaderText(null);
            alert.setContentText("All text field must be important");
            alert.showAndWait();

        } else {
            try {
            connection = DatabaseConnection.getConnection();
            connection.setAutoCommit(false);
            
            

                stm = connection.createStatement();

                String checkinvoiceno = "select INVOICE_NO from PURCHASE_DETAILS where INVOICE_NO ='" + Purchase_Invoice_No.getText() + "'";
                resultSet = stm.executeQuery(checkinvoiceno);
                if (!resultSet.next()) {
            
                  System.out.print("'"+((TextField) Purchase_Date.getEditor()).getText()+ "'");
                    String query = "INSERT INTO PURCHASE_DETAILS(INVOICE_NO,NAME,PURCHASE_DATE,AMOUNT,PAID,OUTSTANDING,PURCHASE_MODE) VALUES ('" + Purchase_Invoice_No.getText() + "','" + Purchase_Name.getText() + "', DATE '"+((TextField) Purchase_Date.getEditor()).getText()+ "'," + Purchase_Amount.getText() + "," + Purchase_Paid.getText() + "," +Purchase_Outstanding.getText()+ ",'" +(String) Purchase_Mode.getSelectionModel().getSelectedItem() + "')";

                    int a = stm.executeUpdate(query);
                    System.out.println("flag A"+a);

                    String query1 = "insert into purchase_history values('" + Purchase_Invoice_No.getText() + "', DATE '"+((TextField) Purchase_Date.getEditor()).getText()+ "' ," + Purchase_Amount.getText() + "," + Purchase_Paid.getText() + "," + Purchase_Outstanding.getText() + ",'" + (String) Purchase_Mode.getSelectionModel().getSelectedItem() + "')";

                    int b = stm.executeUpdate(query1);
                         System.out.println("flag  B"+b);
                    if (a > 0 && b > 0) {
                        connection.commit();
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("INFORMATION");
                        alert.setHeaderText(null);
                        alert.setContentText("Data Saved Succesfully");
                        alert.showAndWait();

                    } else {
                        connection.rollback();
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("ERROR");
                        alert.setHeaderText(null);
                        alert.setContentText("Something is missing try again");
                        alert.showAndWait();

                    }

                    Purchase_Name.clear();
                    Purchase_Amount.clear();
                    Purchase_Paid.clear();
                    Purchase_Outstanding.clear();
                    Purchase_Invoice_No.clear();

                    ((TextField) Purchase_Date.getEditor()).clear();
                    Purchase_Mode.getSelectionModel().select("Cash");

                } else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Validation");
                    alert.setHeaderText(null);
                    alert.setContentText("Record is already present with this invoice number");
                    alert.showAndWait();
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stm != null) {
                    stm.close();
                }
                if (connection != null) {
                    connection.close();
                }
            }

        }
    }

    @FXML
    private void Purchase_Cancel(ActionEvent event) throws SQLException, IOException {
        Purchase_Name.clear();
        Purchase_Amount.clear();
        Purchase_Paid.clear();
        Purchase_Outstanding.clear();
        Purchase_Invoice_No.clear();

        ((TextField) Purchase_Date.getEditor()).clear();
        Purchase_Mode.getSelectionModel().select("Cash");
        if (connection != null) {
            connection.close();
        }

        Parent b = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene a = new Scene(b);
        Stage c = (Stage) ((Node) event.getSource()).getScene().getWindow();
        c.setScene(a);
        c.show();
    }

    @FXML
    private void PaidAmountCalculation(KeyEvent event) {

        Global_Total = Float.parseFloat(Purchase_Amount.getText());
        Globle_paidAmount = Float.parseFloat(Purchase_Paid.getText());
        GlobleOutStandingAmount = (Global_Total - Globle_paidAmount);
        Purchase_Outstanding.setText(String.valueOf(GlobleOutStandingAmount));

    }

}
