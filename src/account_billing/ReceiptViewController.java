package account_billing;

import com.osiersystems.pojos.InvoiceModification;
import com.osiersystems.pojos.NumToWord;
import com.osiersystems.pojos.Receipt;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import static java.lang.System.out;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import requriedDatabaseConnection.DatabaseConnection;

/**
 * FXML Controller class
 *
 * @author Niteen
 */
public class ReceiptViewController implements Initializable {

    @FXML
    private TextField Text_Field_Search;
    @FXML
    private Button Button_Search;
    @FXML
    private RadioButton radio_Box_AllReceipt;
    @FXML
    private ToggleGroup radio_Box_ReceiptFilter;
    @FXML
    private RadioButton radio_Box_ByName;
    @FXML
    private TableView<Receipt> Table_Recipt;
    @FXML
    private TableColumn<Receipt, Integer> Table_InvoiceSrNo;
    @FXML
    private TableColumn<Receipt, Integer> Table_ReceiptNumber;
    @FXML
    private TableColumn<Receipt, String> Table_ReceiptDate;
    @FXML
    private TableColumn<Receipt, String> Table_InvoiceNumber;
    @FXML
    private TableColumn<Receipt, String> Table_PersonName;
    @FXML
    private TableColumn<Receipt, String> Table_Contact;
    @FXML
    private TableColumn<Receipt, String> Table_Total_Amount;
    @FXML
    private TableColumn<Receipt, String> Table_Paid_Amount;
    @FXML
    private TableColumn<Receipt, String> Table_Outstanding_Amount;
    @FXML
    private TableColumn<Receipt, String> Table_InvoiceStatus;
    @FXML
    private RadioButton radio_Box_ReceiptNumber;
    @FXML
    private RadioButton radio_Box_InvoiceNumber;
    @FXML
    private TextField NetToalAmount;
    @FXML
    private TextField NetTotalPaid;
    @FXML
    private TextField NetTotalOutstanding;
    @FXML
    private Button Export;

    Connection connection;
    boolean flag = false;
    float Global_NetTotal_Paid, Global_NetTotal_TotalAmount, Global_NetTotal_Outstandig, Globle_NetTotal_Canceled_Amount;
    String Global_SqlQuery = null;

    String Globall_SubQuery = null;

    String Globle_TextField = null;
    int Global_Table_ReceiptNumber;
    String Global_Table_ReciptDate;
    String Global_Table_InvoiceNumber;
    String Global_Table_PersonName;
    String Global_Table_DateOfInvoice;
    String Global_Table_Total_Amount;
    String Global_Table_InvoiceStatus;
    String Globle_Table_Paid_Amount;
    String Globle_Table_Outstanding_Amount;
    Alert alert = new Alert(Alert.AlertType.WARNING);
    @FXML
    private TextField canceldAmount;

//    NumToWord ntw=new NumToWord(); at the time of receipt save in genn invoice controller.
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        connection = DatabaseConnection.getConnection();

        Table_InvoiceSrNo.setCellValueFactory(new PropertyValueFactory<>("Pojo_InvoiceSrNo"));
        Table_InvoiceSrNo.setStyle("-fx-alignment: CENTER");

        Table_ReceiptNumber.setCellValueFactory(new PropertyValueFactory<>("Pojo_ReceiptNumber"));
        Table_ReceiptNumber.setStyle("-fx-alignment: CENTER");

        Table_ReceiptDate.setCellValueFactory(new PropertyValueFactory<>("Pojo_ReceiptDate"));
        Table_ReceiptDate.setStyle("-fx-alignment: CENTER");

        Table_InvoiceNumber.setCellValueFactory(new PropertyValueFactory<>("Pojo_InvoiceNumber"));
        Table_InvoiceNumber.setStyle("-fx-alignment: CENTER");

        Table_PersonName.setCellValueFactory(new PropertyValueFactory<>("Pojo_PersonName"));
        Table_PersonName.setStyle("-fx-alignment: CENTER");

        Table_Contact.setCellValueFactory(new PropertyValueFactory<>("Pojo_Contact"));
        Table_Contact.setStyle("-fx-alignment: CENTER");

        Table_Total_Amount.setCellValueFactory(new PropertyValueFactory<>("Pojo_Total_Amount"));
        Table_Total_Amount.setStyle("-fx-alignment: CENTER");

        Table_Paid_Amount.setCellValueFactory(new PropertyValueFactory<>("Pojo_Paid_Amount"));
        Table_Paid_Amount.setStyle("-fx-alignment: CENTER");

        Table_Outstanding_Amount.setCellValueFactory(new PropertyValueFactory<>("Pojo_Outstanding_Amount"));
        Table_Outstanding_Amount.setStyle("-fx-alignment: CENTER");

        Table_InvoiceStatus.setCellValueFactory(new PropertyValueFactory<>("Pojo_InvoiceStatus"));
        Table_InvoiceStatus.setStyle("-fx-alignment: CENTER");

        clickOnMouseOnTableRecipt();
        try {
            radio_Box_AllReceiptAction();
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptViewController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void radio_Box_AllReceiptAction() throws SQLException {

        if (radio_Box_AllReceipt.isSelected()) {
            flag = false;  //  reinitialize again because flag updated (true) at the time of all bill load .
            Text_Field_Search.setDisable(true);
            Button_Search.setDisable(true);

            Global_SqlQuery = "SELECT * FROM  RECEIPT_DETAILS";

            loadDataFromDatabaseToTable();

        }

    }

    @FXML
    private void searchButtonAction(ActionEvent event) throws SQLException {

        if (Text_Field_Search.getText().isEmpty()) {

            alert.setTitle("Note");
            alert.setHeaderText(null);
            alert.setContentText(" Enter Name of person  within Text Field !");
            alert.showAndWait();

        } else {

            Globle_TextField = Text_Field_Search.getText();
            flag = false;  //  reinitialize again because flag updated (true) at the time of all bill load .
            Global_SqlQuery = "select * From RECEIPT_DETAILS where " + Globall_SubQuery + " = '" + Globle_TextField + "' ";
            loadDataFromDatabaseToTable();

            if (flag == false) {

                alert.setTitle("Chacking Items");
                alert.setHeaderText(null);
                alert.setContentText("This type of Invoice Not Found please correct text Field");
                alert.showAndWait();

            }

        }
    }

    @FXML
    private void radio_Box_ByNameAction(ActionEvent event) throws SQLException {
        Text_Field_Search.setDisable(false);
        Button_Search.setDisable(false);

        //---------------------------------------Auto complete Customer name list code---------------------------------------------------
        ObservableList list3 = FXCollections.observableArrayList();
        try {

            connection = DatabaseConnection.getConnection();

            String query3 = "select CUSTOMER_NAME from RECEIPT_DETAILS";
            PreparedStatement preparedStatement3 = connection.prepareStatement(query3);
            ResultSet resultSet3 = preparedStatement3.executeQuery();

            while (resultSet3.next()) {

                list3.add(resultSet3.getString("CUSTOMER_NAME"));
            }

            org.controlsfx.control.textfield.TextFields.bindAutoCompletion(Text_Field_Search, list3);

            preparedStatement3.close();
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptViewController.class.getName()).log(Level.SEVERE, null, ex);

        }

        Globall_SubQuery = "CUSTOMER_NAME";

    }

    @FXML
    private void radio_Box_ReceiptNumberAction(ActionEvent event) {
        Text_Field_Search.setDisable(false);
        Button_Search.setDisable(false);
        Globall_SubQuery = "RECEIPT_NUMBER";
    }

    @FXML
    private void radio_Box_InvoiceNumberAction(ActionEvent event) {
        Text_Field_Search.setDisable(false);
        Button_Search.setDisable(false);

        Globall_SubQuery = "INVOICE_NUMBER";
    }

    public void loadDataFromDatabaseToTable() throws SQLException {

        Global_NetTotal_TotalAmount = 0;
        Global_NetTotal_Paid = 0;
        Global_NetTotal_Outstandig = 0;

        ObservableList<Receipt> list = FXCollections.observableArrayList();
        Statement stm = connection.createStatement();
        ResultSet resultSet = stm.executeQuery(Global_SqlQuery);
        int i = 1;
        while (resultSet.next()) {

            Global_Table_ReceiptNumber = resultSet.getInt("RECEIPT_NUMBER");/// Direct Call here   // here update invoice number using mathod  using count query.
            Global_Table_ReciptDate = resultSet.getString("RECEIPT_DATE");
            Global_Table_InvoiceNumber = resultSet.getString("INVOICE_NUMBER");
            Global_Table_PersonName = resultSet.getString("CUSTOMER_NAME");
            Global_Table_DateOfInvoice = resultSet.getString("MOBILE_NUMBER");
            Global_Table_Total_Amount = resultSet.getString("NET_TOTAL_AMOUN");
            Globle_Table_Paid_Amount = resultSet.getString("PAID");
            Globle_Table_Outstanding_Amount = resultSet.getString("OUTSTANDING");
            Global_Table_InvoiceStatus = resultSet.getString("STATUS");

            if (Global_Table_InvoiceStatus.equalsIgnoreCase("Canceled")) {
                // cancled amount can add within net total
                Globle_NetTotal_Canceled_Amount = Globle_NetTotal_Canceled_Amount + Float.parseFloat(Global_Table_Total_Amount);

            } else {
                Global_NetTotal_TotalAmount = (Global_NetTotal_TotalAmount + Float.parseFloat(Global_Table_Total_Amount)); //for nettotal calculation
                Global_NetTotal_Paid = (Global_NetTotal_Paid + Float.parseFloat(Globle_Table_Paid_Amount));
                Global_NetTotal_Outstandig = (Global_NetTotal_Outstandig + Float.parseFloat(Globle_Table_Outstanding_Amount));
            }

            list.add(new Receipt(i, Global_Table_ReceiptNumber, Global_Table_ReciptDate, Global_Table_InvoiceNumber, Global_Table_PersonName, Global_Table_DateOfInvoice, Global_Table_Total_Amount, Globle_Table_Paid_Amount, Globle_Table_Outstanding_Amount, Global_Table_InvoiceStatus));
            i++;
            flag = true;
        }

        Table_Recipt.setItems(list);
        Text_Field_Search.setText("");

        NetToalAmount.setText(String.valueOf(Global_NetTotal_TotalAmount));
        NetTotalPaid.setText(String.valueOf(Global_NetTotal_Paid));
        NetTotalOutstanding.setText(String.valueOf(Global_NetTotal_Outstandig));
        canceldAmount.setText(String.valueOf(Globle_NetTotal_Canceled_Amount));

    }

    @FXML
    private void clickOnMouseOnTableRecipt() {

        Table_Recipt.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                Receipt list = Table_Recipt.getItems().get(Table_Recipt.getSelectionModel().getSelectedIndex());

                int ReceiptNo = list.getPojo_ReceiptNumber();

                System.out.println(ReceiptNo);

                try {

                    //          JasperDesign jd = JRXmlLoader.load("C:\\Users\\Niteen\\Documents\\NetBeansProjects\\Account_Billing\\src\\reports\\report2.jrxml");
                    JasperDesign jd = JRXmlLoader.load("src\\reports\\report2.jrxml");
                    HashMap param = new HashMap();
                    param.put("receiptNo", ReceiptNo);

                    JasperReport jr = JasperCompileManager.compileReport(jd);
                    JasperPrint jp = JasperFillManager.fillReport(jr, param, connection);
                    JasperViewer jv = new JasperViewer(jp, false);

                    jv.setVisible(true);

                } catch (Exception e) {
                    System.err.println(e);
                }

            }
        });

    }

    @FXML
    private void exportAction(ActionEvent event) throws SQLException, FileNotFoundException, IOException {

        if (radio_Box_AllReceipt.isSelected()) {
            Global_SqlQuery = "SELECT * FROM  RECEIPT_DETAILS";

        }

        if (radio_Box_InvoiceNumber.isSelected()) {
            Globall_SubQuery = "INVOICE_NUMBER";
            Global_SqlQuery = "select * From RECEIPT_DETAILS where " + Globall_SubQuery + " = '" + Globle_TextField + "' ";

        }
        if (radio_Box_ReceiptNumber.isSelected()) {
            Globall_SubQuery = "RECEIPT_NUMBER";
            Global_SqlQuery = "select * From RECEIPT_DETAILS where " + Globall_SubQuery + " = '" + Globle_TextField + "' ";

        }
        if (radio_Box_ByName.isSelected()) {
            Globall_SubQuery = "CUSTOMER_NAME";
            Global_SqlQuery = "select * From RECEIPT_DETAILS where " + Globall_SubQuery + " = '" + Globle_TextField + "' ";

        }

        PreparedStatement ps = connection.prepareStatement(Global_SqlQuery);

        ResultSet rs = ps.executeQuery();

        XSSFWorkbook wb = new XSSFWorkbook();

        XSSFSheet sheet = wb.createSheet("Receipt_Report");

        XSSFRow header = sheet.createRow(0);

        header.createCell(0).setCellValue("RECEIPT_NUMBER");
        header.createCell(1).setCellValue("RECEIPT_DATE");
        header.createCell(2).setCellValue("INVOICE_NUMBER");
        header.createCell(3).setCellValue("CUSTOMER_NAME");
        header.createCell(4).setCellValue("MOBILE_NUMBER");
        header.createCell(5).setCellValue("NET_TOTAL_AMOUN");
        header.createCell(6).setCellValue("PAID");
        header.createCell(7).setCellValue("OUTSTANDING");
        header.createCell(8).setCellValue("STATUS");
        header.createCell(9).setCellValue("ADDRESS");
        header.createCell(10).setCellValue("AMOUNT_INWORD");

        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
        sheet.autoSizeColumn(4);
        sheet.autoSizeColumn(5);
        sheet.autoSizeColumn(6);
        sheet.autoSizeColumn(7);
        sheet.autoSizeColumn(8);
        sheet.autoSizeColumn(9);
        sheet.autoSizeColumn(10);

        sheet.setColumnWidth(1, 256 * 20);

        sheet.setZoom(150);

        int index = 1;

        while (rs.next()) {

            System.out.println("in while" + index);

            XSSFRow row = sheet.createRow(index);

            row.createCell(0).setCellValue(rs.getInt("RECEIPT_NUMBER"));
            row.createCell(1).setCellValue(rs.getString("RECEIPT_DATE"));
            row.createCell(2).setCellValue(rs.getString("INVOICE_NUMBER"));
            row.createCell(3).setCellValue(rs.getString("CUSTOMER_NAME"));
            row.createCell(4).setCellValue(rs.getString("MOBILE_NUMBER"));
            row.createCell(5).setCellValue(rs.getString("NET_TOTAL_AMOUN"));
            row.createCell(6).setCellValue(rs.getString("PAID"));
            row.createCell(7).setCellValue(rs.getString("OUTSTANDING"));
            row.createCell(8).setCellValue(rs.getString("STATUS"));
            row.createCell(9).setCellValue(rs.getString("ADDRESS"));
            row.createCell(10).setCellValue(rs.getString("AMOUNT_INWORD"));

            index++;

        }

        XSSFRow row = sheet.createRow(index);
        row.createCell(0).setCellValue("---");
        row.createCell(1).setCellValue("---");
        row.createCell(2).setCellValue("---");
        row.createCell(3).setCellValue("---");
        row.createCell(4).setCellValue("---");
        row.createCell(5).setCellValue("NetTotal Amount");
        row.createCell(6).setCellValue("NetTotal Paid");
        row.createCell(7).setCellValue("NetTotal Outstanding");
        row.createCell(8).setCellValue("---");
        row.createCell(9).setCellValue("---");
        row.createCell(10).setCellValue("---");

        XSSFRow row1 = sheet.createRow(index + 1);
        row1.createCell(0).setCellValue("");
        row1.createCell(1).setCellValue("");
        row1.createCell(2).setCellValue("");//////////////////
        row1.createCell(4).setCellValue("");
        row1.createCell(5).setCellValue(NetToalAmount.getText());
        row1.createCell(6).setCellValue(NetTotalPaid.getText());
        row1.createCell(7).setCellValue(NetTotalOutstanding.getText());
        row1.createCell(8).setCellValue("");
        row1.createCell(9).setCellValue("");
        row1.createCell(10).setCellValue("");

        FileOutputStream fileout = new FileOutputStream("ReceiptReport.XLSX");

        wb.write(fileout);

        fileout.close();

        rs.close();

        ps.close();

        connection.close();

        alert.setTitle("Success");
        alert.setContentText("Your Export completed successfully");

        alert.showAndWait();
    }
}
