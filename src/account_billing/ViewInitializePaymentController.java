package account_billing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.osiersystems.pojos.InitializePayment;
import com.osiersystems.pojos.ViewAccounts;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.StringConverter;
import requriedDatabaseConnection.DatabaseConnection;


/**
 * FXML Controller class
 *
 * @author G1 NOTEBOOK
 */
public class ViewInitializePaymentController implements Initializable {

    @FXML
    private TableView<InitializePayment> table_viewAccount;
    @FXML
    private TableColumn<InitializePayment, Integer> V_Purchase_Srno;
    @FXML
    private TableColumn<InitializePayment, String> V_P_Invoice_No;
    @FXML
    private TableColumn<InitializePayment, String> V_P_Date;
    @FXML
    private TableColumn<InitializePayment, String> V_P_Amount;
    @FXML
    private TableColumn<InitializePayment, String> V_P_Paid;
    @FXML
    private TableColumn<InitializePayment, String> V_P_OutSanding;
    @FXML
    private TableColumn<InitializePayment, String> V_P_Status;
    @FXML
    private TextField Initialize_payment_amount;
    @FXML
    private DatePicker initilazie_payment_date;
    @FXML
    private ComboBox Initialize_payment_mode;

    Float GlobalAmount, GlobalPaid, GlobalOutstanding;
    String GloabalPurchaseDate, GlobalPurchaseMode;

    /**
     * Initializes the controller class.
     */
    String invoicenop;
    String nameofsupplierp;
    Connection connection;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        connection = DatabaseConnection.getConnection();
        V_Purchase_Srno.setCellValueFactory(new PropertyValueFactory<>("Pojo_SrNo"));
        V_Purchase_Srno.setStyle("-fx-alignment: CENTER");

        V_P_Invoice_No.setCellValueFactory(new PropertyValueFactory<>("Pojo_Invoice_No"));
        V_P_Invoice_No.setStyle("-fx-alignment: CENTER");

        V_P_Date.setCellValueFactory(new PropertyValueFactory<>("Pojo_Date"));
        V_P_Date.setStyle("-fx-alignment: CENTER");

        V_P_Amount.setCellValueFactory(new PropertyValueFactory<>("Pojo_Amount"));
        V_P_Amount.setStyle("-fx-alignment: CENTER");

        V_P_Paid.setCellValueFactory(new PropertyValueFactory<>("Pojo_Paid"));
        V_P_Paid.setStyle("-fx-alignment: CENTER");

        V_P_OutSanding.setCellValueFactory(new PropertyValueFactory<>("Pojo_Outstanding"));
        V_P_OutSanding.setStyle("-fx-alignment: CENTER");

        V_P_Status.setCellValueFactory(new PropertyValueFactory<>("Pojo_Status"));
        V_P_Status.setStyle("-fx-alignment: CENTER");

        String state[] = {"Cash", "Online"};

        Initialize_payment_mode.getItems().removeAll(Initialize_payment_mode.getItems());
        Initialize_payment_mode.getItems().addAll(state);
        Initialize_payment_mode.getSelectionModel().select("Cash");

        initilazie_payment_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "yyyy-MM-dd";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            {
                initilazie_payment_date.setPromptText(pattern.toLowerCase());
            }

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

    }

    public void setTextforViewAccount(String invoiceno, String nameofsupplier) throws SQLException {
        //used to retrive records from purchase_history table 
        invoicenop = invoiceno;
        nameofsupplierp = nameofsupplier;
        loaddataFirsttime();

    }

    @FXML
    private void setCellValueFromToTextField(MouseEvent event) {
    }

    private void loaddataFirsttime() throws SQLException {
        connection = DatabaseConnection.getConnection();
        String query = "select *from purchase_details where invoice_no='" + invoicenop + "'";
        ObservableList<InitializePayment> list = FXCollections.observableArrayList();

        Statement stm = connection.createStatement();
        ResultSet resultSet = stm.executeQuery(query);
        int i = 1;
        while (resultSet.next()) {
            System.out.println("Amount:" + resultSet.getFloat("amount"));
            System.out.println("Paid:" + resultSet.getFloat("paid"));
            System.out.println("outstanding:" + resultSet.getFloat("outstanding"));
            GlobalAmount = resultSet.getFloat("amount");
            GlobalOutstanding = resultSet.getFloat("outstanding");
            GlobalPaid = resultSet.getFloat("paid");
            GloabalPurchaseDate = resultSet.getString("purchase_date");
            GlobalPurchaseMode = resultSet.getString("purchase_mode");
            list.add(new InitializePayment(i, invoicenop, GloabalPurchaseDate, String.valueOf(GlobalAmount), String.valueOf(GlobalPaid), String.valueOf(GlobalOutstanding), GlobalPurchaseMode));
            i++;

        }

        table_viewAccount.setItems(list);
        resultSet.close();
        stm.close();
        connection.close();

    }

    @FXML
    private void Clickonsave(ActionEvent event) throws SQLException {
        connection = DatabaseConnection.getConnection();

        if (!("".equals(Initialize_payment_amount.getText())) && !("".equals(initilazie_payment_date.getEditor().getText()))) {
            if (((Float.parseFloat(Initialize_payment_amount.getText())) <= GlobalOutstanding)) {

                //  System.out.println("GlobalAmount:" + GlobalAmount);
                Float outstandingafterpaid = GlobalOutstanding - (Float.parseFloat(Initialize_payment_amount.getText()));
                // System.out.println("outstandingafterpaid:" + outstandingafterpaid);
                Float paidafterpaid = GlobalPaid + (Float.parseFloat(Initialize_payment_amount.getText()));
                String query4initialize = "update purchase_details set purchase_date=date('" + initilazie_payment_date.getEditor().getText() + "'),paid=" + paidafterpaid + ",outstanding=" + outstandingafterpaid + ",purchase_mode='" + Initialize_payment_mode.getSelectionModel().getSelectedItem() + "' where invoice_no='" + invoicenop + "'";
                Statement stm = connection.createStatement();
                int a = stm.executeUpdate(query4initialize);
                stm.close();
                //connection.close();
                Statement stm1 = connection.createStatement();
                String query4initialize1 = "insert into purchase_history values('" + invoicenop + "',date('" + initilazie_payment_date.getEditor().getText() + "')," + GlobalAmount + "," + Float.parseFloat(Initialize_payment_amount.getText()) + "," + outstandingafterpaid + ",'" + Initialize_payment_mode.getSelectionModel().getSelectedItem() + "') ";

                int b = stm1.executeUpdate(query4initialize1);
                stm1.close();
                connection.close();

                if (a > 0 && b > 0) {

                    loaddataFirsttime();

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Initialize Payment");
                    alert.setHeaderText(null);
                    alert.setContentText("Payment initialization completed successfully ");
                    alert.showAndWait();
                    initilazie_payment_date.setValue(null);
                    Initialize_payment_amount.clear();
                    Initialize_payment_mode.getSelectionModel().select("Cash");

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Initialize Payment");
                    alert.setHeaderText(null);
                    alert.setContentText("Something went wrong try again...");
                    alert.showAndWait();
                    initilazie_payment_date.setValue(null);
                    Initialize_payment_amount.clear();
                    Initialize_payment_mode.getSelectionModel().select("Cash");

                }

            } else {

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Initialize Payment");
                alert.setHeaderText(null);
                alert.setContentText("Your Amount should be less than or equal to current ourstanding");
                alert.showAndWait();

            }

        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Initialize Payment");
            alert.setHeaderText(null);
            alert.setContentText("All Fields are mandatory.");
            alert.showAndWait();

        }

    }

    @FXML
    private void clickoncancel(ActionEvent event) {
        System.out.println("clickoncancel");

    }

}
