/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account_billing;

import com.osiersystems.pojos.Balance;
import requriedDatabaseConnection.DatabaseConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author user
 */
public class Balance_StockController implements Initializable {

    @FXML
    private TextField net_Total_Sale_Price;
    @FXML
    private TextField net_Total_Stock;
    @FXML
    private TableView<Balance> table;

    @FXML
    private TableColumn<Balance, Integer> sr_No;
    @FXML
    private TableColumn<Balance, String> name;
    @FXML
    private TableColumn<Balance, String> stock;
    @FXML
    private TableColumn<Balance, String> sale_Price;

    int t = 0;
    int srno = 1;
    private ObservableList<Balance> list = FXCollections.observableArrayList();

    // requried variable  
    float totalSalePriceCalculationPurpose = 0;
    int totalStockCalculationPurpose = 0;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

//     
        //  mrpColumn.setCellValueFactory(new PropertyValueFactory<>("mrp") );
        sr_No.setCellValueFactory(new PropertyValueFactory<>("srno"));   // srno use pojo variable name in maping
        sr_No.setStyle("-fx-alignment: CENTER");
        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        name.setStyle("-fx-alignment: CENTER");
        stock.setCellValueFactory(new PropertyValueFactory<>("stock"));
        stock.setStyle("-fx-alignment: CENTER");
        sale_Price.setCellValueFactory(new PropertyValueFactory<>("price"));
        sale_Price.setStyle("-fx-alignment: CENTER");

        net_Total_Sale_Price.setEditable(false);
        net_Total_Stock.setEditable(false);

        try {
             t = 0;
            Connection connection = DatabaseConnection.getConnection();
            Statement stm = connection.createStatement();
            ResultSet rs = stm.executeQuery("select * from ADD_ITEMS_DETAILS where stock > 0");
            while (rs.next()) {

                list.add(new Balance(srno,
                        rs.getString("ITEMS_NAME"),
                        rs.getString("STOCK"),
                        rs.getString("SALE_PRICE")
                ));

                table.setItems(list);

                t = 1;
                srno++;
                int singlestock = (Integer.parseInt(rs.getString("STOCK")));
                float singlesaleprice = (Float.parseFloat(rs.getString("SALE_PRICE")));

                totalStockCalculationPurpose = (totalStockCalculationPurpose + singlestock);

                Float totalsingleStock = (singlestock * singlesaleprice);    // multiple stock with one sale price value.

                totalSalePriceCalculationPurpose = (totalSalePriceCalculationPurpose + totalsingleStock);

            }

            net_Total_Stock.setText(String.valueOf(totalStockCalculationPurpose));
            net_Total_Sale_Price.setText(String.valueOf(totalSalePriceCalculationPurpose));

            if (t == 0) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Validate owner name");
                alert.setHeaderText(null);
                alert.setContentText("All stock is Avalible");
                alert.showAndWait();

            }

            connection.close();
            stm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
