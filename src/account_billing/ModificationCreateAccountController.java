package account_billing;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import requriedDatabaseConnection.DatabaseConnection;
import validation.ValidateHere;

public class ModificationCreateAccountController implements Initializable {

    @FXML
    private TextField business_Name;
    @FXML
    private TextField business_Registraction_No;
    @FXML
    private TextField business_Pan_No;
    @FXML
    private TextField business_Gst_No;
    @FXML
    private TextField business_Email;
    @FXML
    private TextField business_Phone_No;
    @FXML
    private TextArea business_Address;
    @FXML
    private TextField business_Address_Pin_Code;
    @FXML
    private ComboBox<String> business_Address_State;
    @FXML
    private TextField bank_Name;
    @FXML
    private TextField Branch;
    @FXML
    private TextField account_No;
    @FXML
    private TextField ifsc_No;
    @FXML
    private TextField contact_Person;
    @FXML
    private TextField person_Email;
    @FXML
    private TextField person_Mobile_No;
    @FXML
    private TextField person_Pan_No;
    @FXML
    private RadioButton person_Gender_Male;
    @FXML
    private ToggleGroup gender;
    @FXML
    private RadioButton person_Gender_Female;
    @FXML
    private ComboBox<String> person_Address_State;
    @FXML
    private TextField person_Address_Pin_Code;
    @FXML
    private TextArea person_Address;
    @FXML
    private TextField business_Address_City;
    @FXML
    private TextField person_Address_City;
    @FXML
    private ToggleGroup status;

    Connection connection;

    String gender1 = "Male";
    @FXML
    private RadioButton radioButton_active;
    @FXML
    private RadioButton radioButton_Deactive;

    String status2 = "Active";

    String BusinessEmailp;
    @FXML
    private BorderPane broderPaneWindows;
    @FXML
    private AnchorPane Modification_Windows;
    @FXML
    private Button ButtonEdit;
    @FXML
    private Button ButtonDelete;
    @FXML
    private Button ButtonUpdate;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        connection = DatabaseConnection.getConnection();

    }

    public void setTextforViewAccount(String businessName) {

        BusinessEmailp = businessName;

        try {

            combobox();

            loaddataFirsttime();
        } catch (SQLException ex) {
            Logger.getLogger(ModificationCreateAccountController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void editOnAction(ActionEvent event) {

        business_Name.setEditable(false);
        business_Registraction_No.setEditable(true);
        business_Pan_No.setEditable(true);
        business_Gst_No.setEditable(true);
        business_Email.setEditable(true);
        business_Phone_No.setEditable(true);
        business_Address.setEditable(true);
        business_Address_Pin_Code.setEditable(true);

        business_Address_State.getSelectionModel().select("Maharashtra");
        business_Address_State.setDisable(false);

        business_Address_City.setEditable(true);
        bank_Name.setEditable(true);
        Branch.setEditable(true);
        account_No.setEditable(true);
        ifsc_No.setEditable(true);
        contact_Person.setEditable(true);
        person_Email.setEditable(true);
        person_Mobile_No.setEditable(true);
        person_Pan_No.setEditable(true);
        person_Gender_Female.setDisable(false);
        person_Gender_Male.setDisable(false);
        person_Address.setEditable(true);
        person_Address_City.setEditable(true);

        person_Address_State.getSelectionModel().select("Maharashtra");
        person_Address_State.setDisable(false);

        person_Address_Pin_Code.setEditable(true);

        radioButton_Deactive.setDisable(false);
        radioButton_active.setDisable(false);
        ButtonDelete.setDisable(false);
        ButtonEdit.setDisable(true);
        ButtonUpdate.setDisable(false);
    }

    @FXML
    private void deleteOnAction(ActionEvent event) throws SQLException {

        Statement statement = connection.createStatement();
        int flag = statement.executeUpdate("Delete From CUSTOMER_ACCOUNT where BUSINESS_NAME='" + BusinessEmailp + "' ");

        if (flag > 0) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Validate owner name");
            alert.setHeaderText(null);
            alert.setContentText(" Account Delete Sucessfully ");
            alert.showAndWait();

        } else {

            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Validate owner name");
            alert.setHeaderText(null);
            alert.setContentText(" Account not Deleted  ");
            alert.showAndWait();

        }

    }

    @FXML
    private void saveOnAction(ActionEvent event) {

        if (person_Gender_Female.isSelected()) {
            gender1 = "Female";
        }
        if (radioButton_Deactive.isSelected()) {
            status2 = "Deactive";
        }

        //  validation here 
        if (business_Name.getText().isEmpty() || business_Address.getText().isEmpty() || business_Address_Pin_Code.getText().isEmpty() || business_Email.getText().isEmpty() || business_Gst_No.getText().isEmpty() || business_Pan_No.getText().isEmpty() || business_Phone_No.getText().isEmpty() || business_Address.getText().isEmpty() || person_Address.getText().isEmpty() || person_Address_Pin_Code.getText().isEmpty() || person_Email.getText().isEmpty() || person_Mobile_No.getText().isEmpty() || person_Pan_No.getText().isEmpty() || bank_Name.getText().isEmpty() || Branch.getText().isEmpty() || ifsc_No.getText().isEmpty() || account_No.getText().isEmpty()) {

            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Validation");
            alert.setHeaderText(null);
            alert.setContentText("All text field must be important");
            alert.showAndWait();

        } else {
            //ValidateHere.panNumber(business_Pan_No.getText()) && ValidateHere.gstNumber(business_Gst_No.getText()) &&ValidateHere.email(business_Email.getText()) && ValidateHere.mobile(business_Phone_No.getText())  && ValidateHere.ifscNumber(ifsc_No.getText()) && ValidateHere.email(person_Email.getText()) && ValidateHere.mobile(person_Mobile_No.getText()) && ValidateHere.panNumber(person_Pan_No.getText()) && ValidateHere.number(account_No.getText()) && ValidateHere.number(business_Registraction_No.getText()) && ValidateHere.pinNumber(business_Address_Pin_Code.getText()) && ValidateHere.pinNumber(person_Address_Pin_Code.getText())
            if (ValidateHere.panNumber(business_Pan_No.getText()) && ValidateHere.gstNumber(business_Gst_No.getText()) && ValidateHere.email(business_Email.getText()) && ValidateHere.number(business_Phone_No.getText()) && ValidateHere.ifscNumber(ifsc_No.getText()) && ValidateHere.email(person_Email.getText()) && ValidateHere.mobile(person_Mobile_No.getText()) && ValidateHere.panNumber(person_Pan_No.getText()) && ValidateHere.number(account_No.getText()) && ValidateHere.number(business_Registraction_No.getText()) && ValidateHere.pinNumber(business_Address_Pin_Code.getText()) && ValidateHere.pinNumber(person_Address_Pin_Code.getText())) {

                try {

                    PreparedStatement preparedStatement = connection.prepareStatement("UPDATE CUSTOMER_ACCOUNT SET PERSON_EMAIL=?, BUSINESS_REGISTRACTION_NO=?, BUSINESS_PAN_NO=?, BUSINESS_GST_NO=?, BUSINESS_EMAIL=?, BUSINESS_PHONE_NO=?, BUSINESS_ADDRESS=?, BUSINESS_ADDRESS_PIN_CODE=?, BUSINESS_ADDRESS_STATE=?, BUSINESS_ADDRESS_CITY=?, BANK_NAME=?, BRANCH=?, ACCOUNT_NO=?, IFSC_NO=?, CONTACT_PERSON=?, PERSON_MOBILE_NO=?, PERSON_PAN_NO=?, GENDER=?, PERSON_ADDRESS=?, PERSON_ADDRESS_CITY=?, PERSON_ADDRESS_STATE=?, PERSON_ADDRESS_PIN_CODE=?, STATUS=? where BUSINESS_NAME='" + BusinessEmailp + "' ");

                    preparedStatement.setString(1, person_Email.getText());
                    preparedStatement.setString(2, business_Registraction_No.getText());
                    preparedStatement.setString(3, business_Pan_No.getText());
                    preparedStatement.setString(4, business_Gst_No.getText());
                    preparedStatement.setString(5, business_Email.getText());
                    preparedStatement.setString(6, business_Phone_No.getText());
                    preparedStatement.setString(7, business_Address.getText());
                    preparedStatement.setString(8, business_Address_Pin_Code.getText());
                    preparedStatement.setString(9, (String) business_Address_State.getSelectionModel().getSelectedItem());   // ps.setString(2, (String) combo_units.getSelectionModel().getSelectedItem());
                    preparedStatement.setString(10, business_Address_City.getText());
                    preparedStatement.setString(11, bank_Name.getText());
                    preparedStatement.setString(12, Branch.getText());
                    preparedStatement.setString(13, account_No.getText());
                    preparedStatement.setString(14, ifsc_No.getText());
                    preparedStatement.setString(15, contact_Person.getText());

                    //   preparedStatement.setString(16, person_Email.getText());   // PERSON_EMAIL
//                    preparedStatement.setString(17, person_Mobile_No.getText());
//                    preparedStatement.setString(18, person_Pan_No.getText());
//                    preparedStatement.setString(19, gender1);
//                    preparedStatement.setString(20, person_Address.getText());
//                    preparedStatement.setString(21, person_Address_City.getText());
//                    preparedStatement.setString(22, (String) person_Address_State.getSelectionModel().getSelectedItem());
//                    preparedStatement.setString(23, person_Address_Pin_Code.getText());
//                    preparedStatement.setString(24, status2);
                    preparedStatement.setString(16, person_Mobile_No.getText());
                    preparedStatement.setString(17, person_Pan_No.getText());
                    preparedStatement.setString(18, gender1);
                    preparedStatement.setString(19, person_Address.getText());
                    preparedStatement.setString(20, person_Address_City.getText());
                    preparedStatement.setString(21, (String) person_Address_State.getSelectionModel().getSelectedItem());
                    preparedStatement.setString(22, person_Address_Pin_Code.getText());
                    preparedStatement.setString(23, status2);

                    int result = preparedStatement.executeUpdate();
                    preparedStatement.close();

                    if (result > 0) {

                        business_Name.clear();
                        business_Registraction_No.clear();
                        business_Pan_No.clear();
                        business_Gst_No.clear();
                        business_Email.clear();
                        business_Phone_No.clear();
                        business_Address.clear();
                        business_Address_Pin_Code.clear();
                        business_Address_State.getSelectionModel().select("Maharashtra");
                        business_Address_City.clear();
                        bank_Name.clear();
                        Branch.clear();
                        account_No.clear();
                        ifsc_No.clear();
                        contact_Person.clear();
                        person_Email.clear();
                        person_Mobile_No.clear();
                        person_Pan_No.clear();

                        person_Address.clear();
                        person_Address_City.clear();
                        person_Address_State.getSelectionModel().select("Maharashtra");
                        person_Address_Pin_Code.clear();

                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Validation");
                        alert.setHeaderText(null);
                        alert.setContentText(" Account Successfuly Edited ");
                        alert.showAndWait();

                        //  loaddataFirsttime();
                        business_Name.setEditable(false);
                        business_Registraction_No.setEditable(false);
                        business_Pan_No.setEditable(false);
                        business_Gst_No.setEditable(false);
                        business_Email.setEditable(false);
                        business_Phone_No.setEditable(false);
                        business_Address.setEditable(false);
                        business_Address_Pin_Code.setEditable(false);
                        business_Address_State.getSelectionModel().select("Maharashtra");
                        business_Address_State.setDisable(true);
                        business_Address_City.setEditable(false);
                        bank_Name.setEditable(false);
                        Branch.setEditable(false);
                        account_No.setEditable(false);
                        ifsc_No.setEditable(false);
                        contact_Person.setEditable(false);
                        person_Email.setEditable(false);
                        person_Mobile_No.setEditable(false);
                        person_Pan_No.setEditable(false);
                        person_Gender_Female.setDisable(true);
                        person_Gender_Male.setDisable(true);

                        person_Address.setEditable(false);
                        person_Address_City.setEditable(false);
                        person_Address_State.getSelectionModel().select("Maharashtra");
                        person_Address_State.setDisable(true);
                        person_Address_Pin_Code.setEditable(false);

                        radioButton_Deactive.setDisable(true);
                        radioButton_active.setDisable(true);
                        ButtonDelete.setDisable(true);
                        ButtonEdit.setDisable(true);
                        ButtonUpdate.setDisable(true);

                        //     System.exit(0);
                    } else {

                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Validation");
                        alert.setHeaderText(null);
                        alert.setContentText(" Try Again Account not Edited ");
                        alert.showAndWait();
                    }

                } catch (SQLException ex) {
                    System.out.println(" flag 7");
                    System.err.println("I am within catch block");
                    Logger.getLogger(CreateAccountController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {

                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Validation");
                alert.setHeaderText(null);
                alert.setContentText("Try again");
                alert.showAndWait();
            }

        }
    }

    public void loaddataFirsttime() throws SQLException {

        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("Select * From CUSTOMER_ACCOUNT where BUSINESS_NAME='" + BusinessEmailp + "'");
        while (resultSet.next()) {
            /*
            
  CREATE TABLE "SYSTEM"."CUSTOMER_ACCOUNT" 
   (	"BUSINESS_NAME" VARCHAR2(50 BYTE), 
	"BUSINESS_REGISTRACTION_NO" VARCHAR2(20 BYTE), 
	"BUSINESS_PAN_NO" VARCHAR2(20 BYTE), 
	"BUSINESS_GST_NO" VARCHAR2(20 BYTE), 
	"BUSINESS_EMAIL" VARCHAR2(30 BYTE), 
	"BUSINESS_PHONE_NO" VARCHAR2(20 BYTE), 
	"BUSINESS_ADDRESS" VARCHAR2(20 BYTE), 
	"BUSINESS_ADDRESS_PIN_CODE" VARCHAR2(20 BYTE), 
	"BUSINESS_ADDRESS_STATE" VARCHAR2(20 BYTE), 
	"BUSINESS_ADDRESS_CITY" VARCHAR2(20 BYTE), 
	"BANK_NAME" VARCHAR2(25 BYTE), 
	"BRANCH" VARCHAR2(20 BYTE), 
	"ACCOUNT_NO" VARCHAR2(20 BYTE), 
	"IFSC_NO" VARCHAR2(20 BYTE), 
	"CONTACT_PERSON" VARCHAR2(40 BYTE), 
	"PERSON_EMAIL" VARCHAR2(30 BYTE), 
	"PERSON_MOBILE_NO" VARCHAR2(20 BYTE), 
	"PERSON_PAN_NO" VARCHAR2(20 BYTE), 
	"GENDER" VARCHAR2(20 BYTE), 
	"PERSON_ADDRESS" VARCHAR2(40 BYTE), 
	"PERSON_ADDRESS_CITY" VARCHAR2(20 BYTE), 
	"PERSON_ADDRESS_STATE" VARCHAR2(20 BYTE), 
	"PERSON_ADDRESS_PIN_CODE" VARCHAR2(20 BYTE), 
	"STATUS" VARCHAR2(20 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" ;
             */
            business_Name.setText(resultSet.getString("BUSINESS_NAME"));
            business_Registraction_No.setText(resultSet.getString("BUSINESS_REGISTRACTION_NO"));
            business_Pan_No.setText(resultSet.getString("BUSINESS_PAN_NO"));
            business_Gst_No.setText(resultSet.getString("BUSINESS_GST_NO"));
            business_Email.setText(resultSet.getString("BUSINESS_EMAIL"));
            business_Phone_No.setText(resultSet.getString("BUSINESS_PHONE_NO"));
            business_Address.setText(resultSet.getString("BUSINESS_ADDRESS"));
            business_Address_Pin_Code.setText(resultSet.getString("BUSINESS_ADDRESS_PIN_CODE"));
            // combo_units.getSelectionModel().select("Select");
            business_Address_State.getSelectionModel().select(resultSet.getString("BUSINESS_ADDRESS_STATE"));
            business_Address_City.setText(resultSet.getString("BUSINESS_ADDRESS_CITY"));
            bank_Name.setText(resultSet.getString("BANK_NAME"));
            Branch.setText(resultSet.getString("BRANCH"));
            account_No.setText(resultSet.getString("ACCOUNT_NO"));
            ifsc_No.setText(resultSet.getString("IFSC_NO"));
            contact_Person.setText(resultSet.getString("CONTACT_PERSON"));
            person_Email.setText(resultSet.getString("PERSON_EMAIL"));
            person_Mobile_No.setText(resultSet.getString("PERSON_MOBILE_NO"));
            person_Pan_No.setText(resultSet.getString("PERSON_PAN_NO"));

            String temppp = resultSet.getString("GENDER");
            if (temppp.equalsIgnoreCase("Male")) {
                gender.selectToggle(person_Gender_Male);
            } else {
                gender.selectToggle(person_Gender_Female);
            }

            person_Address.setText(resultSet.getString("PERSON_ADDRESS"));
            person_Address_City.setText(resultSet.getString("PERSON_ADDRESS_CITY"));
            person_Address_State.getSelectionModel().select(resultSet.getString("PERSON_ADDRESS_STATE"));
            person_Address_Pin_Code.setText(resultSet.getString("PERSON_ADDRESS_PIN_CODE"));

            String tempppppp = resultSet.getString("STATUS");
            if (tempppppp.equalsIgnoreCase("Active")) {
                status.selectToggle(radioButton_active);
            } else {
                status.selectToggle(radioButton_Deactive);
            }

        }

        business_Name.setEditable(false);
        business_Registraction_No.setEditable(false);
        business_Pan_No.setEditable(false);
        business_Gst_No.setEditable(false);
        business_Email.setEditable(false);
        business_Phone_No.setEditable(false);
        business_Address.setEditable(false);
        business_Address_Pin_Code.setEditable(false);
        business_Address_State.getSelectionModel().select("Maharashtra");
        business_Address_State.setDisable(true);
        business_Address_City.setEditable(false);
        bank_Name.setEditable(false);
        Branch.setEditable(false);
        account_No.setEditable(false);
        ifsc_No.setEditable(false);
        contact_Person.setEditable(false);
        person_Email.setEditable(false);
        person_Mobile_No.setEditable(false);
        person_Pan_No.setEditable(false);
        person_Gender_Female.setDisable(true);
        person_Gender_Male.setDisable(true);

        person_Address.setEditable(false);
        person_Address_City.setEditable(false);
        person_Address_State.getSelectionModel().select("Maharashtra");
        person_Address_State.setDisable(true);
        person_Address_Pin_Code.setEditable(false);

        radioButton_Deactive.setDisable(true);
        radioButton_active.setDisable(true);
        ButtonDelete.setDisable(true);
        ButtonEdit.setDisable(false);
        ButtonUpdate.setDisable(true);

    }

    public void combobox() {

        String state[] = {"Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chhattisgarh", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala ", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal", "Andaman and Nicobar Islands", "Chandigarh", "Dadra and Nagar Haveli", "Daman and Diu ", "Delhi", "Lakshadweep", "Pondicherry"};

        business_Address_State.getItems().removeAll(business_Address_State.getItems());
        business_Address_State.getItems().addAll(state);
        business_Address_State.getSelectionModel().select("Maharashtra");

        //--------------------------------------------------------------------------------------------------------------------     
        person_Address_State.getItems().removeAll(person_Address_State.getItems());
        person_Address_State.getItems().addAll(state);
        person_Address_State.getSelectionModel().select("Maharashtra");

    }

}
