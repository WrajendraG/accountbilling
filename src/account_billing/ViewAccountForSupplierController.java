package account_billing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.osiersystems.pojos.ViewSupplierAccount;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import requriedDatabaseConnection.DatabaseConnection;

/**
 * FXML Controller class
 *
 * @author G1 NOTEBOOK
 */
public class ViewAccountForSupplierController implements Initializable {

    @FXML
    private TableView<ViewSupplierAccount> table_viewAccount;
    @FXML
    private TableColumn<ViewSupplierAccount, Integer> column_Srno;
    int globle_column_Srno = 0;
    @FXML
    private TableColumn<ViewSupplierAccount, String> column_BusinessName;
    String globle_column_BusinessName;
    @FXML
    private TableColumn<ViewSupplierAccount, String> column_CountactPerson;
    String globle_column_CountactPerson;
    @FXML
    private TableColumn<ViewSupplierAccount, Integer> column_MobileNumber;
    long globle_column_MobileNumber;
    @FXML
    private TableColumn<ViewSupplierAccount, String> column_EmailID;
    String globle_column_EmailID;
    @FXML
    private TableColumn<ViewSupplierAccount, String> column_City;
    String globle_column_City;
    @FXML
    private TableColumn<ViewSupplierAccount, String> column_Status;
    String globle_column_Status;
    @FXML
    private TableColumn<ViewSupplierAccount, Button> column_Action;
    Label globle_column_Action;
    @FXML
    private TextField text_BusinessName;
    @FXML
    private TextField text_CoutactPerson;
    @FXML
    private TextField text_City;

    ;

    Stage stage;
    Parent p;

    String globle_Sql;
    int check = 0;

    Alert alert = new Alert(Alert.AlertType.WARNING);

    @FXML
    private RadioButton radio_button_All;
    @FXML
    private ToggleGroup RadioButton;
    @FXML
    private RadioButton radio_button_Active;
    @FXML
    private RadioButton radio_button_DeActive;
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    Statement stm = null;
    int Globalid = 0;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        connection = DatabaseConnection.getConnection();

        column_Srno.setCellValueFactory(new PropertyValueFactory<>("SrNo"));
        column_Srno.setStyle("-fx-alignment: CENTER");

        column_BusinessName.setCellValueFactory(new PropertyValueFactory<>("BusinessName"));
        column_BusinessName.setStyle("-fx-alignment: LEFT");

        column_MobileNumber.setCellValueFactory(new PropertyValueFactory<>("MobileNumber"));
        column_MobileNumber.setStyle("-fx-alignment: CENTER");

        column_EmailID.setCellValueFactory(new PropertyValueFactory<>("EmailID"));
        column_EmailID.setStyle("-fx-alignment: LEFT");

        column_City.setCellValueFactory(new PropertyValueFactory<>("City"));
        column_City.setStyle("-fx-alignment: LEFT");

        column_CountactPerson.setCellValueFactory(new PropertyValueFactory<>("CountactPerson"));
        column_CountactPerson.setStyle("-fx-alignment: LEFT");

        column_Status.setCellValueFactory(new PropertyValueFactory<>("Status"));
        column_Status.setStyle("-fx-alignment: LEFT");

        column_Action.setCellValueFactory(new PropertyValueFactory<>("Action"));
        column_Action.setStyle("-fx-alignment: CENTER");

        try {
            bindItems1();
            bindItems2();
            bindItems3();
        } catch (SQLException ex) {
            Logger.getLogger(ViewCreatedAccountController.class.getName()).log(Level.SEVERE, null, ex);
        }

        // TODO
        text_BusinessName.setText("");
        text_CoutactPerson.setText("");
        text_City.setText("");

        try {

            globle_Sql = "select * from SUPPLIERACCOUNT ";
            loadDataFromDatabaseToTable();
        } catch (SQLException ex) {
            Logger.getLogger(ViewCreatedAccountController.class.getName()).log(Level.SEVERE, null, ex);
        }

        setCellValueFromToTextField();
    }

    public void bindItems1() throws SQLException {
        try {
            text_BusinessName.setText(null);
            ObservableList categoryList1 = FXCollections.observableArrayList();

            categoryList1.removeAll(categoryList1);

        connection = DatabaseConnection.getConnection();
            preparedStatement = connection.prepareStatement(" select BUSINESS_NAME from SUPPLIERACCOUNT");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                categoryList1.add(resultSet.getString("BUSINESS_NAME"));

            }
            ArrayList observableList1 = new ArrayList<String>(new HashSet<String>(categoryList1));

            org.controlsfx.control.textfield.TextFields.bindAutoCompletion(text_BusinessName, observableList1);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

    }

    public void bindItems2() throws SQLException {
        try {
            text_BusinessName.setText(null);
            ObservableList categoryList2 = FXCollections.observableArrayList();

            categoryList2.removeAll(categoryList2);

            connection = DatabaseConnection.getConnection();
            preparedStatement = connection.prepareStatement(" select CONTACT_PERSON from SUPPLIERACCOUNT ");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                categoryList2.add(resultSet.getString("CONTACT_PERSON"));

            }
            ArrayList observableList2 = new ArrayList<String>(new HashSet<String>(categoryList2));

            org.controlsfx.control.textfield.TextFields.bindAutoCompletion(text_CoutactPerson, observableList2);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }

        }

    }

    public void bindItems3() throws SQLException {
        try {
            text_BusinessName.setText(null);
            ObservableList categoryList3 = FXCollections.observableArrayList();

            categoryList3.removeAll(categoryList3);

            connection = DatabaseConnection.getConnection();
            preparedStatement = connection.prepareStatement("select PERSON_ADDRESS_CITY from SUPPLIERACCOUNT ");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                categoryList3.add(resultSet.getString("PERSON_ADDRESS_CITY"));

            }
            ArrayList observableList3 = new ArrayList<String>(new HashSet<String>(categoryList3));
            org.controlsfx.control.textfield.TextFields.bindAutoCompletion(text_City, observableList3);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

    }

    public void loadDataFromDatabaseToTable() throws SQLException {
                  System.out.println("calle");
        try {
            connection = DatabaseConnection.getConnection();
            ObservableList<ViewSupplierAccount> list = FXCollections.observableArrayList();
            stm = connection.createStatement();
            resultSet = stm.executeQuery(globle_Sql);
            int i = 1;
            while (resultSet.next()) {
                check = 1;
                Globalid = resultSet.getInt("id");
                globle_column_BusinessName = resultSet.getString("BUSINESS_NAME");
                globle_column_MobileNumber = Long.parseLong(resultSet.getString("PERSON_MOBILE_NO"));
                globle_column_EmailID = resultSet.getString("PERSON_EMAIL");
                globle_column_City = resultSet.getString("PERSON_ADDRESS_CITY");
                globle_column_CountactPerson = resultSet.getString("CONTACT_PERSON");
                globle_column_Status = resultSet.getString("STATUS");
                globle_column_Action = new Label("Click Here");

                list.add(new ViewSupplierAccount(Globalid, i, globle_column_BusinessName, globle_column_MobileNumber, globle_column_EmailID, globle_column_City, globle_column_CountactPerson, globle_column_Status, globle_column_Action));
                i++;

            }

            table_viewAccount.setItems(list);

            text_BusinessName.setText("");
            text_CoutactPerson.setText("");
            text_City.setText("");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (connection != null) {
                connection.close();
            }

        }

    }

    @FXML
    private void SearchAction(ActionEvent event) throws SQLException {
        if (!(text_BusinessName.getText().isEmpty()) && !(text_CoutactPerson.getText().isEmpty()) && !(text_City.getText().isEmpty())) {
            globle_Sql = "select * from SUPPLIERACCOUNT where BUSINESS_NAME ='" + text_BusinessName.getText() + "' AND CONTACT_PERSON='" + text_CoutactPerson.getText() + "' AND PERSON_ADDRESS_CITY='" + text_City.getText() + "' ";

            if (radio_button_Active.isSelected()) {

                globle_Sql = globle_Sql + " AND status = 'Active'";

            } else if (radio_button_DeActive.isSelected()) {

                globle_Sql = globle_Sql + "  AND status = 'Deactive' ";

            }

            loadDataFromDatabaseToTable();

            if (check == 0) {
                alert.setTitle("Validate");
                alert.setHeaderText(null);
                alert.setContentText("Record Not Found.....");
                alert.showAndWait();

            }
            check = 0;     /// Reset  check for next filter...

        } else if ((!(text_BusinessName.getText().isEmpty()) && !(text_CoutactPerson.getText().isEmpty())) || (!(text_BusinessName.getText().isEmpty()) && !(text_City.getText().isEmpty())) || (!(text_CoutactPerson.getText().isEmpty()) && !(text_City.getText().isEmpty()))) {

            if ((!(text_BusinessName.getText().isEmpty()) && !(text_CoutactPerson.getText().isEmpty()))) {

                globle_Sql = "select *from SUPPLIERACCOUNT where BUSINESS_NAME='" + text_BusinessName.getText() + "' AND CONTACT_PERSON='" + text_CoutactPerson.getText() + "'";

                if (radio_button_Active.isSelected()) {

                    globle_Sql = globle_Sql + " AND status = 'Active'";

                } else if (radio_button_DeActive.isSelected()) {

                    globle_Sql = globle_Sql + "  AND status = 'Deactive' ";

                }

                loadDataFromDatabaseToTable();
                if (check == 0) {
                    alert.setTitle("Validate");
                    alert.setHeaderText(null);
                    alert.setContentText("Record Not Found.....");
                    alert.showAndWait();

                }
                check = 0;     /// Reset  check for next filter...

            } else if ((!(text_BusinessName.getText().isEmpty()) && !(text_City.getText().isEmpty()))) {

                globle_Sql = "select * from SUPPLIERACCOUNT where BUSINESS_NAME='" + text_BusinessName.getText() + "' AND PERSON_ADDRESS_CITY='" + text_City.getText() + "'";

                if (radio_button_Active.isSelected()) {

                    globle_Sql = globle_Sql + " AND status = 'Active'";

                } else if (radio_button_DeActive.isSelected()) {

                    globle_Sql = globle_Sql + "  AND status = 'Deactive' ";

                }

                loadDataFromDatabaseToTable();

                if (check == 0) {
                    alert.setTitle("Validate");
                    alert.setHeaderText(null);
                    alert.setContentText("Record Not Found.....");
                    alert.showAndWait();

                }
                check = 0;     /// Reset  check for next filter...

            } else if ((!(text_CoutactPerson.getText().isEmpty()) && !(text_City.getText().isEmpty()))) {

                globle_Sql = "select *from SUPPLIERACCOUNT where CONTACT_PERSON='" + text_CoutactPerson.getText() + "' AND PERSON_ADDRESS_CITY='" + text_City.getText() + "'";

                if (radio_button_Active.isSelected()) {

                    globle_Sql = globle_Sql + " AND status = 'Active'";

                } else if (radio_button_DeActive.isSelected()) {

                    globle_Sql = globle_Sql + "  AND status = 'Deactive' ";

                }

                loadDataFromDatabaseToTable();

                if (check == 0) {
                    alert.setTitle("Validate");
                    alert.setHeaderText(null);
                    alert.setContentText("Record Not Found.....");
                    alert.showAndWait();

                }
                check = 0;     /// Reset  check for next filter...

            }
        } else if (!(text_BusinessName.getText().isEmpty()) || !(text_CoutactPerson.getText().isEmpty()) || !(text_City.getText().isEmpty())) {

            if (!(text_BusinessName.getText().isEmpty())) {
                globle_Sql = "select * from SUPPLIERACCOUNT where BUSINESS_NAME='" + text_BusinessName.getText() + "' ";

                if (radio_button_Active.isSelected()) {

                    globle_Sql = globle_Sql + " AND status = 'Active'";

                } else if (radio_button_DeActive.isSelected()) {

                    globle_Sql = globle_Sql + "  AND status = 'Deactive' ";

                }

                loadDataFromDatabaseToTable();

                if (check == 0) {
                    alert.setTitle("Validate");
                    alert.setHeaderText(null);
                    alert.setContentText("Record Not Found.....");
                    alert.showAndWait();

                }
                check = 0;     /// Reset  check for next filter...

            } else if (!(text_CoutactPerson.getText().isEmpty())) {
                globle_Sql = "select * from SUPPLIERACCOUNT where CONTACT_PERSON='" + text_CoutactPerson.getText() + "' ";

                if (radio_button_Active.isSelected()) {

                    globle_Sql = globle_Sql + " AND status = 'Active'";

                } else if (radio_button_DeActive.isSelected()) {

                    globle_Sql = globle_Sql + "  AND status = 'Deactive' ";

                }
                loadDataFromDatabaseToTable();

                if (check == 0) {
                    alert.setTitle("Validate");
                    alert.setHeaderText(null);
                    alert.setContentText("Record Not Found.....");
                    alert.showAndWait();

                }
                check = 0;     /// Reset  check for next filter...

            } else if (!(text_City.getText().isEmpty())) {
                globle_Sql = "select * from SUPPLIERACCOUNT where PERSON_ADDRESS_CITY='" + text_City.getText() + "' ";

                if (radio_button_Active.isSelected()) {

                    globle_Sql = globle_Sql + " AND status = 'Active'";

                } else if (radio_button_DeActive.isSelected()) {

                    globle_Sql = globle_Sql + "  AND status = 'Deactive' ";

                }

                loadDataFromDatabaseToTable();

                if (check == 0) {
                    alert.setTitle("Validate");
                    alert.setHeaderText(null);
                    alert.setContentText("Record Not Found.....");
                    alert.showAndWait();

                }
                check = 0;     /// Reset  check for next filter...

            }

        } else {

            alert.setTitle("Validate");
            alert.setHeaderText(null);
            alert.setContentText("Some Thing Wrong Record Not Found.....");
            alert.showAndWait();
        }

    }

    @FXML
    private void resetTextFields(ActionEvent event) throws SQLException {
        text_BusinessName.setText(null);
        text_CoutactPerson.setText(null);
        text_City.setText(null);

        globle_Sql = "select * from SUPPLIERACCOUNT ";
        loadDataFromDatabaseToTable();
    }

    @FXML
    private void radioBoxSearchAll(ActionEvent event) throws SQLException {
        globle_Sql = "select * from SUPPLIERACCOUNT ";
        loadDataFromDatabaseToTable();
    }

    @FXML
    private void radioBoxSearchActive(ActionEvent event) throws SQLException {
        globle_Sql = "select * from SUPPLIERACCOUNT where  status = 'Active'";
        loadDataFromDatabaseToTable();
    }

    @FXML
    private void radioBoxSearchDeactive(ActionEvent event) throws SQLException {
        globle_Sql = "select * from SUPPLIERACCOUNT where  status = 'Deactive' ";
        loadDataFromDatabaseToTable();
    }

    private void setCellValueFromToTextField() {
        System.out.println("am on table cleck 2");

        table_viewAccount.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                System.out.println("am on mounse cleck");
                ViewSupplierAccount list = table_viewAccount.getItems().get(table_viewAccount.getSelectionModel().getSelectedIndex());
                int id = list.getID();
                System.out.println("idforupdate----" + id);
                String temp = list.getBusinessName();

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("UpdateSupplierAccount.fxml"));
                try {
                    loader.load();
                } catch (Exception e) {
                }
/////////
                UpdateSupplierAccountController display = loader.getController();
                display.setTextforViewAccount(id);
                p = loader.getRoot();
                stage = new Stage();
                stage.resizableProperty().setValue(Boolean.FALSE);// disible
                stage.initStyle(StageStyle.UTILITY);// hide 
                stage.setScene(new Scene(p));
                stage.showAndWait();

                try {
                    loadDataFromDatabaseToTable();
                } catch (SQLException ex) {
                    Logger.getLogger(ViewCreatedAccountController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

    }

    @FXML
    private void setCellValueFromToTextField(MouseEvent event) {
        System.out.println("am on table cleck");
    }

}
