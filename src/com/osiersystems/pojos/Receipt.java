/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osiersystems.pojos;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**

  CREATE TABLE "SYSTEM"."RECEIPT_DETAILS" 
   (	"RECEIPT_NUMBER" NUMBER(*,0) NOT NULL ENABLE, 
	"RECEIPT_DATE" DATE, 
	"INVOICE_NUMBER" VARCHAR2(30 BYTE), 
	"CUSTOMER_NAME" VARCHAR2(50 BYTE), 
	"MOBILE_NUMBER" VARCHAR2(20 BYTE), 
	"NET_TOTAL_AMOUN" VARCHAR2(100 BYTE), 
	"PAID" VARCHAR2(100 BYTE), 
	"OUTSTANDING" VARCHAR2(100 BYTE), 
	"STATUS" VARCHAR2(100 BYTE), 
	 CONSTRAINT "RECEIPT_DETAILS_PK" PRIMARY KEY ("RECEIPT_NUMBER")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" ;
 

 */
public class Receipt {

    private final SimpleIntegerProperty Pojo_InvoiceSrNo; /// its means receipt sr no 
    private final SimpleIntegerProperty Pojo_ReceiptNumber;
    private final SimpleStringProperty Pojo_ReceiptDate;  // date
    private final SimpleStringProperty Pojo_InvoiceNumber; //integer
    private final SimpleStringProperty Pojo_PersonName;
    private final SimpleStringProperty Pojo_Contact; 
    private final SimpleStringProperty Pojo_Total_Amount;
    private final SimpleStringProperty Pojo_Paid_Amount;
    private final SimpleStringProperty Pojo_Outstanding_Amount;
    private final SimpleStringProperty Pojo_InvoiceStatus;

    public Receipt(Integer Pojo_InvoiceSrNo, Integer receiptno, String reciptDate, String Pojo_InvoiceNumber, String Pojo_PersonName, String Pojo_Contact, String Pojo_Total_Amount, String Pojo_Paid_Amount, String Pojo_Outstanding_Amount, String Pojo_InvoiceStatus) {
        this.Pojo_InvoiceSrNo = new SimpleIntegerProperty(Pojo_InvoiceSrNo);
        this.Pojo_ReceiptNumber = new SimpleIntegerProperty(receiptno);
        this.Pojo_ReceiptDate = new SimpleStringProperty(reciptDate);
        this.Pojo_InvoiceNumber = new SimpleStringProperty(Pojo_InvoiceNumber);
        this.Pojo_PersonName = new SimpleStringProperty(Pojo_PersonName);
        this.Pojo_Contact = new SimpleStringProperty(Pojo_Contact);
        this.Pojo_Total_Amount = new SimpleStringProperty(Pojo_Total_Amount);
        this.Pojo_Paid_Amount = new SimpleStringProperty(Pojo_Paid_Amount);
        this.Pojo_Outstanding_Amount = new SimpleStringProperty(Pojo_Outstanding_Amount);
        this.Pojo_InvoiceStatus = new SimpleStringProperty(Pojo_InvoiceStatus);
    }
    //---------------------------------------------

    public int getPojo_InvoiceSrNo() {
        return Pojo_InvoiceSrNo.get();
    }

    public void setPojo_InvoiceSrNo(Integer psrno) {
        Pojo_InvoiceSrNo.set(psrno);
    }
    //---------------------------------------------

    public int getPojo_ReceiptNumber() {
        return Pojo_ReceiptNumber.get();
    }

    public void setPojo_ReceiptNumber(Integer receiptno) {
        Pojo_ReceiptNumber.set(receiptno);
    }

    //------------------------------------------------------
    public String getPojo_InvoiceNumber() {
        return Pojo_InvoiceNumber.get();
    }

    public void setPojo_InvoiceNumber(String p) {
        Pojo_InvoiceNumber.set(p);
    }

//------------------------------------------------------
    public String getPojo_ReceiptDate() {
        return Pojo_ReceiptDate.get();
    }

    public void setPojo_ReceiptDate(String p) {
        Pojo_ReceiptDate.set(p);
    }

    //------------------------------------------------------
    public String getPojo_PersonName() {
        return Pojo_PersonName.get();
    }

    public void setPojo_PersonName(String p) {
        Pojo_PersonName.set(p);
    }

//------------------------------------------------------
    public String getPojo_Contact() {
        return Pojo_Contact.get();
    }

    public void setPojo_Contact(String p) {
        Pojo_Contact.set(p);
    }

    //------------------------------------------------------
    public String getPojo_Total_Amount() {
        return Pojo_Total_Amount.get();
    }

    public void setPojo_Total_Amount(String p) {
        Pojo_Total_Amount.set(p);
    }
    //------------------------------------------------------

    public String getPojo_InvoiceStatus() {
        return Pojo_InvoiceStatus.get();
    }

    public void setPojo_InvoiceStatus(String p) {
        Pojo_InvoiceStatus.set(p);
    }
    //------------------------------------------------------

    public String getPojo_Paid_Amount() {
        return Pojo_Paid_Amount.get();
    }

    public void setPojo_Paid_Amount(String p) {
        Pojo_Paid_Amount.set(p);
    }
    //------------------------------------------------------

    public String getPojo_Outstanding_Amount() {
        return Pojo_Outstanding_Amount.get();
    }

    public void setPojo_Outstanding_Amount(String p) {
        Pojo_Outstanding_Amount.set(p);
    }

}
