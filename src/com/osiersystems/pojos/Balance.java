/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osiersystems.pojos;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author user
 */
public class Balance {

    private final SimpleIntegerProperty srno;
    private final SimpleStringProperty name;
    private final SimpleStringProperty stock;
    private final SimpleStringProperty price;
 
    public Balance(Integer psrno,String pname, String pstock, String prate)
    {
        this.srno= new SimpleIntegerProperty(psrno);
        this.name = new SimpleStringProperty(pname);
        this.stock = new SimpleStringProperty(pstock);
        this.price = new SimpleStringProperty(prate);
    }
 //---------------------------------------------
    public int getSrno()
    {
       return srno.get();
    }
    public void setSrno(Integer psrno)
    {
                srno.set(psrno);
    }
    
    
    
    //-----------------------------------
    public String getName() 
    {
        return name.get();
    }
    public void setName(String pname) 
    {
        name.set(pname);
    }
        
    public String getStock() 
    {
        return stock.get();
    }
    public void setStock(String pstock) 
    {
        stock.set(pstock);
    }
    
    public String getPrice() 
    {
        return price.get();
    }
    public void setPrice(String prate) 
    {
       price.set(prate);
    }
}
