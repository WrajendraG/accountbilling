/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osiersystems.pojos;

import java.sql.Date;

/**
 *
 * @author G1 NOTEBOOK
 */
public class ViewSingleRecordOfPurchaser {
        private int Pojo_SrNo;
    //private String Pojo_Name_of_Supplier;
    private String Pojo_Invoice_No;
    private String Pojo_Date;
    private String Pojo_Amount;
    private String Pojo_Paid;
    private String Pojo_Outstanding;
    private String Pojo_Status;

    public ViewSingleRecordOfPurchaser(int Pojo_SrNo, String Pojo_Invoice_No, String Pojo_Date, String Pojo_Amount, String Pojo_Paid, String Pojo_Outstanding, String Pojo_Status) {
        this.Pojo_SrNo = Pojo_SrNo;
        this.Pojo_Invoice_No = Pojo_Invoice_No;
        this.Pojo_Date = Pojo_Date;
        this.Pojo_Amount = Pojo_Amount;
        this.Pojo_Paid = Pojo_Paid;
        this.Pojo_Outstanding = Pojo_Outstanding;
        this.Pojo_Status = Pojo_Status;
    }

    public int getPojo_SrNo() {
        return Pojo_SrNo;
    }

    public void setPojo_SrNo(int Pojo_SrNo) {
        this.Pojo_SrNo = Pojo_SrNo;
    }

    public String getPojo_Invoice_No() {
        return Pojo_Invoice_No;
    }

    public void setPojo_Invoice_No(String Pojo_Invoice_No) {
        this.Pojo_Invoice_No = Pojo_Invoice_No;
    }

    public String getPojo_Date() {
        return Pojo_Date;
    }

    public void setPojo_Date(String Pojo_Date) {
        this.Pojo_Date = Pojo_Date;
    }

    public String getPojo_Amount() {
        return Pojo_Amount;
    }

    public void setPojo_Amount(String Pojo_Amount) {
        this.Pojo_Amount = Pojo_Amount;
    }

    public String getPojo_Paid() {
        return Pojo_Paid;
    }

    public void setPojo_Paid(String Pojo_Paid) {
        this.Pojo_Paid = Pojo_Paid;
    }

    public String getPojo_Outstanding() {
        return Pojo_Outstanding;
    }

    public void setPojo_Outstanding(String Pojo_Outstanding) {
        this.Pojo_Outstanding = Pojo_Outstanding;
    }

    public String getPojo_Status() {
        return Pojo_Status;
    }

    public void setPojo_Status(String Pojo_Status) {
        this.Pojo_Status = Pojo_Status;
    }

    
}
