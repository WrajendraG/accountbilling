/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osiersystems.pojos;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Niteen
 */
public class Edit_Delete_Items {

    private final SimpleIntegerProperty Table_Items_SrNo;
    private final SimpleStringProperty Table_Items_Product;
    private final SimpleStringProperty Table_Items_Category;
    private final SimpleStringProperty Table_Items_Price;
    private final SimpleStringProperty Table_Items_GST_Rate;
    private final SimpleStringProperty Table_Items_Percentage;

    public Edit_Delete_Items(Integer Table_Items_SrNo, String Table_Items_Product, String Table_Items_Category, String Table_Items_Price, String Table_Items_GST_Rate, String Table_Items_Percentage) {
        this.Table_Items_SrNo = new SimpleIntegerProperty(Table_Items_SrNo);
        this.Table_Items_Product = new SimpleStringProperty(Table_Items_Product);
        this.Table_Items_Category = new SimpleStringProperty(Table_Items_Category);
        this.Table_Items_Price = new SimpleStringProperty(Table_Items_Price);
        this.Table_Items_GST_Rate = new SimpleStringProperty(Table_Items_GST_Rate);
        this.Table_Items_Percentage = new SimpleStringProperty(Table_Items_Percentage);
    }

    //---------------------------------------------
    public int getTable_Items_SrNo() {
        return Table_Items_SrNo.get();
    }

    public void setSrno(Integer psrno) {
        Table_Items_SrNo.set(psrno);
    }

//------------------------------------------------------
    public String getTable_Items_Product() {
        return Table_Items_Product.get();
    }

    public void setTable_Items_Product(String p) {
        Table_Items_Product.set(p);
    }

    //------------------------------------------------------
    public String getTable_Items_Category() {
        return Table_Items_Category.get();
    }

    public void setTable_Items_Category(String p) {
        Table_Items_Category.set(p);
    }

//------------------------------------------------------
    public String getTable_Items_Price() {
        return Table_Items_Price.get();
    }

    public void setTable_Items_Price(String p) {
        Table_Items_Price.set(p);
    }

    //------------------------------------------------------
    public String getTable_Items_GST_Rate() {
        return Table_Items_GST_Rate.get();
    }

    public void setTable_Items_GST_Rate(String p) {
        Table_Items_GST_Rate.set(p);
    }
    //------------------------------------------------------

    public String getTable_Items_Percentage() {
        return Table_Items_Percentage.get();
    }

    public void setTable_Items_Percentage(String p) {
        Table_Items_Percentage.set(p);
    }

}
