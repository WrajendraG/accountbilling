/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.InvoiceModification
 */
package com.osiersystems.pojos;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Niteen
 */
public class InvoiceModification {

    private final SimpleIntegerProperty Pojo_InvoiceSrNo;
    private final SimpleStringProperty Pojo_InvoiceNumber; //integer
    private final SimpleStringProperty Pojo_PersonName;
    private final SimpleStringProperty Pojo_DateOfInvoice;  // date
    private final SimpleStringProperty Pojo_Total_Amount;
    private final SimpleStringProperty Pojo_Paid_Amount;
    private final SimpleStringProperty Pojo_Outstanding_Amount;

    private final SimpleStringProperty Pojo_InvoiceStatus;

    public InvoiceModification(Integer Pojo_InvoiceSrNo, String Pojo_InvoiceNumber, String Pojo_PersonName, String Pojo_DateOfInvoice, String Pojo_Total_Amount, String Pojo_Paid_Amount, String Pojo_Outstanding_Amount, String Pojo_InvoiceStatus) {
        this.Pojo_InvoiceSrNo = new SimpleIntegerProperty(Pojo_InvoiceSrNo);
        this.Pojo_InvoiceNumber = new SimpleStringProperty(Pojo_InvoiceNumber);
        this.Pojo_PersonName = new SimpleStringProperty(Pojo_PersonName);
        this.Pojo_DateOfInvoice = new SimpleStringProperty(Pojo_DateOfInvoice);
        this.Pojo_Total_Amount = new SimpleStringProperty(Pojo_Total_Amount);
        this.Pojo_Paid_Amount = new SimpleStringProperty(Pojo_Paid_Amount);
        this.Pojo_Outstanding_Amount = new SimpleStringProperty(Pojo_Outstanding_Amount);
        this.Pojo_InvoiceStatus = new SimpleStringProperty(Pojo_InvoiceStatus);
    }

    //---------------------------------------------
    public int getPojo_InvoiceSrNo() {
        return Pojo_InvoiceSrNo.get();
    }

    public void setPojo_InvoiceSrNo(Integer psrno) {
        Pojo_InvoiceSrNo.set(psrno);
    }

//------------------------------------------------------
    public String getPojo_InvoiceNumber() {
        return Pojo_InvoiceNumber.get();
    }

    public void setPojo_InvoiceNumber(String p) {
        Pojo_InvoiceNumber.set(p);
    }

    //------------------------------------------------------
    public String getPojo_PersonName() {
        return Pojo_PersonName.get();
    }

    public void setPojo_PersonName(String p) {
        Pojo_PersonName.set(p);
    }

//------------------------------------------------------
    public String getPojo_DateOfInvoice() {
        return Pojo_DateOfInvoice.get();
    }

    public void setPojo_DateOfInvoice(String p) {
        Pojo_DateOfInvoice.set(p);
    }

    //------------------------------------------------------
    public String getPojo_Total_Amount() {
        return Pojo_Total_Amount.get();
    }

    public void setPojo_Total_Amount(String p) {
        Pojo_Total_Amount.set(p);
    }
    //------------------------------------------------------

    public String getPojo_InvoiceStatus() {
        return Pojo_InvoiceStatus.get();
    }

    public void setPojo_InvoiceStatus(String p) {
        Pojo_InvoiceStatus.set(p);
    }
        //------------------------------------------------------

    public String getPojo_Paid_Amount() {
        return Pojo_Paid_Amount.get();
    }

    public void setPojo_Paid_Amount(String p) {
        Pojo_Paid_Amount.set(p);
    }
        //------------------------------------------------------

    public String getPojo_Outstanding_Amount() {
        return Pojo_Outstanding_Amount.get();
    }

    public void setPojo_Outstanding_Amount(String p) {
        Pojo_Outstanding_Amount.set(p);
    }

}
