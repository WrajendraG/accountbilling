/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osiersystems.pojos;

import java.sql.Time;
import java.util.Date;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Niteen
 */
public class ViewPurchase {

    private int Pojo_SrNo;
    private String Pojo_Name_of_Supplier;
    private String Pojo_Invoice_No;
    private String Pojo_Date;
    private String Pojo_Amount;
    private String Pojo_Paid;
    private String Pojo_Outstanding;
    private String Pojo_Status;

    public ViewPurchase(int Pojo_SrNo, String Pojo_Name_of_Supplier, String Pojo_Invoice_No, String Pojo_Date, String Pojo_Amount, String Pojo_Paid, String Pojo_Outstanding, String Pojo_Status) {
        this.Pojo_SrNo = Pojo_SrNo;
        this.Pojo_Name_of_Supplier = Pojo_Name_of_Supplier;
        this.Pojo_Invoice_No = Pojo_Invoice_No;
        this.Pojo_Date = Pojo_Date;
        this.Pojo_Amount = Pojo_Amount;
        this.Pojo_Paid = Pojo_Paid;
        this.Pojo_Outstanding = Pojo_Outstanding;
        this.Pojo_Status = Pojo_Status;
    }

    /**
     * @return the Pojo_SrNo
     */
    public int getPojo_SrNo() {
        return Pojo_SrNo;
    }

    /**
     * @param Pojo_SrNo the Pojo_SrNo to set
     */
    public void setPojo_SrNo(int Pojo_SrNo) {
        this.Pojo_SrNo = Pojo_SrNo;
    }

    /**
     * @return the Pojo_Name_of_Supplier
     */
    public String getPojo_Name_of_Supplier() {
        return Pojo_Name_of_Supplier;
    }

    /**
     * @param Pojo_Name_of_Supplier the Pojo_Name_of_Supplier to set
     */
    public void setPojo_Name_of_Supplier(String Pojo_Name_of_Supplier) {
        this.Pojo_Name_of_Supplier = Pojo_Name_of_Supplier;
    }

    /**
     * @return the Pojo_Invoice_No
     */
    public String getPojo_Invoice_No() {
        return Pojo_Invoice_No;
    }

    /**
     * @param Pojo_Invoice_No the Pojo_Invoice_No to set
     */
    public void setPojo_Invoice_No(String Pojo_Invoice_No) {
        this.Pojo_Invoice_No = Pojo_Invoice_No;
    }

    /**
     * @return the Pojo_Date
     */
    public String getPojo_Date() {
        return Pojo_Date;
    }

    /**
     * @param Pojo_Date the Pojo_Date to set
     */
    public void setPojo_Date(String Pojo_Date) {
        this.Pojo_Date = Pojo_Date;
    }

    /**
     * @return the Pojo_Amount
     */
    public String getPojo_Amount() {
        return Pojo_Amount;
    }

    /**
     * @param Pojo_Amount the Pojo_Amount to set
     */
    public void setPojo_Amount(String Pojo_Amount) {
        this.Pojo_Amount = Pojo_Amount;
    }

    /**
     * @return the Pojo_Paid
     */
    public String getPojo_Paid() {
        return Pojo_Paid;
    }

    /**
     * @param Pojo_Paid the Pojo_Paid to set
     */
    public void setPojo_Paid(String Pojo_Paid) {
        this.Pojo_Paid = Pojo_Paid;
    }

    /**
     * @return the Pojo_Outstanding
     */
    public String getPojo_Outstanding() {
        return Pojo_Outstanding;
    }

    /**
     * @param Pojo_Outstanding the Pojo_Outstanding to set
     */
    public void setPojo_Outstanding(String Pojo_Outstanding) {
        this.Pojo_Outstanding = Pojo_Outstanding;
    }

    /**
     * @return the Pojo_Status
     */
    public String getPojo_Status() {
        return Pojo_Status;
    }

    /**
     * @param Pojo_Status the Pojo_Status to set
     */
    public void setPojo_Status(String Pojo_Status) {
        this.Pojo_Status = Pojo_Status;
    }

    
}
