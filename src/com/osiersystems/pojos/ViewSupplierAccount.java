package com.osiersystems.pojos;

/**
 *
 * @author Rahul Jadhav
 */
import javafx.scene.control.Label;

public class ViewSupplierAccount {

    private Integer ID;
    private Integer SrNo;
    private String BusinessName;
    private long MobileNumber;
    private String EmailID;
    private String City;
    private String CountactPerson;
    private String Status;

    private Label Action;

    public ViewSupplierAccount(Integer ID, Integer SrNo, String BusinessName, long MobileNumber, String EmailID, String City, String CountactPerson, String Status, Label Action) {
        this.ID = ID;
        this.SrNo = SrNo;
        this.BusinessName = BusinessName;
        this.MobileNumber = MobileNumber;
        this.EmailID = EmailID;
        this.City = City;
        this.CountactPerson = CountactPerson;
        this.Status = Status;
        this.Action = Action;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getStatus() {
        return Status;
    }

    public void setSrNo(Integer SrNo) {
        this.SrNo = SrNo;
    }

    public void setBusinessName(String BusinessName) {
        this.BusinessName = BusinessName;
    }

    public void setMobileNumber(long MobileNumber) {
        this.MobileNumber = MobileNumber;
    }

    public void setEmailID(String EmailID) {
        this.EmailID = EmailID;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public void setCountactPerson(String CountactPerson) {
        this.CountactPerson = CountactPerson;
    }

    public void setAction(Label Action) {
        this.Action = Action;
    }

    public Integer getSrNo() {
        return SrNo;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public long getMobileNumber() {
        return MobileNumber;
    }

    public String getEmailID() {
        return EmailID;
    }

    public String getCity() {
        return City;
    }

    public String getCountactPerson() {
        return CountactPerson;
    }

    public Label getAction() {
        return Action;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }
}
