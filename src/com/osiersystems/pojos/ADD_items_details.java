/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osiersystems.pojos;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author G1 NOTEBOOK
 */
public class ADD_items_details {

    private SimpleIntegerProperty SrNo;
    private SimpleStringProperty item_name;
    private SimpleStringProperty hsn_code;
    private SimpleStringProperty units;
    private SimpleFloatProperty rate;
    private SimpleIntegerProperty qty;
    private SimpleFloatProperty amount;
    private SimpleFloatProperty discount_rupess;
    private SimpleFloatProperty taxableValue;
    private SimpleFloatProperty cgst_per;
    private SimpleFloatProperty cgst_rupess;
    private SimpleFloatProperty sgst_per;
    private SimpleFloatProperty sgst_rupess;
    private SimpleFloatProperty total;
    private SimpleIntegerProperty stock;
    private SimpleIntegerProperty taxableCategory;
    private SimpleFloatProperty mrp;
    private SimpleFloatProperty saleprice;
//taxableCategory
//mrp
    //saleprice

    public ADD_items_details() {

    }

    public ADD_items_details(Integer pSrNo, String pitem_name, String phsn_code, String punits, Float prate, Integer pqty, Float pamount, Float pdiscount_rupess, Float ptaxableV, Float pcgst_per, Float pcgst_rupess, Float psgst_per, Float psgst_rupess, Float ptotal, Integer pstock, Integer ptaxableCategory, Float pmrp, Float psaleprice) {
        this.SrNo = new SimpleIntegerProperty(pSrNo);
        this.item_name = new SimpleStringProperty(pitem_name);
        this.hsn_code = new SimpleStringProperty(phsn_code);
        this.units = new SimpleStringProperty(punits);
        this.rate = new SimpleFloatProperty(prate);
        this.qty = new SimpleIntegerProperty(pqty);
        this.amount = new SimpleFloatProperty(pamount);
        this.discount_rupess = new SimpleFloatProperty(pdiscount_rupess);
        this.taxableValue = new SimpleFloatProperty(ptaxableV);
        this.cgst_per = new SimpleFloatProperty(pcgst_per);
        this.cgst_rupess = new SimpleFloatProperty(pcgst_rupess);
        this.sgst_per = new SimpleFloatProperty(psgst_per);
        this.sgst_rupess = new SimpleFloatProperty(psgst_rupess);
        this.total = new SimpleFloatProperty(ptotal);
        this.stock = new SimpleIntegerProperty(pstock);
        this.taxableCategory = new SimpleIntegerProperty(ptaxableCategory);
        this.mrp = new SimpleFloatProperty(pmrp);
        this.saleprice = new SimpleFloatProperty(psaleprice);
    }

    public int getSrNo() {
        return SrNo.get();
    }

    public void setSrNo(Integer pSrNo) {
        SrNo.set(pSrNo);
    }

    public String getItem_name() {
        return item_name.get();
    }

    public void setItem_name(String pitem_name) {
        item_name.set(pitem_name);
    }

    public String getHsn_code() {
        return hsn_code.get();
    }

    public void setHsn_code(String phsn_code) {
        hsn_code.set(phsn_code);
    }

    public String getUnits() {
        return units.get();
    }

    public void setUnits(String punits) {
        units.set(punits);
    }

    public float getRate() {
        return rate.get();
    }

    public void setRate(Float prate) {
        rate.set(prate);
    }

    public int getQty() {
        return qty.get();
    }

    public void setQty(Integer pqty) {
        qty.set(pqty);
    }

    public float getAmount() {
        return amount.get();
    }

    public void setAmount(Float pamount) {
        amount.set(pamount);
    }

    public Float getDiscount_rupess() {
        return discount_rupess.get();
    }

    public void setDiscount_rupess(Float pdiscount_rupess) {
        discount_rupess.set(pdiscount_rupess);
    }

    public float getTaxableValue() {
        return taxableValue.get();
    }

    public void setTaxableValue(Float ptaxableV) {
        taxableValue.set(ptaxableV);
    }

    public Float getCgst_per() {
        return cgst_per.get();
    }

    public void setCgst_per(Float pcgst_per) {
        cgst_per.set(pcgst_per);
    }

    public Float getCgst_rupess() {
        return cgst_rupess.get();
    }

    public void setCgst_rupess(Float pcgst_rupess) {
        cgst_rupess.set(pcgst_rupess);
    }

    public Float getSgst_per() {
        return sgst_per.get();
    }

    public void setSgst_per(Float psgst_per) {
        sgst_per.set(psgst_per);
    }

    public Float getSgst_rupess() {
        return sgst_rupess.get();
    }

    public void setSgst_rupess(Float psgst_rupess) {
        sgst_rupess.set(psgst_rupess);
    }

    public float getTotal() {
        return total.get();
    }

    public void setTotal(Float ptotal) {
        total.set(ptotal);
    }

    public int getStock() {
        return stock.get();
    }

    public void setStock(Integer pstock) {
        stock.set(pstock);
    }

    //----------------------------
    public int getTaxableCategory() {
        return taxableCategory.get();
    }

    public void setTaxableCategory(Integer ptaxableCategory) {
        taxableCategory.set(ptaxableCategory);
    }
//----------------------------------------------------------

    public float getMrp() {
        return mrp.get();
    }

    public void setMrp(Float pmrp) {
        mrp.set(pmrp);
    }
//--------------------------------

    public float getSaleprice() {
        return saleprice.get();
    }

    public void setSaleprice(Float psaleprice) {
        saleprice.set(psaleprice);
    }

}
