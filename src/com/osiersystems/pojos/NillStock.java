/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osiersystems.pojos;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Niteen
 */
public class NillStock {

    private SimpleIntegerProperty srno;
    private SimpleStringProperty item_Name;
      private SimpleStringProperty  Category;
    private SimpleIntegerProperty stock;
    private SimpleFloatProperty purchasing_Price;
    private SimpleIntegerProperty qty;
    private SimpleStringProperty unit;
    private SimpleFloatProperty total;

    public NillStock(int psrno, String pitem_name,String cat ,int pstock, float pPurchasing_price, int pQty, String punit, float ptotal) {

        this.srno = new SimpleIntegerProperty(psrno);
        this.item_Name = new SimpleStringProperty(pitem_name);
          this.Category = new SimpleStringProperty(cat);
        this.stock = new SimpleIntegerProperty(pstock);
        this.purchasing_Price = new SimpleFloatProperty(pPurchasing_price);
        this.qty = new SimpleIntegerProperty(pQty);
        this.unit = new SimpleStringProperty(punit);
        this.total = new SimpleFloatProperty(ptotal);
    }

    //------------------------------------------
    public void setUnit(String punit) {
        this.unit.set(punit);
    }

    public String getUnit() {
        return unit.get();
    }

    //---------------------------------------------
    public void setSrno(int srno) {
        this.srno.set(srno);
    }

    public int getSrno() {
        return srno.get();
    }

    //---------------------------------------------
    public void setItem_Name(String items_name) {
        this.item_Name.set(items_name);
    }

    public String getItem_Name() {
        return item_Name.get();
    }
    
        //---------------------------------------------
    public void setCategory(String cat) {
        this.Category.set(cat);
    }

    public String getCategory() {
        return Category.get();
    }

    //---------------------------------------------
    public void setPurchasing_Price(int purchasing_Price) {
        this.purchasing_Price.set(purchasing_Price);
    }

    public float getPurchasing_Price() {
        return purchasing_Price.get();
    }

    //---------------------------------------------
    public void setStock(int stock) {
        this.stock.set(stock);
    }

    public int getStock() {
        return stock.get();
    }
    //---------------------------------------------

    public void setQty(int qty) {
        this.qty.set(qty);
    }

    public int getQty() {
        return qty.get();
    }
    //---------------------------------------------

    public void setTotal(int total) {
        this.total.set(total);
    }

    public float getTotal() {
        return total.get();
    }

    //---------------------------------------------
}
