package com.osiersystems.pojos;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


public class GstReport {

    private SimpleIntegerProperty Pojo_SrNo;
    private SimpleStringProperty Pojo_InvoiceNumber;
    private SimpleStringProperty Pojo_BuyerName;
    private SimpleStringProperty Pojo_Date;
    private SimpleFloatProperty Pojo_TaxableValue;
    private SimpleFloatProperty Pojo_Cgst;
    private SimpleFloatProperty Pojo_Sgst;
    private SimpleFloatProperty Pojo_TotalTaxableAmount;

    public GstReport() {

    }

    public GstReport(Integer pSrNo, String pitem_name, String phsn_code, String punits, Float prate,  Float pamount, Float pdiscount_rupess, Float ptaxableV) {
        this.Pojo_SrNo = new SimpleIntegerProperty(pSrNo);
        this.Pojo_InvoiceNumber = new SimpleStringProperty(pitem_name);
        this.Pojo_BuyerName = new SimpleStringProperty(phsn_code);
        this.Pojo_Date = new SimpleStringProperty(punits);
        this.Pojo_TaxableValue = new SimpleFloatProperty(prate);
  
        this.Pojo_Cgst = new SimpleFloatProperty(pamount);
        this.Pojo_Sgst = new SimpleFloatProperty(pdiscount_rupess);
        this.Pojo_TotalTaxableAmount = new SimpleFloatProperty(ptaxableV);
   
    }

    public int getPojo_SrNo() {
        return Pojo_SrNo.get();
    }

    public void setPojo_SrNo(Integer pSrNo) {
        Pojo_SrNo.set(pSrNo);
    }

    public String getPojo_InvoiceNumber() {
        return Pojo_InvoiceNumber.get();
    }

    public void setPojo_InvoiceNumber(String pitem_name) {
        Pojo_InvoiceNumber.set(pitem_name);
    }

    public String getPojo_BuyerName() {
        return Pojo_BuyerName.get();
    }

    public void setPojo_BuyerName(String phsn_code) {
        Pojo_BuyerName.set(phsn_code);
    }

    public String getPojo_Date() {
        return Pojo_Date.get();
    }

    public void setPojo_Date(String punits) {
        Pojo_Date.set(punits);
    }

    public float getPojo_TaxableValue() {
        return Pojo_TaxableValue.get();
    }

    public void setPojo_TaxableValue(Float prate) {
        Pojo_TaxableValue.set(prate);
    }


    public float getPojo_Cgst() {
        return Pojo_Cgst.get();
    }

    public void setPojo_Cgst(Float pamount) {
        Pojo_Cgst.set(pamount);
    }

    public Float getPojo_Sgst() {
        return Pojo_Sgst.get();
    }

    public void setPojo_Sgst(Float pdiscount_rupess) {
        Pojo_Sgst.set(pdiscount_rupess);
    }

    public float getPojo_TotalTaxableAmount() {
        return Pojo_TotalTaxableAmount.get();
    }

    public void setPojo_TotalTaxableAmount(Float ptaxableV) {
        Pojo_TotalTaxableAmount.set(ptaxableV);
    }

}
