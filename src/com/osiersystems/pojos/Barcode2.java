/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osiersystems.pojos;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.image.ImageView;

/**
 *
 * @author Niteen
 */
public class Barcode2 {

    private final SimpleIntegerProperty srno;
    private final SimpleStringProperty itemName;
    private final SimpleStringProperty barcode;
    private ImageView image;
    private final SimpleIntegerProperty qty;

    public Barcode2(Integer psrno, String pname, String barcode, ImageView img, Integer qty) {

        this.srno = new SimpleIntegerProperty(psrno);
        this.itemName = new SimpleStringProperty(pname);
        this.barcode = new SimpleStringProperty(barcode);
        this.image = img;
        this.qty = new SimpleIntegerProperty(qty);

    }

    public void setImage(ImageView img) {
        this.image = img;
    }

    public ImageView getImage() {
        return image;
    }
    //---------------------------------------------

    public int getSrno() {
        return srno.get();
    }

    public void setSrno(Integer psrno) {
        srno.set(psrno);
    }

    //-----------------------------------
    public String getItemName() {
        return itemName.get();
    }

    public void setItemName(String pname) {
        itemName.set(pname);
    }

//---------------------------------------
    public String getBarcode() {

        return barcode.get();
    }

    public void setBarcode(String Barcode) {
        barcode.set(Barcode);

    }

    //------------------------------------------
    //---------------------------------------------
    public int getQty() {

        return qty.get();

    }

    public void setQty(Integer qty) {

        this.qty.set(qty);
    }
}
