package com.osiersystems.pojos;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Barcode {

    private final SimpleIntegerProperty srno;
    private final SimpleStringProperty itemName;
    private final SimpleStringProperty barcode;
    private final SimpleIntegerProperty qty;

    public Barcode(Integer psrno, String pname,String barcode,Integer qty) {
        
        this.srno = new SimpleIntegerProperty(psrno);
        this.itemName = new SimpleStringProperty(pname);
        this.barcode= new SimpleStringProperty(barcode);
        this.qty=new SimpleIntegerProperty(qty);

    }
    //---------------------------------------------

    public int getSrno() {
        return srno.get();
    }

    public void setSrno(Integer psrno) {
        srno.set(psrno);
    }

    //-----------------------------------
    public String getItemName() {
        return itemName.get();
    }

    public void setItemName(String pname) {
        itemName.set(pname);
    }
    
//---------------------------------------
public String getBarcode()
{

  return  barcode.get();
}
public void setBarcode(String Barcode)
{
  barcode.set(Barcode);

  }

//---------------------------------------------
public int getQty()
{
    
 return qty.get();

}
public void setQty(Integer qty)
{

this.qty.set(qty);
}

}
