/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account_billing;

import static account_billing.Barcode_Image.createImage;
import com.osiersystems.pojos.Barcode1;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import requriedDatabaseConnection.DatabaseConnection;

/**
 * FXML Controller class
 *
 * @author Niteen
 */
public class GenerateBarcode1Controller implements Initializable {

    @FXML
    private ComboBox<String> combo_Category;
    @FXML
    private TextField txt_ItemName;
    @FXML
    private TableColumn<Barcode1, Integer> column_srNo;
    @FXML
    private TableColumn<Barcode1, String> column_ItemName;
    @FXML
    private TableColumn<Barcode1, String> column_Barcode;
    @FXML
    private TableColumn<Barcode1, ImageView> table_ImageView;

    Connection connection;

    String CategaryName, ItemName, finalBarcode;

    String BarcodeStatus = "NON-GENERATED";

    Alert alert = new Alert(Alert.AlertType.WARNING);
    @FXML
    private TableView<Barcode1> table_Barcodes;
    ImageView BarcodeImage;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        connection = DatabaseConnection.getConnection();

        try {
            ClearTable();
            loadcatageryAgain();

            // TODO
        } catch (SQLException ex) {
            Logger.getLogger(GenerateBarcode1Controller.class.getName()).log(Level.SEVERE, null, ex);
        }

        column_srNo.setCellValueFactory(new PropertyValueFactory<>("srno"));
        column_srNo.setStyle("-fx-alignment: CENTER");

        column_ItemName.setCellValueFactory(new PropertyValueFactory<>("itemName"));
        column_ItemName.setStyle("-fx-alignment: LEFT");

        column_Barcode.setCellValueFactory(new PropertyValueFactory<>("barcode"));
        column_Barcode.setStyle("-fx-alignment: CENTER");

        table_ImageView.setCellValueFactory(new PropertyValueFactory<>("image"));
        table_ImageView.setStyle("-fx-alignment: CENTER");

    }

    public void ClearTable() throws SQLException {

        //PreparedStatement preparedStatement = connection.prepareStatement("TRUNCATE TABLE BILL_CALCULATION");
        Statement statement = connection.createStatement();

        statement.executeQuery("DELETE from BARCODE");

        txt_ItemName.clear();
        // combo_Category.selectionModelProperty().setValue("Select");

    }

    public void loadDataFromDatabaseToTable() throws SQLException, IOException {
        //    String category = (String) combo_Category.getSelectionModel().getSelectedItem();

        ObservableList<Barcode1> list = FXCollections.observableArrayList();
        Statement stm = connection.createStatement();

        ResultSet resultSet = stm.executeQuery("select * from Barcode");
        int i = 1;
        while (resultSet.next()) {

            InputStream fis = resultSet.getBinaryStream("barcode_Image");
            BufferedImage imgt = javax.imageio.ImageIO.read(fis);
            WritableImage newImg = SwingFXUtils.toFXImage(imgt, null);
            BarcodeImage = new ImageView();
            BarcodeImage.setImage(newImg);

            list.add(new Barcode1(i, resultSet.getString("ITEMS_NAME"), resultSet.getString("CODE"), BarcodeImage));
            i++;

        }

        table_Barcodes.setItems(list);

     combo_Category.getSelectionModel().select("Select");
     txt_ItemName.clear();
        resultSet.close();

    }

    public void loadcatageryAgain() throws SQLException {
        //----------------------------------------// Auto complete Items  list code ----------------------------------------------------

        ObservableList updatedList = FXCollections.observableArrayList();
        updatedList.clear();

        String query = "select CATEGORY_NAME from CATEGORY";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {

            updatedList.add(resultSet.getString("category_name"));
        }

        combo_Category.getItems().removeAll(combo_Category.getItems());
        combo_Category.getItems().addAll(updatedList);
        combo_Category.getSelectionModel().select("Select");

        preparedStatement.close();

    }

    public void bindItems1() throws SQLException {
        txt_ItemName.setText(null);
        String category = (String) combo_Category.getSelectionModel().getSelectedItem();

        ObservableList categoryList = FXCollections.observableArrayList();
        // txt_ItemName.setText(null);
        categoryList.removeAll(categoryList);

        connection = DatabaseConnection.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("select * from " + category + "");
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {

            categoryList.add(resultSet.getString("ITEMS_NAME"));

        }

        org.controlsfx.control.textfield.TextFields.bindAutoCompletion(txt_ItemName, categoryList);

        preparedStatement.close();

    }

    private void addWithinTable() throws SQLException, IOException {

        if (txt_ItemName.getText().isEmpty()) {

        } else {
            //select * from ADD_ITEMS_DETAILS where ITEMS_NAME ='Apple' AND Barcode_Status = 'NON-GENERATED';

            String query = "select * from ADD_ITEMS_DETAILS where ITEMS_NAME = '" + txt_ItemName.getText() + "' AND Barcode_Status = 'NON-GENERATED' ";
            PreparedStatement preparedStatement4 = connection.prepareStatement(query);
            ResultSet resultSet4 = preparedStatement4.executeQuery();//'" + items.getText() + "'
            PreparedStatement ps = null;
            int result = 0;
            while (resultSet4.next()) {

                ItemName = resultSet4.getString("ITEMS_NAME");

                finalBarcode = resultSet4.getString("BAR_CODE");

                ps = connection.prepareStatement("insert into barcode values (?,?,?)");
                // Store global variable here 
                ps.setString(1, ItemName);
                ps.setString(2, finalBarcode);

                byte img[] = createImage(finalBarcode);     // here call method for barcode generation 
                int size = img.length;
                InputStream in = new ByteArrayInputStream(img);
                
                ps.setBinaryStream(3, in, size);

                result = ps.executeUpdate();

            }
            if (result > 0) {

                alert.setTitle("Validate owner name");
                alert.setHeaderText(null);
                alert.setContentText("data stored successfuly");
                alert.showAndWait();

            }
            loadDataFromDatabaseToTable();
            preparedStatement4.close();


        }
    }

    @FXML
    private void bindItemsWithinItemName(ActionEvent event) throws SQLException {

        bindItems1();
    }

    @FXML
    private void addButtonusedToStoredItemsWithinTableMenully(ActionEvent event) throws SQLException, IOException {

        addWithinTable();

        loadDataFromDatabaseToTable();

    }

    @FXML
    private void PrintBarcode(ActionEvent event) {

    }

}
