package account_billing;

import com.osiersystems.pojos.Barcode;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import oracle.sql.BLOB;
import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;
import requriedDatabaseConnection.DatabaseConnection;

/**
 * FXML Controller class
 *
 * @author Niteen
 */
public class GenerateBarcodeController implements Initializable {

    @FXML
    private TextField txt_Qty;
    @FXML
    private TableColumn<Barcode, Integer> tabc_srNo;
    @FXML
    private TableColumn<Barcode, String> tabc_Barcode;
    @FXML
    private TableColumn<Barcode, Integer> tabc_Qty;

    Connection connection;

    String CategaryName, ItemName, finalBarcode;
    int Qty;

    Alert alert = new Alert(Alert.AlertType.WARNING);
    @FXML
    private TableView<Barcode> barCodeTable;
    @FXML
    private TextField txt_ItemName;
    @FXML
    private TableColumn<Barcode, String> tabc_ItemName;

    
    
    BufferedImage image;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        connection = DatabaseConnection.getConnection();

     
        try {
            ClearTable();
        } catch (SQLException ex) {
            Logger.getLogger(GenerateBarcodeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //-------------------------------------------------------------------------------------------------------------
        ObservableList itemsNameList = FXCollections.observableArrayList();
        try {

            String query = "select items_name from add_items_details where";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                itemsNameList.add(resultSet.getString("ITEMS_NAME"));

            }

            org.controlsfx.control.textfield.TextFields.bindAutoCompletion(txt_ItemName, itemsNameList);

            preparedStatement.close();
        } catch (SQLException ex) {
            Logger.getLogger(GenerateBarcodeController.class.getName()).log(Level.SEVERE, null, ex);
        }

        tabc_srNo.setCellValueFactory(new PropertyValueFactory<>("srno"));
        tabc_srNo.setStyle("-fx-alignment: CENTER");

        tabc_ItemName.setCellValueFactory(new PropertyValueFactory<>("itemName"));
        tabc_ItemName.setStyle("-fx-alignment: LEFT");

        tabc_Barcode.setCellValueFactory(new PropertyValueFactory<>("barcode"));
        tabc_Barcode.setStyle("-fx-alignment: CENTER");

        tabc_Qty.setCellValueFactory(new PropertyValueFactory<>("qty"));
        tabc_Qty.setStyle("-fx-alignment: CENTER");

    }

    @FXML
    private void addWithinTable(ActionEvent event) throws SQLException {

        boolean flag = false;

        if (txt_ItemName.getText().isEmpty() && txt_Qty.getText().isEmpty()) {

            alert.setTitle("Validation");
            alert.setHeaderText(null);
            alert.setContentText("Enter All Field data ");
            alert.showAndWait();

        } else {

            /*
              CREATE TABLE "SYSTEM"."barcode" 
   (	"ITEMS_NAME" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	"CODE" NUMBER, 
	"QTY" NUMBER, 
	 CONSTRAINT "barcode" PRIMARY KEY ("ITEMS_NAME")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" ;
 
             */
            Qty = Integer.parseInt(txt_Qty.getText());
            System.out.println(" I am Here ");
            String query = "select * from ADD_ITEMS_DETAILS where ITEMS_NAME='" + txt_ItemName.getText() + "'";
            PreparedStatement preparedStatement4 = connection.prepareStatement(query);
            ResultSet resultSet4 = preparedStatement4.executeQuery();//'" + items.getText() + "'

            while (resultSet4.next()) {

                ItemName = resultSet4.getString("ITEMS_NAME");

                finalBarcode = resultSet4.getString("BAR_CODE");

                flag = true;
            }

            preparedStatement4.close();

            ///Object barcodeResultObject = Barcode_Image.createImage(ItemName, finalBarcode);
//            BarcodeEAN codeEAN = new BarcodeEAN();
//            codeEAN.setCodeType(codeEAN.EAN13);
//            codeEAN.setCode("" + finalBarcode + "");
//            Image imageEAN = codeEAN.createImageWithBarcode(ItemName, null, null);
            if (flag == true) {

              //  printBarcode();
                PreparedStatement ps = connection.prepareStatement("insert into barcode values (?,?,?)");
                // Store global variable here 
                ps.setString(1, ItemName);
               ps.setString(2, finalBarcode);
           //   ps.setBinaryStream(2, image);
                ps.setInt(3, Integer.valueOf(txt_Qty.getText()));
                //ps.setBlob(2, (BLOB)bean39);
                int result = ps.executeUpdate();
                if (result > 0) {
                    loadDataFromDatabaseToTable();

                } else {

                    alert.setTitle("BARCODE");
                    alert.setHeaderText(null);
                    alert.setContentText(" barcode Logic  not is generated ");
                    alert.showAndWait();
                }
                ps.close();

            }
        }
    }

    @FXML
    private void printBarcode() {

      //   AddBarcode.creatrBarcode(ItemName, finalBarcode);
  
    }

    public void loadDataFromDatabaseToTable() throws SQLException {

        ObservableList<Barcode> list = FXCollections.observableArrayList();
        Statement stm = connection.createStatement();

        ResultSet resultSet = stm.executeQuery("select * from barcode");
        int i = 1;
        while (resultSet.next()) {
            // this value pass to pojo constructur and that value get using observable list to table.
            list.add(new Barcode(i , resultSet.getString("ITEMS_NAME") , resultSet.getString("CODE") , resultSet.getInt("QTY")));
            i++;
        }

        barCodeTable.setItems(list);

        txt_ItemName.clear();
        txt_Qty.clear();

        resultSet.close();

    }

    public void ClearTable() throws SQLException {

        //PreparedStatement preparedStatement = connection.prepareStatement("TRUNCATE TABLE BILL_CALCULATION");
        Statement statement = connection.createStatement();

        statement.executeQuery("DELETE from BARCODE");

        txt_ItemName.clear();
        txt_Qty.clear();

    }

    @FXML
    private void reSetAll(ActionEvent event) throws SQLException {

        ClearTable();
        loadDataFromDatabaseToTable();

    }

    public void ceeateBarcode() {

    }
}
