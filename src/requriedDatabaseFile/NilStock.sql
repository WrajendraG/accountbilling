--------------------------------------------------------
--  File created - Thursday-May-17-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table NILSTOCK
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."NILSTOCK" 
   (	"ITEMNAME" VARCHAR2(100 BYTE), 
	"STOCK" NUMBER(*,0), 
	"PURCHASINGPRICE" FLOAT(126), 
	"QTY" NUMBER(*,0), 
	"UNIT" VARCHAR2(20 BYTE), 
	"REQURIEDAMOUNT" FLOAT(126), 
	"CATEGORY" VARCHAR2(40 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into SYSTEM.NILSTOCK
SET DEFINE OFF;
Insert into SYSTEM.NILSTOCK (ITEMNAME,STOCK,PURCHASINGPRICE,QTY,UNIT,REQURIEDAMOUNT,CATEGORY) values ('Foundation',0,500,1,'Pcs.',500,'Cosmatic');
