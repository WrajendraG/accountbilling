--------------------------------------------------------
--  File created - Monday-March-19-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table BARCODE
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."BARCODE" 
   (	"ITEMS_NAME" VARCHAR2(50 BYTE), 
	"CODE" VARCHAR2(50 BYTE), 
	"BARCODE_IMAGE" BLOB, 
	"QTY" NUMBER(*,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" 
 LOB ("BARCODE_IMAGE") STORE AS (
  TABLESPACE "SYSTEM" ENABLE STORAGE IN ROW CHUNK 8192 PCTVERSION 10
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)) ;
REM INSERTING into SYSTEM.BARCODE
SET DEFINE OFF;
--------------------------------------------------------
--  Constraints for Table BARCODE
--------------------------------------------------------

  ALTER TABLE "SYSTEM"."BARCODE" MODIFY ("BARCODE_IMAGE" NOT NULL ENABLE);
