--------------------------------------------------------
--  File created - Monday-March-19-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table NILSTOCK
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."NILSTOCK" 
   (	"ITEMNAME" VARCHAR2(100 BYTE), 
	"STOCK" NUMBER(*,0), 
	"PURCHASINGPRICE" FLOAT(126), 
	"QTY" NUMBER(*,0), 
	"UNIT" VARCHAR2(20 BYTE), 
	"REQURIEDAMOUNT" FLOAT(126)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into SYSTEM.NILSTOCK
SET DEFINE OFF;
Insert into SYSTEM.NILSTOCK (ITEMNAME,STOCK,PURCHASINGPRICE,QTY,UNIT,REQURIEDAMOUNT) values ('Powder',0,40,1,'Pcs.',40);
