--------------------------------------------------------
--  File created - Monday-March-19-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table PASS
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."PASS" 
   (	"NAME" VARCHAR2(30 BYTE), 
	"PAS" VARCHAR2(30 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into SYSTEM.PASS
SET DEFINE OFF;
Insert into SYSTEM.PASS (NAME,PAS) values ('pankajgadkar@gmail.com','Pankaj@123');
--------------------------------------------------------
--  DDL for Index PASS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYSTEM"."PASS_PK" ON "SYSTEM"."PASS" ("NAME") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table PASS
--------------------------------------------------------

  ALTER TABLE "SYSTEM"."PASS" ADD CONSTRAINT "PASS_PK" PRIMARY KEY ("NAME")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
 
  ALTER TABLE "SYSTEM"."PASS" MODIFY ("PAS" NOT NULL ENABLE);
