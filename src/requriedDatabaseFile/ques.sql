--------------------------------------------------------
--  File created - Monday-March-19-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table QUES
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."QUES" 
   (	"AN1" VARCHAR2(10 BYTE), 
	"AN2" VARCHAR2(10 BYTE), 
	"AN3" VARCHAR2(10 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into SYSTEM.QUES
SET DEFINE OFF;
