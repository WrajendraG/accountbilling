package validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.control.Alert;

public class ValidateHere {

    boolean flag = false;

    static Alert alert = new Alert(Alert.AlertType.WARNING);

    public static boolean firstName(String first_Name) {
        Pattern p = Pattern.compile("[a-zA-Z]+");
        Matcher m = p.matcher(first_Name);
        if (m.find() && m.group().equals(first_Name)) {
            return true;
        } else {
 
            alert.setTitle("Validate owner name");
            alert.setHeaderText(null);
            alert.setContentText("Owner Name Must be start with character ");
            alert.showAndWait();
            return false;
        }
    }

    public static boolean lastName(String last_Name) {

         Pattern p = Pattern.compile("[a-zA-Z]+");
        Matcher m = p.matcher(last_Name);
        if (m.find() && m.group().equals(last_Name)) {
            return true;
        } else {

            alert.setTitle("Validate owner name");
            alert.setHeaderText(null);
            alert.setContentText("Owner Name Must be start with character ");
            alert.showAndWait();
            return false;
        }
    }

    public static boolean email(String email) {

        Pattern p = Pattern.compile("[a-zA-z0-9][a-zA-z0-9._]*@[a-zA-z0-9]+([.][a-zA-Z]+)+");
        Matcher m = p.matcher(email);

        if (m.find() && m.group().equals(email)) {
            return true;
        } else {

            alert.setTitle("Validate Email");
            alert.setHeaderText(null);
            alert.setContentText("Please Enter Valid Email ");
            alert.showAndWait();
            return false;
        }
    }

    public static boolean mobile(String mobile_Number) {
        Pattern p = Pattern.compile("^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$");
        Matcher m = p.matcher(mobile_Number);
        if (m.find() && m.group().equals(mobile_Number)) {
            return true;
        } else {

            alert.setTitle("Validate Mobile Number");
            alert.setHeaderText(null);
            alert.setContentText("Please Enter Valid Mobile Number ");
            alert.showAndWait();
            return false;
        }
    }

    public static boolean password(String password) {

        Pattern p = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,15})");
        Matcher m = p.matcher(password);

        if (m.matches()) {
            return true;
        } else {

            alert.setTitle("Validate Password");
            alert.setHeaderText(null);
            alert.setContentText("Password must contain at list one(Digit , Lowercase , Uppercase ,special character) and length between 6-15 ) ");
            alert.showAndWait();
            return false;
        }
    }

    public static boolean panNumber(String pan_Number) {
        
            Pattern p = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}$");
                 Matcher m =p.matcher(pan_Number);
                if(m.find() && m.group().equals(pan_Number))
                {
                    return true;
                }
                else
                {

                    alert.setTitle("Validation");
                    alert.setHeaderText(null);
                    alert.setContentText("You Entered Wrong PAN Number ");
                    alert.showAndWait();
                    return false;
                }

    }

    public static boolean bankNumber(String bank_Number) {

      Pattern p = Pattern.compile("[0-9]+");
        Matcher m = p.matcher(bank_Number);
        if (m.find() && m.group().equals(bank_Number))
        {
            return true;
            
        } else {

            alert.setTitle("Validation");
            alert.setHeaderText(null);
            alert.setContentText("You Entered Number Only Not Character ");
            alert.showAndWait();
            return false;
            
        }
    }
     public static boolean number(String number) {

      Pattern p = Pattern.compile("[0-9]+");
        Matcher m = p.matcher(number);
        if (m.find() && m.group().equals(number))
        {
            return true;
            
        } else {

            alert.setTitle("Validation");
            alert.setHeaderText(null);
            alert.setContentText("In Some Text Field You Entered Number Only ");
            alert.showAndWait();
            return false;
            
        }
    }
   public static boolean pinNumber(String pin_Number) {

      Pattern p = Pattern.compile("^[1-9][0-9]{5}$");
        Matcher m = p.matcher(pin_Number);
        if (m.find() && m.group().equals(pin_Number))
        {
            return true;
            
        } else {

            alert.setTitle("Validation");
            alert.setHeaderText(null);
            alert.setContentText("You Entered Wrong Pin Number ");
            alert.showAndWait();
            return false;
            
        }
    }
//\\d{5}(-\\d{4})?
    public static boolean ifscNumber(String ifsc_Number) {

                                     Pattern p = Pattern.compile("[A-Z|a-z]{4}[0][\\d]{6}$");
                                                            Matcher m =p.matcher(ifsc_Number);
                                                            if(m.find() && m.group().equals(ifsc_Number))
                                                            {
                                                                return true;
                                                            }
                                                            else
                                                            {

                                                                alert.setTitle("Validation");
                                                                alert.setHeaderText(null);
                                                                alert.setContentText("You Entered Wrong IFSC Code ");
                                                                alert.showAndWait();
                                                                return false;
                                                            }
    }

    public static boolean gstNumber(String gst_Number) {

        
       // Pattern p = Pattern.compile("[0-9]{2}[A-Z|a-z]{5}[0-9]{4}[A-Z|a-z]{1}[0-9]{1}[Z|z][0-9]{1}$");
        Pattern p = Pattern.compile("[0-9]{2}[(a-z)(A-Z)]{5}\\d{4}[(a-z)(A-Z)]{1}\\d{1}Z\\d{1}");
                                                 //"[0-9]{2}[(a-z)(A-Z)]{5}\d{4}[(a-z)(A-Z)]{1}\d{1}Z\d{1}"
        Matcher m = p.matcher(gst_Number);
        if (m.find() && m.group().equals(gst_Number)) {
            return true;
            
        } else {

            alert.setTitle("Validation");
            alert.setHeaderText(null);
            alert.setContentText("You Entered Wrong GST Number ");
            alert.showAndWait();
            return false;
            
        }
    }

}


/*






*/
